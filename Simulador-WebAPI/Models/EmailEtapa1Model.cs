﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_WebAPI.Models
{
    public class EmailEtapa1Model
    {
        public string Nome { get; set; }
        public virtual List<CampoEtapa1Model> Campos { get; set; }
    }

    public class CampoEtapa1Model
    {
        public string Texto { get; set; }
        public virtual List<Campo2Etapa1Model> SubCampos { get; set; }
    }

    public class Campo2Etapa1Model
    {
        public string Texto { get; set; }
        public virtual List<RespostaEtapa1Model> SubCampos { get; set; }
    }

    public class RespostaEtapa1Model
    {
        public string Resposta { get; set; }
    }
}
