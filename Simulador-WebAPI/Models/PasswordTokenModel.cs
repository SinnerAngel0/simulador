﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_WebAPI.Models
{
    public class PasswordTokenModel
    {
        public string Senha { get; set; }
        public string Token { get; set; }

    }
}
