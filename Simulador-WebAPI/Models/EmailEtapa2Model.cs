﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_WebAPI.Models
{
    public class EmailEtapa2Model
    {
        public string Nome { get; set; }
        public List<CampoEtapa2Model> Campos { get; set; }
    }

    public class CampoEtapa2Model
    {
        public string Texto { get; set; }
        public List<RespostaEtapa2Model> SubCampos { get; set; }
    }

    public class RespostaEtapa2Model
    {
        public string Resposta { get; set; }
    }

}
