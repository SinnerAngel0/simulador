﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using IESB_ConteudoWebAPI.Helpers;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Simulador_DAO.Entities;
using Simulador_DAO.Facades;
using Simulador_WebAPI.Helpers;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Simulador_WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SimuladorController : ControllerBase
    {
        private readonly ApplicationSettings _appSettings;
        public static IWebHostEnvironment _environment;
        private FileHelper _fileHelper;
        public SimuladorController(IOptions<ApplicationSettings> appSettings, IWebHostEnvironment environment)
        {
            _appSettings = appSettings.Value;
            _environment = environment;
            _fileHelper = new FileHelper(appSettings, environment);
        }

        [HttpGet, Route("BuscarSimulador")]
        public async Task<IActionResult> BuscarSimulador()
        {
            try
            {
                List<Tabela> tabelas = Repositorio.Get().Tabela.Listar();
                List<Coluna> colunas = Repositorio.Get().Coluna.Listar();
                List<Campo> Campo = Repositorio.Get().Campo.Listar();
                Simulador simulador = Repositorio.Get().Simulador.Buscar(x => x.ID == 1);
                return Ok(simulador);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet, Route("BuscarTabelaEtapa1/{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> BuscarTabelaEtapa1(int id)
        {
            try
            {
                string userID = User.Claims.First(c => c.Type == "UserID").Value;

                Usuario usuario = Repositorio.Get().Usuario.Buscar(x => x.ID == Int32.Parse(userID));

                Empresa empresa = Repositorio.Get().Empresa.Buscar(x => x.ID == id);

                Tabela tabela = Repositorio.Get().Tabela.Buscar(x => x.ID == 1);
                List<Campo> campos = Repositorio.Get().Campo.Listar(x => x.TabelaID == tabela.ID);
                Etapa1Helper helper = new Etapa1Helper();
                return Ok(helper.TabelaBuilder(tabela, empresa));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet, Route("BuscarTabelaEtapa2/{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> BuscarTabelaEtapa2(int id)
        {
            try
            {
                string userID = User.Claims.First(c => c.Type == "UserID").Value;

                Usuario usuario = Repositorio.Get().Usuario.Buscar(x => x.ID == Int32.Parse(userID));

                Empresa empresa = Repositorio.Get().Empresa.Buscar(x => x.ID == id);

                Tabela tabela = Repositorio.Get().Tabela.Buscar(x => x.ID == 2);
                List<Campo> campos = Repositorio.Get().Campo.Listar(x => x.TabelaID == tabela.ID);
                Etapa2Helper helper = new Etapa2Helper();
                return Ok(helper.TabelaBuilder(tabela, empresa));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet, Route("BuscarTabelaEtapa3/{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> BuscarTabelaEtapa3(int id)
        {
            try
            {
                string userID = User.Claims.First(c => c.Type == "UserID").Value;

                Usuario usuario = Repositorio.Get().Usuario.Buscar(x => x.ID == Int32.Parse(userID));

                Empresa empresa = Repositorio.Get().Empresa.Buscar(x => x.ID == id);

                Tabela tabela = Repositorio.Get().Tabela.Buscar(x => x.ID == 3);
                List<Campo> campos = Repositorio.Get().Campo.Listar(x => x.TabelaID == tabela.ID);
                Etapa3Helper helper = new Etapa3Helper();
                return Ok(helper.TabelaBuilder(tabela, empresa));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpGet, Route("BuscarTabelaEtapa4/{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> BuscarTabelaEtapa4(int id)
        {
            try
            {
                string userID = User.Claims.First(c => c.Type == "UserID").Value;
                Usuario usuario = Repositorio.Get().Usuario.Buscar(x => x.ID == Int32.Parse(userID));

                Empresa empresa = Repositorio.Get().Empresa.Buscar(x => x.ID == id);

                Tabela tabela = Repositorio.Get().Tabela.Buscar(x => x.ID == 4);
                List<Campo> campos = Repositorio.Get().Campo.Listar(x => x.TabelaID == tabela.ID);
                //Etapa4Helper helper4 = new Etapa4Helper();
                Etapa2Helper helper = new Etapa2Helper();
                helper.GetDespesasVariaveis(empresa);
                helper.GetMargemContribuicao(empresa);
                helper.GetDespesasFixas(empresa);
                helper.GetInvestimentos(empresa);

               helper.GetCapacidadePagamentoMensal(empresa);
               helper.VendaMinima(empresa);
               helper.VendaMinimaMensal(empresa);
               helper.GetLucroMensalDesejado(empresa);
               helper.GetLucroMensalDesejadoPerc(empresa);
               helper.GetPontoEquilibrio(empresa);
               helper.GetTaxaMarcacao(empresa);
               helper.GetProduto(empresa);
               helper.GetVenda(empresa);
               helper.GetCaixaDisponivel(empresa);
               helper.GetAposAmortizacao(empresa);
               helper.VendasMediaMensal(empresa);
               helper.GetLucratividade(empresa);

               Etapa1Helper helper1 = new Etapa1Helper();
               helper1.Maturidade(empresa);
               helper1.Satisfacao(empresa);
                return Ok(helper.TabelaBuilder(tabela, empresa));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet, Route("BuscarCampoUsuarios/{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> BuscarCampoUsuario(int id)
        {
            try
            {
                string userID = User.Claims.First(c => c.Type == "UserID").Value;
                string access = User.Claims.First(c => c.Type == "accessControl").Value;

                Usuario usuario = Repositorio.Get().Usuario.Buscar(Int32.Parse(userID));
                List<CampoEmpresa> campoUsuarios = Repositorio.Get().CampoEmpresa.Listar(x => x.CampoID == id && x.EmpresaID == usuario.ID);
                return Ok(campoUsuarios);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet, Route("DeletarCampoEmpresa/{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<Object> DeletarCampoEmpresa(int id)
        {
            try
            {
                string userID = User.Claims.First(c => c.Type == "UserID").Value;
                string access = User.Claims.First(c => c.Type == "accessControl").Value;
                Repositorio.Get().CampoEmpresa.Excluir(id);
                return new
                {
                    message = "CampoUsuario deletado com sucesso!"
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost, Route("GravarCampoEmpresaEtapa1")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<Object> GravarCampoEmpresaEtapa1(CampoEmpresa model)
        {
            try
            {
                string userID = User.Claims.First(c => c.Type == "UserID").Value;
                Usuario usuario = Repositorio.Get().Usuario.Buscar(Int32.Parse(userID));

                CampoEmpresa campoEmpresa = Repositorio.Get().CampoEmpresa.Buscar(model.ID);
                campoEmpresa.Resposta = model.Resposta;
                Repositorio.Get().CampoEmpresa.Alterar(campoEmpresa);
                return campoEmpresa;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost, Route("GravarCampoEmpresaEtapa2")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GravarCampoEmpresaEtapa2(CampoEmpresa model)
        {
            try
            {
                string userID = User.Claims.First(c => c.Type == "UserID").Value;
                Usuario usuario = Repositorio.Get().Usuario.Buscar(Int32.Parse(userID));

                CampoEmpresa campoUsuario = Repositorio.Get().CampoEmpresa.Buscar(model.ID);
                Empresa empresa = Repositorio.Get().Empresa.Buscar(x => x.ID == model.EmpresaID);
                campoUsuario.Resposta = model.Resposta;
                Repositorio.Get().CampoEmpresa.Alterar(campoUsuario);
                Etapa2Helper helper = new Etapa2Helper();

                return Ok(helper.CampoEmpresaBuilder(campoUsuario, empresa));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class FileUploadAPI
        {
            public IFormFile files { get; set; }
        }


        [HttpPost]
        [Route("EnviarResultado")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        //Post: /api/ApplicationUser/ResetPassword
        // public async Task<IActionResult> EnviarResultadoEtapa1([FromForm]FileUploadAPI objFile)
        public async Task<IActionResult> EnviarResultado([FromForm]FileUploadAPI objFile)
        {
            try
            {
                string userID = User.Claims.First(c => c.Type == "UserID").Value;
                var usuario = Repositorio.Get().Usuario.Buscar(Int32.Parse(userID));



                //string url = _fileHelper.UploadImagem(objFile);

                EmailConfig emailConfig = _appSettings.EmailConfig;

                string from, to, bcc, cc, subject, body;
                from = emailConfig.Client;
                to = usuario.Email.Trim();
                bcc = "";
                cc = "";
                subject = "Resultado Diagnóstico Financeiro – SEBRAE";


                body = System.IO.File.ReadAllText(Path.Combine(_environment.ContentRootPath, "EmailTemplates/etapa1template.cshtml"));


                body = body.Replace("@ViewBag", usuario.FullName);
                body = body.ToString();

                NetworkCredential login = new NetworkCredential(emailConfig.Client, emailConfig.ClientPassword);
                EmailHelper email = new EmailHelper(login, emailConfig.Host, emailConfig.Port);
                email.Enviar(from, to, cc, bcc, subject, body,objFile.files);


                return Ok(new
                {
                    data = "E-mail enviado com sucesso!"
                });

                //return Ok(new { email = users[0].Email});
            }
            catch (Exception ex)
            {
                throw ex;
                //return BadRequest(new { message = "Problema no servidor tente mais tarde" });
            }
        }

        [HttpGet, Route("CheckMaturidadeSatisfacao/{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> CheckMaturidadeSatisfacao(int id)
        {
            try
            {
                string userID = User.Claims.First(c => c.Type == "UserID").Value;
                string access = User.Claims.First(c => c.Type == "accessControl").Value;

                Usuario usuario = Repositorio.Get().Usuario.Buscar(Int32.Parse(userID));
                Empresa empresa = Repositorio.Get().Empresa.Buscar(x => x.ID == id);

                List<Campo> camposMaturidade = new List<Campo>();
                camposMaturidade.Add(Repositorio.Get().Campo.Buscar(x => x.ID == 2));
                camposMaturidade.Add(Repositorio.Get().Campo.Buscar(x => x.ID == 13));
                camposMaturidade.Add(Repositorio.Get().Campo.Buscar(x => x.ID == 27));
                camposMaturidade.Add(Repositorio.Get().Campo.Buscar(x => x.ID == 38));
                camposMaturidade.Add(Repositorio.Get().Campo.Buscar(x => x.ID == 52));
                camposMaturidade.Add(Repositorio.Get().Campo.Buscar(x => x.ID == 75));
                camposMaturidade.Add(Repositorio.Get().Campo.Buscar(x => x.ID == 125));


                List<Campo> camposSatisfacao = new List<Campo>();
                camposSatisfacao.Add(Repositorio.Get().Campo.Buscar(x => x.ID == 7));
                camposSatisfacao.Add(Repositorio.Get().Campo.Buscar(x => x.ID == 18));
                camposSatisfacao.Add(Repositorio.Get().Campo.Buscar(x => x.ID == 32));
                camposSatisfacao.Add(Repositorio.Get().Campo.Buscar(x => x.ID == 43));
                camposSatisfacao.Add(Repositorio.Get().Campo.Buscar(x => x.ID == 57));
                camposSatisfacao.Add(Repositorio.Get().Campo.Buscar(x => x.ID == 114));
                camposSatisfacao.Add(Repositorio.Get().Campo.Buscar(x => x.ID == 130));
                Boolean mat = false;
                Boolean satis = false;
                foreach (Campo superCampo in camposMaturidade)
                {
                    CampoEmpresa campoUsuario2 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == superCampo.SubCampos[0].ID && x.EmpresaID == empresa.ID);                    
                    if(campoUsuario2.Resposta != null)
                    {
                        mat = true;
                    }
                }

                foreach (Campo superCampo in camposSatisfacao)
                {
                    CampoEmpresa campoUsuario2 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == superCampo.SubCampos[0].ID && x.EmpresaID == empresa.ID);
                    if (campoUsuario2.Resposta != null)
                    {
                        satis = true;
                    }
                }

                return Ok(new
                {
                    maturidade = mat,
                    satisfacao = satis
                }) ;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet, Route("CheckPorcentagem/{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> CheckPorcentagem(int id)
        {
            try
            {
                string userID = User.Claims.First(c => c.Type == "UserID").Value;
                string access = User.Claims.First(c => c.Type == "accessControl").Value;

                Usuario usuario = Repositorio.Get().Usuario.Buscar(Int32.Parse(userID));
                Empresa empresa = Repositorio.Get().Empresa.Buscar(x => x.ID == id);

                List<Campo> campos = Repositorio.Get().Campo.Listar(x => x.Tipo == Simulador_DAO.Enums.ENUMTipo.INPUT || x.Tipo == Simulador_DAO.Enums.ENUMTipo.COMBO);
                double percFinal = 0;
                        double perc = 100.0f / campos.Count;
               
                    foreach (Campo subCampo in campos)
                    {
                        List<CampoEmpresa> campoEmpresas = Repositorio.Get().CampoEmpresa.Listar(x => x.EmpresaID == empresa.ID && x.CampoID == subCampo.ID);
                        foreach (CampoEmpresa campoEmpresa in campoEmpresas)
                        {
                            if (campoEmpresa.Resposta != null)
                            {
                                percFinal += perc;
                            }
                        }
                    }
                

                return Ok(new {
                    porcentagem = Math.Round(percFinal, 2)
            });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
