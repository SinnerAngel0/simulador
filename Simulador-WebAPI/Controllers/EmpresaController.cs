﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Simulador_DAO.Entities;
using Simulador_DAO.Enums;
using Simulador_DAO.Facades;
using Simulador_WebAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpresaController : ControllerBase
    {
        private readonly ApplicationSettings _appSettings;
        public static IWebHostEnvironment _environment;
        public EmpresaController(IOptions<ApplicationSettings> appSettings, IWebHostEnvironment environment)
        {
            _appSettings = appSettings.Value;
            _environment = environment;
        }


        [HttpPost,Route("CadastroEmpresa")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        //Post: /api/ApplicationUser/Cadastro
        public async Task<IActionResult> CadastroEmpresa(Empresa model)
        {
            try
            {
                string userID = User.Claims.First(c => c.Type == "UserID").Value;

                Usuario usuario = Repositorio.Get().Usuario.Buscar(x => x.ID == Int32.Parse(userID));
                UsuarioHelper helper = new UsuarioHelper();
                Empresa response = new Empresa();
                response.Ativo = true;
                response.NomeFantasia = model.NomeFantasia;
                response.Cnpj = model.Cnpj;
                response.Produto = model.Produto;
                response.RazaoSocial = model.RazaoSocial;
                response.Faturamento = model.Faturamento;
                response.UsuarioID = usuario.ID;
                var errormsg = new List<Object>();
                if (Repositorio.Get().Empresa.Listar(x => x.Cnpj == response.Cnpj).Count > 0)
                {
                    errormsg.Add(new { message = "Esse CNPJ já está cadastrado" });
                }
                if (errormsg.Count > 0)
                {
                    return BadRequest(errormsg);
                }
                Repositorio.Get().Empresa.Cadastrar(response);
                List<Campo> campos = Repositorio.Get().Campo.Listar(x => x.Tipo != ENUMTipo.TEXT);
                foreach (Campo campo in campos)
                {
                    CampoEmpresa campoEmpresa = new CampoEmpresa();
                    campoEmpresa.Campo = campo;
                    campoEmpresa.Empresa = response;
                    Repositorio.Get().CampoEmpresa.Cadastrar(campoEmpresa);
                }
                return Ok(response);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
    }
}
