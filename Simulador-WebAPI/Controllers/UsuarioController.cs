﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Simulador_DAO.Entities;
using Simulador_DAO.Enums;
using Simulador_DAO.Facades;
using Simulador_WebAPI.Helpers;
using Simulador_WebAPI.Models;

namespace Simulador_WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private readonly ApplicationSettings _appSettings;
        public static IWebHostEnvironment _environment;
        public UsuarioController(IOptions<ApplicationSettings> appSettings, IWebHostEnvironment environment)
        {
            _appSettings = appSettings.Value;
            _environment = environment;
        }

        [HttpPost]
        [Route("Cadastro")]
        //Post: /api/ApplicationUser/Cadastro
        public async Task<IActionResult> Cadastro(Usuario model)
        {           
            try
            {
                UsuarioHelper helper = new UsuarioHelper();
                Usuario response = new Usuario();
                response.Ativo = true;
                response.FullName = model.FullName;
                int max = model.FullName.Split(" ").Length;
                string[] splitNome = model.FullName.Split(" ");
                response.UserName = max > 1 ? splitNome[0] + " " + splitNome[max - 1] : splitNome[0];
                response.Cpf = model.Cpf;
                response.TipoTelefone = model.TipoTelefone;
                response.Telefone = model.Telefone;
                response.Email = model.Email;
                response.Senha = helper.Base64Encode(model.Senha);
                response.Access = ENUMAccess.USUARIO;
                var errormsg = new List<Object>();               

                if (Repositorio.Get().Usuario.Listar(x => x.Email == response.Email).Count > 0)
                {
                    errormsg.Add(new { message = "Esse E-mail já está cadastrado" });
                }
                if (Repositorio.Get().Usuario.Listar(x => x.Cpf == response.Cpf).Count > 0)
                {
                    errormsg.Add(new { message = "Esse Cpf já está cadastrado" });
                }
                if (errormsg.Count > 0)
                {
                    return BadRequest(errormsg);
                }
                Repositorio.Get().Usuario.Cadastrar(response);
            
                return Ok(response);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        [HttpPost]
        [Route("Login")]
        //Post: /api/ApplicationUser/Login
        public async Task<IActionResult> Login(LoginModel model)
        {
            try
            {
                UsuarioHelper helper = new UsuarioHelper();
                Usuario usuario = Repositorio.Get().Usuario.Buscar(x => x.Cpf == model.Cpf && x.Senha == helper.Base64Encode(model.Senha.ToString()));

                var tokenDescriptior = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]{
                        new Claim("UserID", usuario.ID.ToString()),
                        new Claim("accessControl", usuario.Access.ToString()),
                    }),
                    Expires = DateTime.UtcNow.AddHours(3),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSettings.JWT_Secret)), SecurityAlgorithms.HmacSha256Signature)
                };
                var tokenHandler = new JwtSecurityTokenHandler();
                var securityToken = tokenHandler.CreateToken(tokenDescriptior);
                var token = tokenHandler.WriteToken(securityToken);
                return Ok(new { token });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet, Route("BuscarUsuario")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<Object> BuscarUsuario()
        {
            string userID = User.Claims.First(c => c.Type == "UserID").Value;
            string access = User.Claims.First(c => c.Type == "accessControl").Value;

            Usuario user = Repositorio.Get().Usuario.Buscar(x => x.ID == Int32.Parse(userID) && x.Access.ToString() == access);
            UsuarioHelper helper = new UsuarioHelper();
            return helper.Builder(user);
        }

        [HttpPost, Route("AlterarNomeUsuario")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<Object> AlterarNomeUsuario(Usuario usuario)
        {
            try
            {
                string userID = User.Claims.First(c => c.Type == "UserID").Value;
                string access = User.Claims.First(c => c.Type == "accessControl").Value;

                Usuario response = Repositorio.Get().Usuario.Buscar(x => x.ID == Int32.Parse(userID) && x.Access.ToString() == access);
                response.FullName = usuario.FullName;
                int max = usuario.FullName.Split(" ").Length;
                string[] splitNome = usuario.FullName.Split(" ");
                response.UserName = max > 1 ? splitNome[0] + " " + splitNome[max - 1] : splitNome[0];
                Repositorio.Get().Usuario.Alterar(response);
                return new
                {
                    data = "Nome alterado com sucesso"
                };
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost, Route("AlterarEmailUsuario")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<Object> AlterarEmailUsuario(Usuario usuario)
        {
            try
            {
                string userID = User.Claims.First(c => c.Type == "UserID").Value;
                string access = User.Claims.First(c => c.Type == "accessControl").Value;

                Usuario response = Repositorio.Get().Usuario.Buscar(x => x.ID == Int32.Parse(userID) && x.Access.ToString() == access);
                response.Email = usuario.Email;
                Repositorio.Get().Usuario.Alterar(response);
                return new
                {
                    data = "E-mail alterado com sucesso"
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost, Route("AlterarTelefoneUsuario")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<Object> AlterarTelefoneUsuario(Usuario usuario)
        {
            try
            {
                string userID = User.Claims.First(c => c.Type == "UserID").Value;
                string access = User.Claims.First(c => c.Type == "accessControl").Value;

                Usuario response = Repositorio.Get().Usuario.Buscar(x => x.ID == Int32.Parse(userID) && x.Access.ToString() == access);
                response.Telefone = usuario.Telefone;
                response.TipoTelefone = usuario.TipoTelefone;
                Repositorio.Get().Usuario.Alterar(response);
                return new
                {
                    data = "Telefone alterado com sucesso"
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost, Route("AlterarSenhaUsuario")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<Object> AlterarSenhaUsuario(TrocaSenha model)
        {
            try
            {
                string userID = User.Claims.First(c => c.Type == "UserID").Value;
                string access = User.Claims.First(c => c.Type == "accessControl").Value;

                UsuarioHelper helper = new UsuarioHelper();
                Usuario response = Repositorio.Get().Usuario.Buscar(x => x.ID == Int32.Parse(userID) && x.Access.ToString() == access && x.Senha == helper.Base64Encode(model.Senha));
                response.Senha = helper.Base64Encode(model.SenhaNova);
                Repositorio.Get().Usuario.Alterar(response);
                return new
                {
                    data = "Senha alterada com sucesso"
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost, Route("AlterarTipoEmpresaUsuario")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<Object> AlterarTipoEmpresaUsuario(Usuario usuario)
        {
            try
            {
                string userID = User.Claims.First(c => c.Type == "UserID").Value;
                string access = User.Claims.First(c => c.Type == "accessControl").Value;

                UsuarioHelper helper = new UsuarioHelper();
                Usuario response = Repositorio.Get().Usuario.Buscar(x => x.ID == Int32.Parse(userID) && x.Access.ToString() == access);
                //response.TipoEmpresa = usuario.TipoEmpresa;
                Repositorio.Get().Usuario.Alterar(response);
                return new
                {
                    data = "Tipo da empresa alterado com sucesso"
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost, Route("AlterarProdutoUsuario")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<Object> AlterarProdutoUsuario(Usuario usuario)
        {
            try
            {
                string userID = User.Claims.First(c => c.Type == "UserID").Value;
                string access = User.Claims.First(c => c.Type == "accessControl").Value;

                UsuarioHelper helper = new UsuarioHelper();
                Usuario response = Repositorio.Get().Usuario.Buscar(x => x.ID == Int32.Parse(userID) && x.Access.ToString() == access);
                //response.Produto = usuario.Produto;
                Repositorio.Get().Usuario.Alterar(response);
                return new
                {
                    data = "Produto alterado com sucesso"
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost, Route("AlterarRazaoSocialUsuario")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<Object> AlterarRazaoSocialUsuario(Usuario usuario)
        {
            try
            {
                string userID = User.Claims.First(c => c.Type == "UserID").Value;
                string access = User.Claims.First(c => c.Type == "accessControl").Value;

                UsuarioHelper helper = new UsuarioHelper();
                Usuario response = Repositorio.Get().Usuario.Buscar(x => x.ID == Int32.Parse(userID) && x.Access.ToString() == access);
                //response.RazaoSocial = usuario.RazaoSocial;
                Repositorio.Get().Usuario.Alterar(response);
                return new
                {
                    data = "Razão Social alterado com sucesso"
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost, Route("AlterarFaturamentoUsuario")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<Object> AlterarFaturamentoUsuario(Usuario usuario)
        {
            try
            {
                string userID = User.Claims.First(c => c.Type == "UserID").Value;
                string access = User.Claims.First(c => c.Type == "accessControl").Value;

                UsuarioHelper helper = new UsuarioHelper();
                Usuario response = Repositorio.Get().Usuario.Buscar(x => x.ID == Int32.Parse(userID) && x.Access.ToString() == access);
                //response.Faturamento = usuario.Faturamento;
                Repositorio.Get().Usuario.Alterar(response);
                return new
                {
                    data = "Faturamento alterado com sucesso"
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost]
        [Route("ResetPassword")]
        //Post: /api/ApplicationUser/ResetPassword
        public async Task<IActionResult> ResetPassword(Usuario model)
        {
            try
            {
                List<Usuario> users = Repositorio.Get().Usuario.Listar(x => x.Email == model.Email);
                if (users.Count > 0)
                {
                    EmailConfig emailConfig = _appSettings.EmailConfig;

                    string from, to, bcc, cc, subject, body;
                    from = emailConfig.Client;
                    to = users[0].Email.Trim();
                    bcc = "";
                    cc = "";
                    subject = "Recuperar senha!";


                    body = System.IO.File.ReadAllText(Path.Combine(_environment.ContentRootPath, "TemplateEmail/resetpassword.cshtml"));
                    var token = TokenHelper.Get().VerifyToken(_appSettings.JWT_Secret, users[0].Email);
                    var url = "https://localhost:5001/trocar-senha?token=" + token;
                    EmailToken emailToken = new EmailToken();
                    emailToken.Ativo = true;
                    emailToken.Email = users[0].Email;
                    emailToken.Token = token;
                    Repositorio.Get().EmailToken.Cadastrar(emailToken);
                    body = body.Replace("@ViewBag.changePassword", url);
                    body = body.ToString();

                    NetworkCredential login = new NetworkCredential(emailConfig.Client, emailConfig.ClientPassword);
                    EmailHelper email = new EmailHelper(login, emailConfig.Host, emailConfig.Port);
                    email.Enviar(from, to, cc, bcc, subject, body,null);


                    return Ok(new
                    {
                        data = "E-mail enviado com sucesso!"
                    });
                }
                else
                {
                    return BadRequest(new { error = "Esse E-mail não está cadastrado" });
                }
                //return Ok(new { email = users[0].Email});
            }
            catch (Exception ex)
            {
                throw ex;
                //return BadRequest(new { message = "Problema no servidor tente mais tarde" });
            }
        }

        [HttpPost]
        [Route("MudarPassword")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        //Post: /api/ApplicationUser/MudarPassword
        public async Task<IActionResult> MudarPassword(PasswordTokenModel model)
        {
            try
            {
                string email = User.Claims.First(c => c.Type == "Email").Value;
                List<EmailToken> resetTokenList = Repositorio.Get().EmailToken.Listar(x => x.Email == email && x.Token == model.Token);
                if (resetTokenList.Count > 0)
                {
                    Usuario user = Repositorio.Get().Usuario.Buscar(x => x.Email == email);
                    user.Senha = model.Senha;
                    Repositorio.Get().Usuario.Alterar(user);
                    Repositorio.Get().EmailToken.Excluir(resetTokenList[0].ID);
                    return Ok(new { message = "Senha alterada com sucesso!" });
                }
                else
                {
                    return BadRequest(new { error = "Requerimento expirado ou não existe, reenvie novamente" });
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}