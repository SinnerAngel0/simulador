﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Simulador_WebAPI.Helpers
{
    public class EmailHelper
    {
        private NetworkCredential _login;
        private string _host;
        private int _port;
        public EmailHelper(NetworkCredential login, string host, string port)
        {
            _login = login;
            _host = host;
            _port = Int32.Parse(port);
        }

        public SmtpClient Client()
        {
            var client = new SmtpClient();
            client.UseDefaultCredentials = false;
            client.Host = _host;
            client.Port = _port;
            client.EnableSsl = true;
            client.Credentials = _login;
            //client.SendCompleted += new SendCompletedEventHandler("calback");
            return client;
        }

        public MailMessage Menssagem(string from, string to, string cc, string bcc, string subject, string body, IFormFile file)
        {
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(from);
            msg.To.Add(new MailAddress(to));
            if (!string.IsNullOrEmpty(cc))
                msg.To.Add(new MailAddress(cc));
            if (!string.IsNullOrEmpty(bcc))
                msg.To.Add(new MailAddress(bcc));

            msg.Subject = subject;
            msg.Body = body;
            msg.BodyEncoding = Encoding.UTF8;
            msg.IsBodyHtml = true;
            if(file.Length > 0 && file != null)
            {
                using (var ms = new MemoryStream())
                {
                    file.CopyTo(ms);
                    var fileBytes = ms.ToArray();
                    Attachment att = new Attachment(new MemoryStream(fileBytes), file.FileName);
                    msg.Attachments.Add(att);
                }
            }
            
            msg.Priority = MailPriority.Normal;
            msg.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

            return msg;
        }

        public void Enviar(string from, string to, string cc, string bcc, string subject, string body, IFormFile file)
        {
            var cliente = Client();
            var mensagem = Menssagem(from, to, cc, bcc, subject, body, file);
            cliente.Send(mensagem);
        }

    }
}
