﻿using Microsoft.IdentityModel.Tokens;
using Simulador_DAO.Entities;
using Simulador_DAO.Facades;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Simulador_WebAPI.Helpers
{
    public class TokenHelper
    {
        static TokenHelper instance;

        public TokenHelper() { }

        public string TokenPasswordReset(string secret, string email)
        {
            var tokenDescriptior = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]{
                        new Claim("Email", email)
                    }),
                Expires = DateTime.UtcNow.AddMinutes(5),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secret)), SecurityAlgorithms.HmacSha256Signature)
            };
            var tokenHandler = new JwtSecurityTokenHandler();
            var securityToken = tokenHandler.CreateToken(tokenDescriptior);
            var token = tokenHandler.WriteToken(securityToken);
            return token;
        }

        public string VerifyToken(string secret, string email)
        {
            List<EmailToken> tokens = Repositorio.Get().EmailToken.Listar(x => x.Email == email);
            if (tokens.Count > 0)
            {
                return tokens[0].Token;
            }
            else
            {
                return TokenPasswordReset(secret, email);
            }
        }

        public static TokenHelper Get()
        {
            instance = instance == null ? instance = new TokenHelper() : instance;
            return instance;
        }

    }
}
