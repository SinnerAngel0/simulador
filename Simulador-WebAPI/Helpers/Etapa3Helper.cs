﻿using Simulador_DAO.Entities;
using Simulador_DAO.Facades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_WebAPI.Helpers
{
    public class Etapa3Helper
    {

        public Etapa3Helper()
        {

        }

        public Object TabelaBuilder(Tabela tabela, Empresa empresa)
        {
            List<Campo> response = Repositorio.Get().Campo.Listar(x => x.TabelaID == tabela.ID);
            List<Object> campos = new List<object>();
            foreach (Campo campo in response)
            {
                campos.Add(CampoBuilder(campo, empresa));
            }
            return new
            {
                tabela.ID,
                tabela.Nome,
                campos

            };
        }

        public Object CampoBuilder(Campo campo, Empresa empresa)
        {
            List<Campo> response = Repositorio.Get().Campo.Listar(x => x.SuperCampoID == campo.ID);
            List<Object> subCampos = new List<object>();
            foreach (Campo model in response)
            {
                if (model.Tipo != Simulador_DAO.Enums.ENUMTipo.TEXT)
                {
                    List<CampoEmpresa> CampoEmpresas = Repositorio.Get().CampoEmpresa.Listar(x => x.CampoID == model.ID && x.EmpresaID == empresa.ID);
                    foreach (CampoEmpresa CampoEmpresa in CampoEmpresas)
                    {
                        subCampos.Add(CampoEmpresaBuilder(CampoEmpresa, empresa));
                    }
                }
                else
                {
                    subCampos.Add(CampoBuilder(model, empresa));
                }
            }
            return new
            {
                campo.ID,
                campo.Descricao,
                campo.Texto,
                campo.Tipo,
                subCampos
            };
        }

        public Object CampoEmpresaBuilder(CampoEmpresa CampoEmpresa, Empresa empresa)
        {
            List<Campo> response = Repositorio.Get().Campo.Listar(x => x.ID == CampoEmpresa.CampoID);
            List<Object> subCampos = new List<object>();
            foreach (Campo campo in response)
            {
                foreach (Campo campos2 in campo.SubCampos)
                {
                    subCampos.Add(CampoBuilder(campos2, empresa));
                }
            }
            return new
            {
                CampoEmpresa.ID,
                CampoEmpresa.Resposta,
                CampoEmpresa.Campo,
                subCampos
            };
        }

        public void GetAtivoCirculante(Empresa empresa)
        {
            Campo despesas = Repositorio.Get().Campo.Buscar(x => x.ID == 184);
            List<Campo> perguntas = despesas.SubCampos;

            double finalResposta = 0;
            foreach(Campo pergunta in perguntas)
            {
                foreach(Campo resposta in pergunta.SubCampos)
                {
                    if(resposta.Tipo == Simulador_DAO.Enums.ENUMTipo.INPUT)
                    {
                        List<CampoEmpresa> uRespostas = Repositorio.Get().CampoEmpresa.Listar(x => x.CampoID == resposta.ID && x.EmpresaID == empresa.ID);
                        double doubleResposta = 0;
                        foreach (CampoEmpresa uResposta in uRespostas)
                        {
                            var checkValor = (uResposta.Resposta == null) ? 0 : Double.Parse(uResposta.Resposta);
                            doubleResposta = Somar(doubleResposta, checkValor);
                        }
                        finalResposta = Somar(finalResposta, doubleResposta);
                    }
                }
            }

            CampoEmpresa uAtivoCirc = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 205 && x.EmpresaID == empresa.ID);
            uAtivoCirc.Resposta = finalResposta.ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uAtivoCirc);
            
        }

        public void GetAtivoNaoCirculante(Empresa empresa)
        {
            Campo vendas = Repositorio.Get().Campo.Buscar(x => x.ID == 195);
            List<Campo> perguntas = vendas.SubCampos;
            CampoEmpresa uDespesas = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 106 && x.EmpresaID == empresa.ID);
            double finalResposta = 0;
            foreach (Campo pergunta in perguntas)
            {
                foreach (Campo resposta in pergunta.SubCampos)
                {
                    if (resposta.Tipo == Simulador_DAO.Enums.ENUMTipo.INPUT)
                    {
                        List<CampoEmpresa> uRespostas = Repositorio.Get().CampoEmpresa.Listar(x => x.CampoID == resposta.ID && x.EmpresaID == empresa.ID);
                        double doubleResposta = 0;
                        foreach (CampoEmpresa uResposta in uRespostas)
                        {
                            var checkValor = (uResposta.Resposta == null) ? 0 : Double.Parse(uResposta.Resposta);
                            doubleResposta = Somar(doubleResposta, checkValor);
                        }
                        finalResposta = Somar(finalResposta, doubleResposta);
                    }
                }
            }

            CampoEmpresa uAtivoNCirc = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 207 && x.EmpresaID == empresa.ID);
            var valorFinal = Subtrair(finalResposta,Double.Parse(uDespesas.Resposta));
            uAtivoNCirc.Resposta = valorFinal.ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uAtivoNCirc);
        }

        public void GetAtivo(Empresa empresa)
        {
            CampoEmpresa uAtivoCirc = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 207 && x.EmpresaID == empresa.ID);
            CampoEmpresa uAtivoNCirc = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 205 && x.EmpresaID == empresa.ID);

            CampoEmpresa uAtivo = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 209 && x.EmpresaID == empresa.ID);
            var valorFinal = Somar(0, Double.Parse(uAtivoCirc.Resposta));
            valorFinal = Somar(valorFinal, Double.Parse(uAtivoNCirc.Resposta));
            uAtivo.Resposta = valorFinal.ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uAtivo);
        }

        public void GetPassCirc(Empresa empresa)
        {
            Campo despesas = Repositorio.Get().Campo.Buscar(x => x.ID == 211);
            List<Campo> perguntas = despesas.SubCampos;

            double finalResposta = 0;
            foreach (Campo pergunta in perguntas)
            {
                foreach (Campo resposta in pergunta.SubCampos)
                {
                    if (resposta.Tipo == Simulador_DAO.Enums.ENUMTipo.INPUT)
                    {
                        List<CampoEmpresa> uRespostas = Repositorio.Get().CampoEmpresa.Listar(x => x.CampoID == resposta.ID && x.EmpresaID == empresa.ID);
                        double doubleResposta = 0;
                        foreach (CampoEmpresa uResposta in uRespostas)
                        {
                            var checkValor = (uResposta.Resposta == null) ? 0 : Double.Parse(uResposta.Resposta);
                            doubleResposta = Somar(doubleResposta, checkValor);
                        }
                        finalResposta = Somar(finalResposta, doubleResposta);
                    }
                }
            }

            CampoEmpresa uPassCirc = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 228 && x.EmpresaID == empresa.ID);
            uPassCirc.Resposta = finalResposta.ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uPassCirc);

        }

        public void GetPassNCirc(Empresa empresa)
        {
            Campo despesas = Repositorio.Get().Campo.Buscar(x => x.ID == 224);
            List<Campo> perguntas = despesas.SubCampos;

            double finalResposta = 0;
            foreach (Campo pergunta in perguntas)
            {
                foreach (Campo resposta in pergunta.SubCampos)
                {
                    if (resposta.Tipo == Simulador_DAO.Enums.ENUMTipo.INPUT)
                    {
                        List<CampoEmpresa> uRespostas = Repositorio.Get().CampoEmpresa.Listar(x => x.CampoID == resposta.ID && x.EmpresaID == empresa.ID);
                        double doubleResposta = 0;
                        foreach (CampoEmpresa uResposta in uRespostas)
                        {
                            var checkValor = (uResposta.Resposta == null) ? 0 : Double.Parse(uResposta.Resposta);
                            doubleResposta = Somar(doubleResposta, checkValor);
                        }
                        finalResposta = Somar(finalResposta, doubleResposta);
                    }
                }
            }

            CampoEmpresa uPassNCirc = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 230 && x.EmpresaID == empresa.ID);
            uPassNCirc.Resposta = finalResposta.ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uPassNCirc);

        }

        public void GetPatrimonioLiquido(Empresa empresa)
        {
            CampoEmpresa uAtivo = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 209 && x.EmpresaID == empresa.ID);
            CampoEmpresa uPassCirc = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 228 && x.EmpresaID == empresa.ID);
            CampoEmpresa uPassNCirc = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 230 && x.EmpresaID == empresa.ID);

            CampoEmpresa uPatrimonio = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 232 && x.EmpresaID == empresa.ID);
            var valorFinal = Somar(0, Double.Parse(uAtivo.Resposta));
            valorFinal = Subtrair(valorFinal, Double.Parse(uPassCirc.Resposta));
            valorFinal = Subtrair(valorFinal, Double.Parse(uPassNCirc.Resposta));
            uPatrimonio.Resposta = valorFinal.ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uPatrimonio);

        }

        public void VendasMediaMensal(Empresa empresa)
        {
            List<CampoEmpresa> CampoEmpresas = Repositorio.Get().CampoEmpresa.Listar(x => x.CampoID == 91 && x.EmpresaID == empresa.ID);

            double finalResposta = 0;

            foreach (CampoEmpresa CampoEmpresa in CampoEmpresas)
            {
                var checkValor = (CampoEmpresa.Resposta == null) ? 0 : Double.Parse(CampoEmpresa.Resposta);
                finalResposta = Somar(finalResposta, checkValor);
            }            

            CampoEmpresa uDespesas = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 241 && x.EmpresaID == empresa.ID);
            uDespesas.Resposta = "R$"+ Math.Round(finalResposta,2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uDespesas);

        }

        public void GetCustoTotal(Empresa empresa)
        {
            CampoEmpresa uDespesas = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 106 && x.EmpresaID == empresa.ID);
            CampoEmpresa uDespesasFixas = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 125 && x.EmpresaID == empresa.ID);
            CampoEmpresa investimento = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 134 && x.EmpresaID == empresa.ID);

            double finalResposta = 0;
            finalResposta = Somar(finalResposta, (uDespesas.Resposta == null) ? 0 : Double.Parse(uDespesas.Resposta));
            finalResposta = Somar(finalResposta, (uDespesasFixas.Resposta == null) ? 0 : Double.Parse(uDespesasFixas.Resposta));
            finalResposta = Somar(finalResposta, (investimento.Resposta == null) ? 0 : Double.Parse(investimento.Resposta));            

            CampoEmpresa custoTotal = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 243 && x.EmpresaID == empresa.ID);
            custoTotal.Resposta = "R$" + Math.Round(finalResposta,2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(custoTotal);

        }

        public void GetPontoEquilibrio(Empresa empresa)
        {

            CampoEmpresa uMargem = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 108 && x.EmpresaID == empresa.ID);
            CampoEmpresa uDespesasFixas = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 125 && x.EmpresaID == empresa.ID);
            CampoEmpresa uInvestimento = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 134 && x.EmpresaID == empresa.ID);

            CampoEmpresa uVendas = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 142 && x.EmpresaID == empresa.ID);

            double finalResposta = 0;
            finalResposta = Somar(finalResposta, (uDespesasFixas.Resposta == null) ? 0 : Double.Parse(uDespesasFixas.Resposta));
            finalResposta = Somar(finalResposta, (uInvestimento.Resposta == null) ? 0 : Double.Parse(uInvestimento.Resposta));

            finalResposta = Dividir(finalResposta, (uMargem.Resposta == null) ? 0 : Double.Parse(uMargem.Resposta));
            finalResposta = Multiplicar(finalResposta, (uVendas.Resposta == null) ? 0 : Double.Parse(uVendas.Resposta.Replace("R$","")));

            CampoEmpresa pontoEquilibrio = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 245 && x.EmpresaID == empresa.ID);
            pontoEquilibrio.Resposta = "R$" + Math.Round(finalResposta, 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(pontoEquilibrio);

        }

        public void GetPontoEquilibrioPerc (Empresa empresa)
        {
            Campo despesas = Repositorio.Get().Campo.Buscar(x => x.ID == 126);
            List<Campo> perguntas = despesas.SubCampos;


            CampoEmpresa uVendas = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 142 && x.EmpresaID == empresa.ID);
            CampoEmpresa pontoEquilibrio = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 146 && x.EmpresaID == empresa.ID);

            double finalResposta = 0;
            finalResposta = Somar(finalResposta, (pontoEquilibrio.Resposta == null) ? 0 : Double.Parse(pontoEquilibrio.Resposta.Replace("R$", "")));
            finalResposta = Dividir(finalResposta, (uVendas.Resposta == null) ? 0 : Double.Parse(uVendas.Resposta.Replace("R$", "")));
            finalResposta = Multiplicar(finalResposta, 100);

            CampoEmpresa pontoEquilibrioperc = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 247 && x.EmpresaID == empresa.ID);
            pontoEquilibrioperc.Resposta = Math.Round(finalResposta, 2).ToString() +"%";
            Repositorio.Get().CampoEmpresa.Alterar(pontoEquilibrioperc);

        }

        public void GetLucratividadeInvestimento(Empresa empresa)
        {
            CampoEmpresa uVendas = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 241 && x.EmpresaID == empresa.ID);
            CampoEmpresa resposta3 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 162 && x.EmpresaID == empresa.ID);
            double finalResposta = 0;

            finalResposta = Somar(finalResposta, VerifyValue(resposta3.Resposta));
            finalResposta = Dividir(finalResposta, VerifyValue(uVendas.Resposta));
            finalResposta = Multiplicar(finalResposta, 100);

            CampoEmpresa uLucratividadeInvestimento = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 249 && x.EmpresaID == empresa.ID);
            uLucratividadeInvestimento.Resposta = Math.Round(finalResposta, 2).ToString() + "%";
            Repositorio.Get().CampoEmpresa.Alterar(uLucratividadeInvestimento);

        }

        public void GetRentabilidadePatrimonioLiquido(Empresa empresa)
        {
            CampoEmpresa resposta3 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 162 && x.EmpresaID == empresa.ID);
            CampoEmpresa uPatrimonio = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 232 && x.EmpresaID == empresa.ID);
            double finalResposta = 0;

            finalResposta = Somar(finalResposta, VerifyValue(resposta3.Resposta));
            finalResposta = Dividir(finalResposta, VerifyValue(uPatrimonio.Resposta));
            finalResposta = Multiplicar(finalResposta, 100);

            CampoEmpresa pontoEquilibrioperc = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 251 && x.EmpresaID == empresa.ID);
            pontoEquilibrioperc.Resposta = Math.Round(finalResposta, 2).ToString() + "%";
            Repositorio.Get().CampoEmpresa.Alterar(pontoEquilibrioperc);

        }

        public void GetVendaMinima(Empresa empresa)
        {
            List<CampoEmpresa> vMinima = Repositorio.Get().CampoEmpresa.Listar(x => x.CampoID == 236 && x.EmpresaID == empresa.ID);
            CampoEmpresa uDespesasFixas = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 125 && x.EmpresaID == empresa.ID);
            CampoEmpresa investimento = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 134 && x.EmpresaID == empresa.ID);
            CampoEmpresa uVenda = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 241 && x.EmpresaID == empresa.ID);
            CampoEmpresa uDespesas = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 106 && x.EmpresaID == empresa.ID);

            var resposta1 = Somar(0, VerifyValue(uDespesasFixas.Resposta));
            resposta1 = Somar(resposta1, VerifyValue(investimento.Resposta));

            var resposta2 = Somar(0, VerifyValue(uVenda.Resposta));
            resposta2 = Subtrair(resposta2, VerifyValue(uDespesas.Resposta));


            double finalResposta = 0;

            foreach (CampoEmpresa CampoEmpresa in vMinima)
            {
                finalResposta = Somar(finalResposta, VerifyValue(CampoEmpresa.Resposta));
            }

            finalResposta = Somar(finalResposta, resposta1);
            finalResposta = Dividir(finalResposta, resposta2);
            finalResposta = Multiplicar(finalResposta, VerifyValue(uVenda.Resposta));

            CampoEmpresa vendaMinima = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 253 && x.EmpresaID == empresa.ID);
            vendaMinima.Resposta = "R$" + Math.Round(finalResposta, 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(vendaMinima);
        }

        public void GetLucratividadeDesejada(Empresa empresa)
        {
            List<CampoEmpresa> vMinima = Repositorio.Get().CampoEmpresa.Listar(x => x.CampoID == 236 && x.EmpresaID == empresa.ID);
            CampoEmpresa vendaMinima = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 253 && x.EmpresaID == empresa.ID);
            double finalResposta = 0;

            foreach (CampoEmpresa CampoEmpresa in vMinima)
            {
                finalResposta = Somar(finalResposta, VerifyValue(CampoEmpresa.Resposta));
            }

          
            finalResposta = Dividir(finalResposta, VerifyValue(vendaMinima.Resposta));
            finalResposta = Multiplicar(finalResposta, 100);

            CampoEmpresa uLucratividade = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 255 && x.EmpresaID == empresa.ID);
            uLucratividade.Resposta = Math.Round(finalResposta, 2).ToString() + "%";
            Repositorio.Get().CampoEmpresa.Alterar(uLucratividade);

        }

        public void GetMarkup(Empresa empresa)
        {
            double taxas = 0;
            double despesas = 0;
            double investimentos = 0;
            CampoEmpresa uVendas = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 142 && x.EmpresaID == empresa.ID);
            List<CampoEmpresa> impostosETaxas = Repositorio.Get().CampoEmpresa.Listar(x => x.CampoID == 94 && x.EmpresaID == empresa.ID);
            CampoEmpresa uDespesasFixas = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 125 && x.EmpresaID == empresa.ID);
            CampoEmpresa uInvestimento = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 134 && x.EmpresaID == empresa.ID);
            CampoEmpresa uLucratividade = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 255 && x.EmpresaID == empresa.ID);
            foreach (CampoEmpresa CampoEmpresa in impostosETaxas)
            {
                taxas = Somar(taxas, VerifyValue(CampoEmpresa.Resposta));
            }
            taxas = Dividir(taxas, VerifyValue(uVendas.Resposta));
            taxas = Multiplicar(100,taxas);

            despesas = Somar(despesas, VerifyValue(uDespesasFixas.Resposta));
            despesas = Dividir(despesas, VerifyValue(uVendas.Resposta));
            despesas = Multiplicar(100, despesas);

            investimentos = Somar(investimentos, VerifyValue(uInvestimento.Resposta));
            investimentos = Dividir(investimentos, VerifyValue(uVendas.Resposta));
            investimentos = Multiplicar(100, investimentos);


            double resposta1 = Somar(0, taxas);
            resposta1 = Somar(resposta1, despesas);
            resposta1 = Somar(resposta1, investimentos);
            resposta1 = Somar(resposta1, VerifyValue(uLucratividade.Resposta));

            double finalResposta = 100;
            finalResposta = Subtrair(finalResposta, resposta1);


            CampoEmpresa uMarkup = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 257 && x.EmpresaID == empresa.ID);
            uMarkup.Resposta = Math.Round(finalResposta, 2).ToString() + "%";
            Repositorio.Get().CampoEmpresa.Alterar(uMarkup);

        }

        public void GetMarcacao(Empresa empresa)
        {
            CampoEmpresa uMarkup = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 257 && x.EmpresaID == empresa.ID);

            double resposta1 = 0;
            double finalResposta = 100;

            resposta1 = Somar(0, VerifyValue(uMarkup.Resposta));
            resposta1 = Multiplicar(resposta1, 100);

            finalResposta = Dividir(finalResposta, resposta1);
            finalResposta = Multiplicar(finalResposta, 100);

            CampoEmpresa uMarcacao = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 260 && x.EmpresaID == empresa.ID);
            uMarcacao.Resposta = Math.Round(finalResposta, 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uMarcacao);
        }

        public void GetLiquidezImediata(Empresa empresa)
        {
            double caixaBanco = 0;
            CampoEmpresa uPassCirc = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 228 && x.EmpresaID == empresa.ID);
            List<CampoEmpresa> uCaixaBanco = Repositorio.Get().CampoEmpresa.Listar(x => x.CampoID == 186 && x.EmpresaID == empresa.ID);
            foreach (CampoEmpresa CampoEmpresa in uCaixaBanco)
            {
                caixaBanco = Somar(caixaBanco, VerifyValue(CampoEmpresa.Resposta));
            }            

            double finalResposta = 0;            

            finalResposta = Somar(finalResposta, caixaBanco);
            finalResposta = Dividir(finalResposta, VerifyValue(uPassCirc.Resposta));

            CampoEmpresa uLiquidezImediata = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 262 && x.EmpresaID == empresa.ID);
            uLiquidezImediata.Resposta = Math.Round(finalResposta, 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uLiquidezImediata);
        }

        public void GetLiquidezCorrente(Empresa empresa)
        {
            CampoEmpresa uPassCirc = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 228 && x.EmpresaID == empresa.ID);
            CampoEmpresa uAtivoCirc = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 205 && x.EmpresaID == empresa.ID);

            double finalResposta = 0;

            finalResposta = Somar(finalResposta, VerifyValue(uAtivoCirc.Resposta));
            finalResposta = Dividir(finalResposta, VerifyValue(uPassCirc.Resposta));

            CampoEmpresa uLiquidezCorrente = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 264 && x.EmpresaID == empresa.ID);
            uLiquidezCorrente.Resposta = Math.Round(finalResposta, 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uLiquidezCorrente);
        }

        public void GetLiquidezCorrenteSEstoque(Empresa empresa)
        {
            double estoques = 0;
            CampoEmpresa uPassCirc = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 228 && x.EmpresaID == empresa.ID);
            CampoEmpresa uAtivoCirc = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 205 && x.EmpresaID == empresa.ID);
            List<CampoEmpresa> uEstoques = Repositorio.Get().CampoEmpresa.Listar(x => x.CampoID == 194 && x.EmpresaID == empresa.ID);
            foreach (CampoEmpresa CampoEmpresa in uEstoques)
            {
                estoques = Somar(estoques, VerifyValue(CampoEmpresa.Resposta));
            }

            double finalResposta = 0;

            finalResposta = Somar(finalResposta, VerifyValue(uAtivoCirc.Resposta));
            finalResposta = Subtrair(finalResposta, estoques);
            finalResposta = Dividir(finalResposta, VerifyValue(uPassCirc.Resposta));

            CampoEmpresa uLiquidezCorrenteSEstoque = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 266 && x.EmpresaID == empresa.ID);
            uLiquidezCorrenteSEstoque.Resposta = Math.Round(finalResposta, 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uLiquidezCorrenteSEstoque);
        }

        public void GetPrazoMedioRecebimento(Empresa empresa)
        {
            double recebidos= 0;
            double recebidosMais90= 0;
            CampoEmpresa uVendas = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 142 && x.EmpresaID == empresa.ID);
            

            double finalResposta = 0;
            if(VerifyValue(uVendas.Resposta) != 0)
            {
                List<CampoEmpresa> uRecebidos = Repositorio.Get().CampoEmpresa.Listar(x => x.CampoID == 188 && x.EmpresaID == empresa.ID);
                foreach (CampoEmpresa CampoEmpresa in uRecebidos)
                {
                    recebidos = Somar(recebidos, VerifyValue(CampoEmpresa.Resposta));
                }

                List<CampoEmpresa> uRecebidosMais90 = Repositorio.Get().CampoEmpresa.Listar(x => x.CampoID == 190 && x.EmpresaID == empresa.ID);
                foreach (CampoEmpresa CampoEmpresa in uRecebidosMais90)
                {
                    recebidosMais90 = Somar(recebidosMais90, VerifyValue(CampoEmpresa.Resposta));
                }

                double resposta1 = 0;
                resposta1 = Somar(resposta1, recebidos);
                resposta1 = Somar(resposta1, recebidosMais90);

                double resposta2 = 0;
                resposta2 = Somar(resposta2, VerifyValue(uVendas.Resposta));
                resposta2 = Dividir(resposta2, 30);
                finalResposta = Somar(finalResposta, resposta1);
                finalResposta = Dividir(finalResposta, resposta2);
            }            

            CampoEmpresa uPrazoMedioRecebimento = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 268 && x.EmpresaID == empresa.ID);
            uPrazoMedioRecebimento.Resposta = Math.Round(finalResposta, 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uPrazoMedioRecebimento);
        }

        public void GetPrazoMedioPagamento(Empresa empresa)
        {
            double finalResposta = 0;

            CampoEmpresa uCustoTotal = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 243 && x.EmpresaID == empresa.ID);

            if (VerifyValue(uCustoTotal.Resposta) != 0)
            {
                CampoEmpresa uPassCirc = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 228 && x.EmpresaID == empresa.ID);
                finalResposta = Somar(finalResposta, VerifyValue(uPassCirc.Resposta));
                finalResposta = Dividir(finalResposta, VerifyValue(uCustoTotal.Resposta));
                finalResposta = Multiplicar(finalResposta, 30);
            }

            CampoEmpresa uPrazoMedioPagamento = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 270 && x.EmpresaID == empresa.ID);
            uPrazoMedioPagamento.Resposta = Math.Round(finalResposta, 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uPrazoMedioPagamento);
        }

        public void GetPrazoMedioEstoque(Empresa empresa)
        {
            double finalResposta = 0;

            double comissao = 0;
            List<CampoEmpresa> uComissao = Repositorio.Get().CampoEmpresa.Listar(x => x.CampoID == 96 && x.EmpresaID == empresa.ID);
            foreach (CampoEmpresa CampoEmpresa in uComissao)
            {
                comissao = Somar(comissao, VerifyValue(CampoEmpresa.Resposta));
            }            

            if (comissao != 0)
            {
                double estoques = 0;
                List<CampoEmpresa> uEstoques = Repositorio.Get().CampoEmpresa.Listar(x => x.CampoID == 194 && x.EmpresaID == empresa.ID);
                foreach (CampoEmpresa CampoEmpresa in uEstoques)
                {
                    estoques = Somar(estoques, VerifyValue(CampoEmpresa.Resposta));
                }

                double resposta1 = 0;
                resposta1 = Somar(resposta1, comissao);
                resposta1 = Dividir(resposta1, 30);

                CampoEmpresa uPassCirc = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 228 && x.EmpresaID == empresa.ID);
                finalResposta = Somar(finalResposta, estoques);
                finalResposta = Dividir(finalResposta, resposta1);
            }

            CampoEmpresa uPrazoMedioEstoque = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 272 && x.EmpresaID == empresa.ID);
            uPrazoMedioEstoque.Resposta = Math.Round(finalResposta, 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uPrazoMedioEstoque);
        }

        public void GetCicloFinaceiro(Empresa empresa)
        {
            double finalResposta = 0;

            CampoEmpresa uPrazoMedioRecebimento = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 268 && x.EmpresaID == empresa.ID);
            CampoEmpresa uPrazoMedioPagamento = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 270 && x.EmpresaID == empresa.ID);
            CampoEmpresa uPrazoMedioEstoque = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 272 && x.EmpresaID == empresa.ID);

            finalResposta = Somar(finalResposta, VerifyValue(uPrazoMedioEstoque.Resposta));
            finalResposta = Somar(finalResposta, VerifyValue(uPrazoMedioRecebimento.Resposta));
            finalResposta = Subtrair(finalResposta, VerifyValue(uPrazoMedioPagamento.Resposta));

            CampoEmpresa uCicloFinanceiro = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 274 && x.EmpresaID == empresa.ID);
            uCicloFinanceiro.Resposta = Math.Round(finalResposta, 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uCicloFinanceiro);
        }

        public void GetGiroDeCaixa(Empresa empresa)
        {
            double finalResposta = 360;

            CampoEmpresa uCicloFinanceiro = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 274 && x.EmpresaID == empresa.ID);

            finalResposta = Dividir(finalResposta, VerifyValue(uCicloFinanceiro.Resposta));

            CampoEmpresa uGiroDeCaixa = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 276 && x.EmpresaID == empresa.ID);
            uGiroDeCaixa.Resposta = Math.Round(finalResposta, 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uGiroDeCaixa);
        }

        public void GetCapitalDeGiro(Empresa empresa)
        {
            double finalResposta = 0;

            CampoEmpresa uCustoTotal = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 243 && x.EmpresaID == empresa.ID);
            CampoEmpresa uGiroDeCaixa = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 276 && x.EmpresaID == empresa.ID);

            finalResposta = Somar(finalResposta, VerifyValue(uCustoTotal.Resposta));
            finalResposta = Multiplicar(finalResposta, 12);
            finalResposta = Dividir(finalResposta, VerifyValue(uGiroDeCaixa.Resposta));

            CampoEmpresa uCapitalDeGiro = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 278 && x.EmpresaID == empresa.ID);
            uCapitalDeGiro.Resposta = "R$" + Math.Round(finalResposta, 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uCapitalDeGiro);
        }

        public void GetEstimativaDeCaixa(Empresa empresa)
        {
            double finalResposta = 0;

            CampoEmpresa resposta3 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 162 && x.EmpresaID == empresa.ID);

            finalResposta = Somar(finalResposta, VerifyValue(resposta3.Resposta));
            finalResposta = Multiplicar(finalResposta, 12);

            CampoEmpresa uEstimativaCaixa = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 280 && x.EmpresaID == empresa.ID);
            uEstimativaCaixa.Resposta = "R$" + Math.Round(finalResposta, 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uEstimativaCaixa);
        }


        public void GetResposta1(Empresa empresa)
        {
            CampoEmpresa uVendasRes = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 142 && x.EmpresaID == empresa.ID);            

            CampoEmpresa uVendas = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 282 && x.EmpresaID == empresa.ID);
            uVendas.Resposta = "R$" + Math.Round(VerifyValue(uVendasRes.Resposta), 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uVendas);

            CampoEmpresa resposta3 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 162 && x.EmpresaID == empresa.ID);
            CampoEmpresa uResposta3If = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 283 && x.EmpresaID == empresa.ID);
            if (VerifyValue(resposta3.Resposta) < 0)
            {
                uResposta3If.Resposta = "gerando um prejuízo de";
            }
            else
            {
                uResposta3If.Resposta = "e ainda sobra um lucro de";
            }
            Repositorio.Get().CampoEmpresa.Alterar(uResposta3If);

            CampoEmpresa uResposta3 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 284 && x.EmpresaID == empresa.ID);
            uResposta3.Resposta = "R$" + Math.Round(VerifyValue(resposta3.Resposta), 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uResposta3);

            CampoEmpresa uResposta3If2 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 285 && x.EmpresaID == empresa.ID);
            if (VerifyValue(resposta3.Resposta) < 0)
            {
                uResposta3If2.Resposta = "um prejuizo de";
            }
            else
            {
                uResposta3If2.Resposta = "uma lucratividade de";
            }
            Repositorio.Get().CampoEmpresa.Alterar(uResposta3If2);

            CampoEmpresa uLucratividadeInvestimentoRes = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 249 && x.EmpresaID == empresa.ID);

            CampoEmpresa uLucratividadeInvestimento = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 286 && x.EmpresaID == empresa.ID);
            uLucratividadeInvestimento.Resposta =  Math.Round(VerifyValue(uLucratividadeInvestimentoRes.Resposta), 2).ToString() + "%";
            Repositorio.Get().CampoEmpresa.Alterar(uLucratividadeInvestimento);

        }

        public void GetResposta2(Empresa empresa)
        {

            CampoEmpresa uVendas = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 142 && x.EmpresaID == empresa.ID);
            CampoEmpresa uDespesas = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 106 && x.EmpresaID == empresa.ID);

            double intermediario = 0;
            intermediario = Somar(intermediario, VerifyValue(uVendas.Resposta));
            intermediario = Subtrair(intermediario, VerifyValue(uDespesas.Resposta));

            CampoEmpresa uIntermediarioIf = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 288 && x.EmpresaID == empresa.ID);
            if (intermediario <= 0)
            {
                uIntermediarioIf.Resposta = "a empresa não remunera o seu";
            }
            else
            {
                uIntermediarioIf.Resposta = "a empresa vem remunerando o seu";
            }
            Repositorio.Get().CampoEmpresa.Alterar(uIntermediarioIf);

            CampoEmpresa uPatrimonio = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 232 && x.EmpresaID == empresa.ID);
            CampoEmpresa uPatrimonioIf = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 289 && x.EmpresaID == empresa.ID);
            if (VerifyValue(uPatrimonio.Resposta) > 0)
            {
                uIntermediarioIf.Resposta = "capital próprio";
            }
            else
            {
                uIntermediarioIf.Resposta = "ativo total";
            }
            Repositorio.Get().CampoEmpresa.Alterar(uPatrimonioIf);

            CampoEmpresa uPatrimonioIf2 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 290 && x.EmpresaID == empresa.ID);
            if (VerifyValue(uPatrimonio.Resposta) <= 0)
            {
                uPatrimonioIf2.Resposta = "";
            }
            else
            {
                uPatrimonioIf2.Resposta = "em";
            }
            Repositorio.Get().CampoEmpresa.Alterar(uPatrimonioIf2);

            CampoEmpresa uResposta3 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 284 && x.EmpresaID == empresa.ID);
            CampoEmpresa uAtivo = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 209 && x.EmpresaID == empresa.ID);

            double finalResposta = 0;
            finalResposta = Somar(finalResposta, VerifyValue(uResposta3.Resposta));
            finalResposta = Dividir(finalResposta, VerifyValue(uAtivo.Resposta));
            finalResposta = Multiplicar(finalResposta, 100);

            CampoEmpresa uResposta2 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 291 && x.EmpresaID == empresa.ID);
            uResposta2.Resposta = Math.Round(finalResposta, 2).ToString() + "%";
            Repositorio.Get().CampoEmpresa.Alterar(uResposta2);

            CampoEmpresa uResposta2If = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 292 && x.EmpresaID == empresa.ID);
            if (VerifyValue(uPatrimonio.Resposta) <= 0)
            {
                uResposta2If.Resposta = ".";
            }
            else
            {
                uResposta2If.Resposta = "ao mês.";
            }
            Repositorio.Get().CampoEmpresa.Alterar(uResposta2If);

        }

        public void GetResposta3(Empresa empresa)
        {

            CampoEmpresa pontoEquilibrio = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 236 && x.EmpresaID == empresa.ID);

            CampoEmpresa uResposta3 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 294 && x.EmpresaID == empresa.ID);
            uResposta3.Resposta = "R$" + Math.Round(VerifyValue(pontoEquilibrio.Resposta), 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uResposta3);

        }

        public void GetResposta4(Empresa empresa)
        {
            double vendasDesejada = 0;

            List<CampoEmpresa> uVendasDesejada = Repositorio.Get().CampoEmpresa.Listar(x => x.CampoID == 245 && x.EmpresaID == empresa.ID);
            foreach (CampoEmpresa CampoEmpresa in uVendasDesejada)
            {
                vendasDesejada = Somar(vendasDesejada, VerifyValue(CampoEmpresa.Resposta));
            }

            CampoEmpresa uResposta4_1 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 296 && x.EmpresaID == empresa.ID);
            uResposta4_1.Resposta = "R$" + Math.Round(vendasDesejada, 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uResposta4_1);

            CampoEmpresa vendaMinima = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 253 && x.EmpresaID == empresa.ID);

            CampoEmpresa uResposta4_2 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 297 && x.EmpresaID == empresa.ID);
            uResposta4_2.Resposta = "R$" + Math.Round(VerifyValue(vendaMinima.Resposta), 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uResposta4_2);

        }


        public void GetResposta5(Empresa empresa)
        {
            CampoEmpresa uMarcacao = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 260 && x.EmpresaID == empresa.ID);
            CampoEmpresa uResposta5_1 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 299 && x.EmpresaID == empresa.ID);
            uResposta5_1.Resposta =  Math.Round(VerifyValue(uMarcacao.Resposta), 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uResposta5_1);

            double compraProduto = 0;

            List<CampoEmpresa> uCompraProduto = Repositorio.Get().CampoEmpresa.Listar(x => x.CampoID == 239 && x.EmpresaID == empresa.ID);
            foreach (CampoEmpresa CampoEmpresa in uCompraProduto)
            {
                compraProduto = Somar(compraProduto, VerifyValue(CampoEmpresa.Resposta));
            }

            CampoEmpresa uResposta5_2 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 300 && x.EmpresaID == empresa.ID);
            uResposta5_2.Resposta = "R$" + Math.Round(compraProduto, 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uResposta5_2);

            double finalResposta = 0;
            finalResposta = Somar(finalResposta, compraProduto);
            finalResposta = Multiplicar(finalResposta, VerifyValue(uMarcacao.Resposta));


            CampoEmpresa uResposta5_3 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 301 && x.EmpresaID == empresa.ID);
            uResposta5_3.Resposta = "R$" + Math.Round(finalResposta, 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uResposta5_3);

        }

        public void GetResposta6(Empresa empresa)
        {
            CampoEmpresa uPatrimonio = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 232 && x.EmpresaID == empresa.ID);
            CampoEmpresa uPatrimonioIf = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 303 && x.EmpresaID == empresa.ID);
            CampoEmpresa uPatrimonioIf2 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 304 && x.EmpresaID == empresa.ID);
            if (VerifyValue(uPatrimonio.Resposta) < 0)
            {
                uPatrimonioIf.Resposta = "não terá condições de pagar todas a suas dívidas";
                uPatrimonioIf2.Resposta = "e ainda terá que aportar";
            }
            else
            {
                uPatrimonioIf.Resposta = "terá condições de pagar todas as suas dívidas";
                uPatrimonioIf2.Resposta = "e ainda sobrará de recursos próprios:";
            }
            Repositorio.Get().CampoEmpresa.Alterar(uPatrimonioIf);
            Repositorio.Get().CampoEmpresa.Alterar(uPatrimonioIf2);


            CampoEmpresa uResposta6 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 305 && x.EmpresaID == empresa.ID);
            uResposta6.Resposta = "R$" + Math.Round(VerifyValue(uPatrimonio.Resposta), 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uResposta6);

        }

        public void GetResposta7(Empresa empresa)
        {

            CampoEmpresa uLiquidezCorrente = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 264 && x.EmpresaID == empresa.ID);

            CampoEmpresa uResposta7 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 307 && x.EmpresaID == empresa.ID);
            uResposta7.Resposta = "R$" + Math.Round(VerifyValue(uLiquidezCorrente.Resposta), 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uResposta7);

        }

        public void GetResposta8(Empresa empresa)
        {

            CampoEmpresa uLiquidezCorrenteSEstoque = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 266 && x.EmpresaID == empresa.ID);

            CampoEmpresa uResposta8_1 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 309 && x.EmpresaID == empresa.ID);
            uResposta8_1.Resposta = "R$" + Math.Round(VerifyValue(uLiquidezCorrenteSEstoque.Resposta), 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uResposta8_1);

            CampoEmpresa uLiquidezImediata = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 262 && x.EmpresaID == empresa.ID);

            CampoEmpresa uResposta8_2 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 310 && x.EmpresaID == empresa.ID);
            uResposta8_2.Resposta = "R$" + Math.Round(VerifyValue(uLiquidezImediata.Resposta), 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uResposta8_2);

        }

        public void GetResposta9(Empresa empresa)
        {

            CampoEmpresa uPrazoMedioRecebimento = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 268 && x.EmpresaID == empresa.ID);

            CampoEmpresa uResposta9_1 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 312 && x.EmpresaID == empresa.ID);
            uResposta9_1.Resposta = Math.Round(VerifyValue(uPrazoMedioRecebimento.Resposta)).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uResposta9_1);

            CampoEmpresa uPrazoMedioPagamento = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 270 && x.EmpresaID == empresa.ID);

            CampoEmpresa uResposta9_2 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 313 && x.EmpresaID == empresa.ID);
            uResposta9_2.Resposta = Math.Round(VerifyValue(uPrazoMedioPagamento.Resposta)).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uResposta9_2);

        }

        public void GetResposta10(Empresa empresa)
        {
            CampoEmpresa uPrazoMedioEstoque = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 272 && x.EmpresaID == empresa.ID);

            CampoEmpresa uResposta10 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 315 && x.EmpresaID == empresa.ID);
            uResposta10.Resposta = Math.Round(VerifyValue(uPrazoMedioEstoque.Resposta), 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uResposta10);           

        }

        public void GetResposta11(Empresa empresa)
        {
            CampoEmpresa uVendas = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 142 && x.EmpresaID == empresa.ID);
            CampoEmpresa uDespesasFixas = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 125 && x.EmpresaID == empresa.ID);

            double finalResposta1 = 0;
            finalResposta1 = Somar(finalResposta1, VerifyValue(uDespesasFixas.Resposta));
            finalResposta1 = Dividir(finalResposta1, VerifyValue(uVendas.Resposta));
            finalResposta1 = Multiplicar(finalResposta1, 100);

            CampoEmpresa uResposta11_1 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 317 && x.EmpresaID == empresa.ID);
            uResposta11_1.Resposta = Math.Round(finalResposta1, 2).ToString() + "%";
            Repositorio.Get().CampoEmpresa.Alterar(uResposta11_1);

            double salario = 0;
            List<CampoEmpresa> uSalario = Repositorio.Get().CampoEmpresa.Listar(x => x.CampoID == 113 && x.EmpresaID == empresa.ID);
            foreach (CampoEmpresa CampoEmpresa in uSalario)
            {
                salario = Somar(salario, VerifyValue(CampoEmpresa.Resposta));
            }

            double encargos = 0;
            List<CampoEmpresa> uEncargos = Repositorio.Get().CampoEmpresa.Listar(x => x.CampoID == 115 && x.EmpresaID == empresa.ID);
            foreach (CampoEmpresa CampoEmpresa in uEncargos)
            {
                encargos = Somar(encargos, VerifyValue(CampoEmpresa.Resposta));
            }

            double finalResposta2 = 0;
            finalResposta2 = Somar(finalResposta2, salario);
            finalResposta2 = Somar(finalResposta2, encargos);
            finalResposta2 = Dividir(finalResposta2, VerifyValue(uVendas.Resposta));
            finalResposta2 = Multiplicar(finalResposta2, 100);

            CampoEmpresa uResposta11_2 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 318 && x.EmpresaID == empresa.ID);
            uResposta11_2.Resposta = Math.Round(finalResposta2, 2).ToString() + "%";
            Repositorio.Get().CampoEmpresa.Alterar(uResposta11_2);

        }

        public void GetResposta12(Empresa empresa)
        {
            CampoEmpresa uCapitalDeGiro = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 278 && x.EmpresaID == empresa.ID);

            CampoEmpresa uResposta12 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 320 && x.EmpresaID == empresa.ID);
            uResposta12.Resposta = "R$" + Math.Round(VerifyValue(uCapitalDeGiro.Resposta), 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uResposta12);

        }

        public void GetResposta13(Empresa empresa)
        {
            double finalResposta = 0;

            CampoEmpresa resposta3 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 162 && x.EmpresaID == empresa.ID);

            finalResposta = Somar(finalResposta, VerifyValue(resposta3.Resposta));
            finalResposta = Multiplicar(finalResposta, 0.3);

            CampoEmpresa uResposta13 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 322 && x.EmpresaID == empresa.ID);
            uResposta13.Resposta = "R$" + Math.Round(finalResposta, 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uResposta13);

        }


        public double Somar(double valor1, double valor2)
        {
            double resposta = new double();

            resposta = valor1 + valor2;

            return resposta;
        }

        public double Subtrair(double valor1, double valor2)
        {
            double resposta = new double();

            resposta = valor1 - valor2;

            return resposta;
        }

        public double Dividir(double valor1, double valor2)
        {
            double resposta = new double();
            if (valor1 == 0 || valor2 == 0) return 0;
            resposta = valor1 / valor2;

            return resposta;
        }

        public double Multiplicar(double valor1, double valor2)
        {
            double resposta = new double();
            if (valor1 == 0 || valor2 == 0) return 0;

            resposta = valor1 * valor2;

            return resposta;
        }

        public double VerifyValue(string valor)
        {
            if (valor == null) return 0;
            return Double.Parse(valor.Replace("R$", "").Replace("%", ""));
        }
    }
}
