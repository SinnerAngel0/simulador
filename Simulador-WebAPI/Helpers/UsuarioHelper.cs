﻿using Simulador_DAO.Entities;
using Simulador_DAO.Facades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_WebAPI.Helpers
{
    public class UsuarioHelper
    {    
        public UsuarioHelper()
        {

        }
        public Object Builder(Usuario usuario)
        {
            List<Empresa> response = Repositorio.Get().Empresa.Listar(x => x.UsuarioID == usuario.ID);
            List<Object> empresas = new List<object>();
            foreach (Empresa empresa in response)
            {
                empresas.Add(EmpresaBuilder(empresa));
            }
            return new
            {
                usuario.ID,
                usuario.FullName,
                usuario.UserName,
                usuario.Email,
                usuario.Cpf,
                usuario.TipoTelefone,
                usuario.Telefone,
                empresas,
                usuario.Access
            };
        }

        public Object EmpresaBuilder(Empresa empresa)
        {
            return new
            {
                empresa.ID,
                empresa.NomeFantasia,
                empresa.Cnpj,
                empresa.Faturamento,
                empresa.Produto,
                empresa.RazaoSocial,
                empresa.TipoEmpresa
            };
        }

        public string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

    }
}
