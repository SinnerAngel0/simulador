﻿using Simulador_DAO.Entities;
using Simulador_DAO.Facades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_WebAPI.Helpers
{
    public class Etapa2Helper
    {

        public Etapa2Helper()
        {

        }

        public Object TabelaBuilder(Tabela tabela, Empresa empresa)
        {
            List<Campo> response = Repositorio.Get().Campo.Listar(x => x.TabelaID == tabela.ID);
            List<Object> campos = new List<object>();
            foreach (Campo campo in response)
            {
                campos.Add(CampoBuilder(campo, empresa));
            }
            return new
            {
                tabela.ID,
                tabela.Nome,
                campos

            };
        }

        public Object CampoBuilder(Campo campo, Empresa empresa)
        {
            List<Campo> response = Repositorio.Get().Campo.Listar(x => x.SuperCampoID == campo.ID);
            List<Object> subCampos = new List<object>();
            foreach (Campo model in response)
            {
                if (model.Tipo != Simulador_DAO.Enums.ENUMTipo.TEXT)
                {
                    List<CampoEmpresa> CampoEmpresas = Repositorio.Get().CampoEmpresa.Listar(x => x.CampoID == model.ID && x.EmpresaID == empresa.ID);
                    foreach (CampoEmpresa CampoEmpresa in CampoEmpresas)
                    {
                        subCampos.Add(CampoEmpresaBuilder(CampoEmpresa, empresa));
                    }
                }
                else
                {
                    subCampos.Add(CampoBuilder(model, empresa));
                }
            }
            return new
            {
                campo.ID,
                campo.Descricao,
                campo.Texto,
                campo.Tipo,
                subCampos
            };
        }

        public Object CampoEmpresaBuilder(CampoEmpresa CampoEmpresa, Empresa empresa)
        {
            List<Campo> response = Repositorio.Get().Campo.Listar(x => x.ID == CampoEmpresa.CampoID);
            List<Object> subCampos = new List<object>();
            foreach (Campo campo in response)
            {
                foreach (Campo campos2 in campo.SubCampos)
                {
                    subCampos.Add(CampoBuilder(campos2, empresa));
                }
            }
            return new
            {
                CampoEmpresa.ID,
                CampoEmpresa.Resposta,
                CampoEmpresa.Campo,
                subCampos
            };
        }

        public void GetDespesasVariaveis(Empresa empresa)
        {
            Campo materiaPrima = Repositorio.Get().Campo.Buscar(x => x.ID == 104);
            Campo suprimento = Repositorio.Get().Campo.Buscar(x => x.ID == 120);
            Campo mercadoriaVendida = Repositorio.Get().Campo.Buscar(x => x.ID == 122);
            Campo comissoesVenda = Repositorio.Get().Campo.Buscar(x => x.ID == 136);
            Campo impostosTaxa = Repositorio.Get().Campo.Buscar(x => x.ID == 138);
            Campo outrosCustosVariavel = Repositorio.Get().Campo.Buscar(x => x.ID == 72);

            double finalResposta = 0.00f;
            finalResposta = Somar(finalResposta, GetRespostas(materiaPrima, empresa));
            finalResposta = Somar(finalResposta, GetRespostas(suprimento, empresa));
            finalResposta = Somar(finalResposta, GetRespostas(mercadoriaVendida, empresa));
            finalResposta = Somar(finalResposta, GetRespostas(comissoesVenda, empresa));
            finalResposta = Somar(finalResposta, GetRespostas(impostosTaxa, empresa));
            finalResposta = Somar(finalResposta, GetRespostas(outrosCustosVariavel, empresa));

            CampoEmpresa eDespesasVariaveis = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 141 && x.EmpresaID == empresa.ID);
            eDespesasVariaveis.Resposta = finalResposta.ToString();
            Repositorio.Get().CampoEmpresa.Alterar(eDespesasVariaveis);
            
        }  

        public void GetDespesasFixas(Empresa empresa)
        {
            Campo aluguel = Repositorio.Get().Campo.Buscar(x => x.ID == 64);
            Campo despesasGerais = Repositorio.Get().Campo.Buscar(x => x.ID == 66);
            Campo gastosAdministrativos = Repositorio.Get().Campo.Buscar(x => x.ID == 68);
            Campo iPTU = Repositorio.Get().Campo.Buscar(x => x.ID == 70);
            Campo salarioProlabore = Repositorio.Get().Campo.Buscar(x => x.ID == 86);
            Campo encargosSociais = Repositorio.Get().Campo.Buscar(x => x.ID == 88);
            Campo despesasFixas = Repositorio.Get().Campo.Buscar(x => x.ID == 95);

            double finalResposta = 0.00f;
            finalResposta = Somar(finalResposta, GetRespostas(aluguel, empresa));
            finalResposta = Somar(finalResposta, GetRespostas(despesasGerais, empresa));
            finalResposta = Somar(finalResposta, GetRespostas(gastosAdministrativos, empresa));
            finalResposta = Somar(finalResposta, GetRespostas(iPTU, empresa));
            finalResposta = Somar(finalResposta, GetRespostas(salarioProlabore, empresa));
            finalResposta = Somar(finalResposta, GetRespostas(encargosSociais, empresa));
            finalResposta = Somar(finalResposta, GetRespostas(despesasFixas, empresa));

            CampoEmpresa eDespesasFixas = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 145 && x.EmpresaID == empresa.ID);
            eDespesasFixas.Resposta = finalResposta.ToString();
            Repositorio.Get().CampoEmpresa.Alterar(eDespesasFixas);

        }

        public void GetInvestimentos(Empresa empresa)
        {
            Campo parcelamento = Repositorio.Get().Campo.Buscar(x => x.ID == 99);
            Campo amortaizacaoAtual = Repositorio.Get().Campo.Buscar(x => x.ID == 97);
            Campo outros = Repositorio.Get().Campo.Buscar(x => x.ID == 101);

            double finalResposta = 0.00f;
            finalResposta = Somar(finalResposta, GetRespostas(parcelamento, empresa));
            finalResposta = Somar(finalResposta, GetRespostas(amortaizacaoAtual, empresa));
            finalResposta = Somar(finalResposta, GetRespostas(outros, empresa));

            CampoEmpresa eInvestimentos = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 147 && x.EmpresaID == empresa.ID);
            eInvestimentos.Resposta = finalResposta.ToString();
            Repositorio.Get().CampoEmpresa.Alterar(eInvestimentos);

        }

        public void GetMargemContribuicao(Empresa empresa)
        {
            Campo vendas = Repositorio.Get().Campo.Buscar(x => x.ID == 24);

            Campo materiaPrima = Repositorio.Get().Campo.Buscar(x => x.ID == 104);
            Campo suprimento = Repositorio.Get().Campo.Buscar(x => x.ID == 120);
            Campo mercadoriaVendida = Repositorio.Get().Campo.Buscar(x => x.ID == 122);
            Campo comissoesVenda = Repositorio.Get().Campo.Buscar(x => x.ID == 136);
            Campo impostosTaxa = Repositorio.Get().Campo.Buscar(x => x.ID == 138);
            Campo outrosCustosVariavel = Repositorio.Get().Campo.Buscar(x => x.ID == 72);

            double finalResposta = 0.00f;
            finalResposta = Somar(finalResposta, GetRespostas(vendas, empresa));
            finalResposta = Subtrair(finalResposta, GetRespostas(materiaPrima, empresa));
            finalResposta = Subtrair(finalResposta, GetRespostas(suprimento, empresa));
            finalResposta = Subtrair(finalResposta, GetRespostas(mercadoriaVendida, empresa));
            finalResposta = Subtrair(finalResposta, GetRespostas(comissoesVenda, empresa));
            finalResposta = Subtrair(finalResposta, GetRespostas(impostosTaxa, empresa));
            finalResposta = Subtrair(finalResposta, GetRespostas(outrosCustosVariavel, empresa));

            CampoEmpresa eMargem = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 143 && x.EmpresaID == empresa.ID);
            eMargem.Resposta = finalResposta.ToString();
            Repositorio.Get().CampoEmpresa.Alterar(eMargem);
        }

        public void GetCaixaDisponivel(Empresa empresa)
        {
            CampoEmpresa eVendasMedia = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 155 && x.EmpresaID == empresa.ID);
            CampoEmpresa eDespesasVariaveis = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 141 && x.EmpresaID == empresa.ID);
            CampoEmpresa eDespesasFixas = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 145 && x.EmpresaID == empresa.ID);
            CampoEmpresa eInvestimentos = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 147 && x.EmpresaID == empresa.ID);
            double intermediario = 0;
            intermediario = Somar(intermediario, (eVendasMedia.Resposta == null) ? 0.00f : Double.Parse(eVendasMedia.Resposta.Replace("R$", "")));
            intermediario = Subtrair(intermediario, (eDespesasVariaveis.Resposta == null) ? 0.00f : Double.Parse(eDespesasVariaveis.Resposta));
            intermediario = Subtrair(intermediario, (eDespesasFixas.Resposta == null) ? 0.00f : Double.Parse(eDespesasFixas.Resposta));

            double finalResposta = 0.00f;
            finalResposta = Somar(finalResposta, intermediario);
            finalResposta = Subtrair(finalResposta, (eInvestimentos.Resposta == null) ? 0.00f : Double.Parse(eInvestimentos.Resposta));
            finalResposta = Multiplicar(finalResposta, 12);

            CampoEmpresa eCaixaDisponivel = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 173 && x.EmpresaID == empresa.ID);
            eCaixaDisponivel.Resposta = Math.Round(finalResposta, 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(eCaixaDisponivel);

        }

        public void GetCapacidadePagamentoMensal(Empresa empresa)
        {
            Campo vendas = Repositorio.Get().Campo.Buscar(x => x.ID == 24);

            CampoEmpresa eDespesasVariaveis = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 141 && x.EmpresaID == empresa.ID);
            CampoEmpresa eDespesasFixas = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 145 && x.EmpresaID == empresa.ID);
            CampoEmpresa eInvestimentos = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 147 && x.EmpresaID == empresa.ID);


            double intermediario = 0;
            intermediario = Somar(GetRespostas(vendas, empresa), intermediario);
            intermediario = Subtrair(intermediario, (eDespesasVariaveis.Resposta == null) ? 0.00f : Double.Parse(eDespesasVariaveis.Resposta));
            intermediario = Subtrair(intermediario, (eDespesasFixas.Resposta == null) ? 0.00f : Double.Parse(eDespesasFixas.Resposta));

            double intermediario2 = 0;
            intermediario2 = Somar(intermediario2, intermediario);
            intermediario2 = Subtrair(intermediario2, (eInvestimentos.Resposta == null) ? 0.00f : Double.Parse(eInvestimentos.Resposta));

            double respostaFinal = 0;
            respostaFinal = Somar(respostaFinal, intermediario2);
            respostaFinal = Multiplicar(respostaFinal, 0.3);


            CampoEmpresa eCapacidadePagamentoMensal = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 149 && x.EmpresaID == empresa.ID);
            eCapacidadePagamentoMensal.Resposta = Math.Round(respostaFinal, 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(eCapacidadePagamentoMensal);
        }

        public void VendasMediaMensal(Empresa empresa)
        {
            Campo vendas = Repositorio.Get().Campo.Buscar(x => x.ID == 24);

            double finalResposta = 0.00f;

            finalResposta = Somar(GetRespostas(vendas, empresa), finalResposta);            

            CampoEmpresa eVendasMedia = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 155 && x.EmpresaID == empresa.ID);
            eVendasMedia.Resposta = Math.Round(finalResposta,2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(eVendasMedia);

        }

        public void VendaMinima(Empresa empresa)
        {
            Campo lucroDesejado = Repositorio.Get().Campo.Buscar(x => x.ID == 49);



            double finalResposta = 0.00f;

            finalResposta = Somar(GetRespostas(lucroDesejado, empresa), finalResposta);

            CampoEmpresa eVendaMinima = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 179 && x.EmpresaID == empresa.ID);
            eVendaMinima.Resposta = Math.Round(finalResposta, 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(eVendaMinima);

        }

        public void VendaMinimaMensal(Empresa empresa)
        {
            Campo venda = Repositorio.Get().Campo.Buscar(x => x.ID == 24);
            CampoEmpresa eDespesasVariaveis = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 141 && x.EmpresaID == empresa.ID);
            CampoEmpresa eDespesasFixas = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 145 && x.EmpresaID == empresa.ID);
            CampoEmpresa eInvestimentos = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 147 && x.EmpresaID == empresa.ID);


            double finalResposta = 0.00f;
            finalResposta = Somar(finalResposta, GetRespostas(venda, empresa));
            finalResposta = Subtrair(finalResposta, (eDespesasVariaveis.Resposta == null) ? 0.00f : Double.Parse(eDespesasVariaveis.Resposta));
            finalResposta = Subtrair(finalResposta, (eDespesasFixas.Resposta == null) ? 0.00f : Double.Parse(eDespesasFixas.Resposta));
            finalResposta = Subtrair(finalResposta, (eInvestimentos.Resposta == null) ? 0.00f : Double.Parse(eInvestimentos.Resposta));

           

        }

        public void GetLucroMensalDesejado(Empresa empresa)
        {
            Campo venda = Repositorio.Get().Campo.Buscar(x => x.ID == 24);

            CampoEmpresa eDespesasVariaveis = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 141 && x.EmpresaID == empresa.ID);
            CampoEmpresa eDespesasFixas = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 145 && x.EmpresaID == empresa.ID);
            CampoEmpresa eInvestimentos = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 147 && x.EmpresaID == empresa.ID);
            CampoEmpresa eMargem = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 143 && x.EmpresaID == empresa.ID);
            CampoEmpresa eVendaMinimaMensal = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 179 && x.EmpresaID == empresa.ID);

            double intermediario1 = 0;
            intermediario1 = Somar(intermediario1, (eVendaMinimaMensal.Resposta == null) ? 0.00f : Double.Parse(eVendaMinimaMensal.Resposta));
            intermediario1 = Somar(intermediario1, (eDespesasFixas.Resposta == null) ? 0.00f : Double.Parse(eDespesasFixas.Resposta));
            intermediario1 = Somar(intermediario1, (eInvestimentos.Resposta == null) ? 0.00f : Double.Parse(eInvestimentos.Resposta));

            double intermediario2 = 0;
            intermediario2 = Somar(intermediario2, GetRespostas(venda, empresa));
            intermediario2 = Subtrair(intermediario2, (eDespesasVariaveis.Resposta == null) ? 0.00f : Double.Parse(eDespesasVariaveis.Resposta));

            double finalResposta = 0.00f;
            finalResposta = Somar(finalResposta, intermediario1);
            finalResposta = Dividir(finalResposta, intermediario2);
            finalResposta = Multiplicar(finalResposta, GetRespostas(venda, empresa));

            CampoEmpresa eLucroMensalDesejado = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 175 && x.EmpresaID == empresa.ID);
            eLucroMensalDesejado.Resposta = Math.Round(finalResposta, 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(eLucroMensalDesejado);

            CampoEmpresa eVendaMinima = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 157 && x.EmpresaID == empresa.ID);
            eVendaMinima.Resposta = Math.Round(finalResposta, 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(eVendaMinima);
        }

        public void GetLucroMensalDesejadoPerc(Empresa empresa)
        {
            Campo lucroMensal = Repositorio.Get().Campo.Buscar(x => x.ID == 49);
            CampoEmpresa eLucroMensalDesejado = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 175 && x.EmpresaID == empresa.ID);

            double finalResposta = 0.00f;
            finalResposta = Somar(finalResposta, GetRespostas(lucroMensal, empresa));
            finalResposta = Dividir(finalResposta, (eLucroMensalDesejado.Resposta == null) ? 0.00f : Double.Parse(eLucroMensalDesejado.Resposta.Replace("R$", "")));
            finalResposta = Multiplicar(finalResposta, 100);

            CampoEmpresa eVendaDesejavelPerc = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 159 && x.EmpresaID == empresa.ID);
            eVendaDesejavelPerc.Resposta = Math.Round(finalResposta, 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(eVendaDesejavelPerc);
            CampoEmpresa eVendaDesejavelPerc2 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 177 && x.EmpresaID == empresa.ID);
            eVendaDesejavelPerc2.Resposta = Math.Round(finalResposta, 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(eVendaDesejavelPerc2);
        }

        public void GetPontoEquilibrio(Empresa empresa)
        {

            CampoEmpresa eDespesasFixas = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 145 && x.EmpresaID == empresa.ID);
            CampoEmpresa eInvestimentos = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 147 && x.EmpresaID == empresa.ID);
            CampoEmpresa eMargem = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 143 && x.EmpresaID == empresa.ID);

            Campo venda = Repositorio.Get().Campo.Buscar(x => x.ID == 24);

            double finalResposta = 0.00f;
            finalResposta = Somar(finalResposta, (eDespesasFixas.Resposta == null) ? 0.00f : Double.Parse(eDespesasFixas.Resposta));
            finalResposta = Somar(finalResposta, (eInvestimentos.Resposta == null) ? 0.00f : Double.Parse(eInvestimentos.Resposta));

            finalResposta = Dividir(finalResposta, (eMargem.Resposta == null) ? 0.00f : Double.Parse(eMargem.Resposta));
            finalResposta = Multiplicar(finalResposta, GetRespostas(venda, empresa));

            CampoEmpresa ePontoEquilibrio = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 169 && x.EmpresaID == empresa.ID);
            ePontoEquilibrio.Resposta = Math.Round(finalResposta,2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(ePontoEquilibrio);

        }        

        public void GetTaxaMarcacao(Empresa empresa)
        {
            Campo venda = Repositorio.Get().Campo.Buscar(x => x.ID == 24);
            Campo impostosTaxas = Repositorio.Get().Campo.Buscar(x => x.ID == 138);
            CampoEmpresa eVendaDesejavelPerc = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 159 && x.EmpresaID == empresa.ID);

            double intermediario1 = Multiplicar(100, GetRespostas(impostosTaxas, empresa));
            intermediario1 = Dividir(intermediario1, GetRespostas(venda, empresa));

            CampoEmpresa eDespesasFixas = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 145 && x.EmpresaID == empresa.ID);
            double intermediario2 = Multiplicar(100, (eDespesasFixas.Resposta == null) ? 0.00f : Double.Parse(eDespesasFixas.Resposta));
            intermediario2 = Dividir(intermediario2, GetRespostas(venda, empresa));

            CampoEmpresa eInvestimentos = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 147 && x.EmpresaID == empresa.ID);
            double intermediario3 = Multiplicar(100, (eInvestimentos.Resposta == null) ? 0.00f : Double.Parse(eInvestimentos.Resposta));
            intermediario3 = Dividir(intermediario3, GetRespostas(venda, empresa));

            Campo vendas = Repositorio.Get().Campo.Buscar(x => x.ID == 24);

            double finalRespostaMark = 100;

            finalRespostaMark = Subtrair(finalRespostaMark, intermediario1);
            finalRespostaMark = Subtrair(finalRespostaMark, intermediario2);
            finalRespostaMark = Subtrair(finalRespostaMark, intermediario3);
            var lucratividade = (eVendaDesejavelPerc.Resposta == null) ? 0.00f : Double.Parse(eVendaDesejavelPerc.Resposta.Replace("%", "").Replace("$", ""));
            finalRespostaMark = Subtrair(finalRespostaMark, lucratividade);

            double intermediario = 100;
            double finalResposta = 100;
            intermediario = Multiplicar(intermediario, finalRespostaMark);
            finalResposta = Dividir(finalResposta, intermediario);
            finalResposta = Multiplicar(finalResposta, 100);

            CampoEmpresa eTaxaMarcacao = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 163 && x.EmpresaID == empresa.ID);
            eTaxaMarcacao.Resposta = Math.Round(finalResposta,2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(eTaxaMarcacao);

        }

        public void GetProduto(Empresa empresa)
        {
            Campo produto = Repositorio.Get().Campo.Buscar(x => x.ID == 106);

            double finalResposta = 0.00f;
            finalResposta = Somar(finalResposta, GetRespostas(produto, empresa));
            CampoEmpresa eProduto = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 161 && x.EmpresaID == empresa.ID);
            eProduto.Resposta = Math.Round(finalResposta, 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(eProduto);
        }

        public void GetVenda(Empresa empresa)
        {
            CampoEmpresa eProduto = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 161 && x.EmpresaID == empresa.ID);
            CampoEmpresa eTaxaMarcacao = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 163 && x.EmpresaID == empresa.ID);

            double finalResposta = 0.00f;
            finalResposta = Somar(finalResposta, (eProduto.Resposta == null || Double.Parse(eProduto.Resposta.Replace("%", "").Replace("R$", "")) == 0) ? 10 : Double.Parse(eProduto.Resposta.Replace("%", "").Replace("R$", "")));
            finalResposta = Multiplicar(finalResposta, (eTaxaMarcacao.Resposta == null) ? 0.00f : Double.Parse(eTaxaMarcacao.Resposta.Replace("%", "").Replace("R$", "")));

            CampoEmpresa eValorVenda = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 171 && x.EmpresaID == empresa.ID);
            eValorVenda.Resposta = Math.Round(finalResposta,2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(eValorVenda);
        }

        public void GetAposAmortizacao(Empresa empresa)
        {
            CampoEmpresa eVendasMedia = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 155 && x.EmpresaID == empresa.ID);
            CampoEmpresa eDespesasVariaveis = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 141 && x.EmpresaID == empresa.ID);
            CampoEmpresa eDespesasFixas = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 145 && x.EmpresaID == empresa.ID);
            CampoEmpresa eInvestimentos = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 147 && x.EmpresaID == empresa.ID);

            double intermediario = 0;
            intermediario = Somar(intermediario, (eVendasMedia.Resposta == null) ? 0.00f : Double.Parse(eVendasMedia.Resposta.Replace("R$", "")));
            intermediario = Subtrair(intermediario, (eDespesasVariaveis.Resposta == null) ? 0.00f : Double.Parse(eDespesasVariaveis.Resposta));
            intermediario = Subtrair(intermediario, (eDespesasFixas.Resposta == null) ? 0.00f : Double.Parse(eDespesasFixas.Resposta));

            double intermediario2 = 0;
            intermediario2 = Somar(intermediario2, intermediario);
            intermediario2 = Subtrair(intermediario2, (eInvestimentos.Resposta == null) ? 0.00f : Double.Parse(eInvestimentos.Resposta));

            CampoEmpresa eAposAmortizacao = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 165 && x.EmpresaID == empresa.ID);
            eAposAmortizacao.Resposta = Math.Round(intermediario2, 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(eAposAmortizacao);
        }

        public void GetLucratividade(Empresa empresa)
        {

            CampoEmpresa eVendasMedia = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 155 && x.EmpresaID == empresa.ID);
            CampoEmpresa eDespesasVariaveis = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 141 && x.EmpresaID == empresa.ID);
            CampoEmpresa eDespesasFixas = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 145 && x.EmpresaID == empresa.ID);
            CampoEmpresa eInvestimentos = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 147 && x.EmpresaID == empresa.ID);

            double intermediario = 0;
            intermediario = Somar(intermediario, (eVendasMedia.Resposta == null) ? 0.00f : Double.Parse(eVendasMedia.Resposta.Replace("R$", "")));
            intermediario = Subtrair(intermediario, (eDespesasVariaveis.Resposta == null) ? 0.00f : Double.Parse(eDespesasVariaveis.Resposta));
            intermediario = Subtrair(intermediario, (eDespesasFixas.Resposta == null) ? 0.00f : Double.Parse(eDespesasFixas.Resposta));

            double intermediario2 = 0;
            intermediario2 = Somar(intermediario2, intermediario);
            intermediario2 = Subtrair(intermediario2, (eInvestimentos.Resposta == null) ? 0.00f : Double.Parse(eInvestimentos.Resposta));

            double finalResposta = 0.00f;
            finalResposta = Somar(finalResposta, intermediario2);
            finalResposta = Dividir(finalResposta, (eVendasMedia.Resposta == null) ? 0.00f : Double.Parse(eVendasMedia.Resposta.Replace("R$", "")));
            finalResposta = Multiplicar(finalResposta, 100);

            CampoEmpresa uLucartividade = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 167 && x.EmpresaID == empresa.ID);
            uLucartividade.Resposta = Math.Round(finalResposta, 2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(uLucartividade);
        }

         

       
        

        public double GetRespostas(Campo model, Empresa empresa)
        {
            double doubleResposta = 0;
            foreach (Campo resposta in model.SubCampos)
            {
                if (resposta.Tipo == Simulador_DAO.Enums.ENUMTipo.INPUT)
                {
                    List<CampoEmpresa> uRespostas = Repositorio.Get().CampoEmpresa.Listar(x => x.CampoID == resposta.ID && x.EmpresaID == empresa.ID);
                    foreach (CampoEmpresa uResposta in uRespostas)
                    {
                        var checkValor = (uResposta.Resposta == null || uResposta.Resposta == "") ? 0.00f : Double.Parse(uResposta.Resposta);
                        doubleResposta = Somar(doubleResposta, checkValor);
                    }
                }
            }

            return doubleResposta;
        }

        public double Somar(double valor1, double valor2)
        {
            double resposta = new double();

            resposta = valor1 + valor2;

            return resposta;
        }

        public double Subtrair(double valor1, double valor2)
        {
            double resposta = new double();

            resposta = valor1 - valor2;

            return resposta;
        }

        public double Dividir(double valor1, double valor2)
        {
            double resposta = new double();
            if (valor1 == 0 || valor2 == 0) return 0;
            resposta = valor1 / valor2;

            return resposta;
        }

        public double Multiplicar(double valor1, double valor2)
        {
            double resposta = new double();
            if (valor1 == 0 || valor2 == 0) return 0;

            resposta = valor1 * valor2;

            return resposta;
        }
    }
}
