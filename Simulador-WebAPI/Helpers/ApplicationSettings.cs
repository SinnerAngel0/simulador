﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_WebAPI.Helpers
{
    public class ApplicationSettings
    {
        public string JWT_Secret { get; set; }
        public string Client_URL { get; set; }
        public EmailConfig EmailConfig { get; set; }
        public string StoredFilesPath { get; set; }
    }

    public class EmailConfig
    {

        public string Host { get; set; }
        public string Port { get; set; }
        public string Client { get; set; }
        public string ClientPassword { get; set; }
    }

    public class TrocaSenha 
    {
        public string Senha { get; set; }

        public string SenhaNova { get; set; }
    }

}
