﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Policy;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using Simulador_WebAPI.Helpers;

namespace IESB_ConteudoWebAPI.Helpers
{
    public class FileHelper
    {
        public static IWebHostEnvironment _environment;
        private readonly ApplicationSettings _appSettings;
        public FileHelper(IOptions<ApplicationSettings> appSettings, IWebHostEnvironment environment)
        {
            _appSettings = appSettings.Value;
            _environment = environment;
        }

      /*  public String UploadImagem(FileUploadAPI objFile)
        {

            var folderName = Path.Combine(_environment.WebRootPath, "temp");
            var format = (objFile.files.FileName.Split(".").Length > 1) ? objFile.files.FileName.Split(".")[1].ToLower() : "png";
            var uniqueFileName = Guid.NewGuid().ToString() + "_" + objFile.files.FileName.Split(".")[0] + "." + format;
            var imagePath = folderName + @"/";
            var filePath = Path.Combine(imagePath + uniqueFileName);

            string[] filePaths = Directory.GetFiles(folderName, "*",
                                         SearchOption.TopDirectoryOnly);
            List<Object> imageFiles = new List<Object>();
            foreach (string filepath in filePaths)
            {
                if (Regex.IsMatch(filepath, @".jpg|.jpeg|.png|.gif$"))
                {
                    if (System.IO.File.Exists(filepath))
                    {
                        long length = new System.IO.FileInfo(filepath).Length;
                        var finalPath = filepath.Replace(_environment.WebRootPath, _appSettings.StoredFilesPath);
                        if (objFile.files.Length == length)
                        {
                            return finalPath;
                        }
                    }

                }
            }
            using (var fileStream = new FileStream(Path.Combine(folderName, uniqueFileName), FileMode.Create))
            {
                objFile.files.CopyTo(fileStream);
                fileStream.Flush();
                filePath = filePath.Replace(_environment.WebRootPath, _appSettings.StoredFilesPath);
                return filePath;
            }
        }


        public string BinaryToStrings(FileUploadAPI data)
        {
            using (var ms = new MemoryStream())
            {
                data.files.CopyTo(ms);
                var fileBytes = ms.ToArray();
                string s = Convert.ToBase64String(fileBytes);
                // act on the Base64 data
                return s;
            }
        } */



    }
}