﻿using Simulador_DAO.Entities;
using Simulador_DAO.Facades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_WebAPI.Helpers
{
    public class Etapa1Helper
    {
        public Etapa1Helper()
        {

        }

        public Object TabelaBuilder(Tabela tabela, Empresa empresa)
        {
            List<Campo> response = Repositorio.Get().Campo.Listar(x => x.TabelaID == tabela.ID);
            List<Object> campos = new List<object>();
            foreach(Campo campo in response)
            {
                campos.Add(CampoBuilder(campo, empresa));
            }
            return new
            {
                tabela.ID,
                tabela.Nome,
                campos

            };
        }

        public Object CampoBuilder(Campo campo, Empresa empresa)
        {
            List<Campo> response = Repositorio.Get().Campo.Listar(x => x.SuperCampoID == campo.ID);
            List<Object> subCampos = new List<object>();
            foreach(Campo model in response)
            {
                if(model.Tipo != Simulador_DAO.Enums.ENUMTipo.TEXT)
                {
                    List<CampoEmpresa> campoUsuarios = Repositorio.Get().CampoEmpresa.Listar(x => x.CampoID == model.ID && x.EmpresaID == empresa.ID);
                    foreach(CampoEmpresa campoUsuario in campoUsuarios)
                    {
                        subCampos.Add(CampoUsuarioBuilder(campoUsuario, empresa));
                    }
                }
                else
                {
                    subCampos.Add(CampoBuilder(model, empresa));
                }
            }
            return new
            {
                campo.ID,
                campo.Descricao,
                campo.Texto,
                campo.Tipo,
                subCampos
            };
        }

        public Object CampoUsuarioBuilder(CampoEmpresa campoEmpresa, Empresa empresa)
        {
            List<Campo> response = Repositorio.Get().Campo.Listar(x => x.ID == campoEmpresa.CampoID);
            List<Object> subCampos = new List<object>();
            foreach(Campo campo in response)
            {
                foreach(Campo campos2 in campo.SubCampos)
                {
                    subCampos.Add(CampoBuilder(campos2, empresa));
                }
            }
            return new
            {
                campoEmpresa.ID,
                campoEmpresa.Resposta,
                campoEmpresa.CampoID,
                subCampos
            };
        }

        public void Maturidade(Empresa empresa)
        {
            List<Campo> campos = new List<Campo>();
            campos.Add(Repositorio.Get().Campo.Buscar(x => x.ID == 2));
            campos.Add(Repositorio.Get().Campo.Buscar(x => x.ID == 13));
            campos.Add(Repositorio.Get().Campo.Buscar(x => x.ID == 27));
            campos.Add(Repositorio.Get().Campo.Buscar(x => x.ID == 38));
            campos.Add(Repositorio.Get().Campo.Buscar(x => x.ID == 52));
            campos.Add(Repositorio.Get().Campo.Buscar(x => x.ID == 75));
            campos.Add(Repositorio.Get().Campo.Buscar(x => x.ID == 125));

            double mat = 0;
            foreach (Campo superCampo in campos)
            {

               
                double percFix = 100.0 / campos.Count;


                CampoEmpresa campoUsuario1 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == superCampo.SubCampos[0].ID && x.EmpresaID == empresa.ID);
                if (campoUsuario1.Resposta == "2")
                {
                    mat += percFix;
                }
                if (campoUsuario1.Resposta == "1")
                {
                    mat += percFix / 2;
                }

            }

            CampoEmpresa maturiadade = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 151 && x.EmpresaID == empresa.ID);
            maturiadade.Resposta = Math.Round(mat,2).ToString();
            Repositorio.Get().CampoEmpresa.Alterar(maturiadade);
        }

        public void Satisfacao(Empresa empresa)
        {
            List<Campo> campos = new List<Campo>();
            campos.Add(Repositorio.Get().Campo.Buscar(x => x.ID == 7));
            campos.Add(Repositorio.Get().Campo.Buscar(x => x.ID == 18));
            campos.Add(Repositorio.Get().Campo.Buscar(x => x.ID == 32));
            campos.Add(Repositorio.Get().Campo.Buscar(x => x.ID == 43));
            campos.Add(Repositorio.Get().Campo.Buscar(x => x.ID == 57));
            campos.Add(Repositorio.Get().Campo.Buscar(x => x.ID == 114));
            campos.Add(Repositorio.Get().Campo.Buscar(x => x.ID == 130));
            double satis = 0;
            foreach (Campo superCampo in campos)
            {

                List<Campo> SubCampos = Repositorio.Get().Campo.Listar(x => x.SuperCampoID == superCampo.ID);
                double percFix = 100.0 / campos.Count;


                CampoEmpresa campoUsuario2 = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == superCampo.SubCampos[0].ID && x.EmpresaID == empresa.ID);
                if (campoUsuario2.Resposta == "0")
                {
                    satis += percFix;
                }
                if (campoUsuario2.Resposta == "1")
                {
                    satis += percFix / 2;
                }
                if (campoUsuario2.Resposta == "2")
                {
                    satis += percFix / 3;
                }

            }

            CampoEmpresa satisfacao = Repositorio.Get().CampoEmpresa.Buscar(x => x.CampoID == 153 && x.EmpresaID == empresa.ID);
            satisfacao.Resposta = Math.Round(satis,2).ToString() ;
            Repositorio.Get().CampoEmpresa.Alterar(satisfacao);
        }
    }
}
