﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Repositories
{
    public interface AbstractRepositoryBehavior<TEntity>
    {
        IQueryable<TEntity> GetAll();
        IQueryable<TEntity> Get(Func<TEntity, bool> predicate);
        TEntity Find(params object[] key);
        void Add(TEntity obj);
        void Update(TEntity obj);
        void Save();
        void Excluir(Func<TEntity, bool> predicate);

    }
}
