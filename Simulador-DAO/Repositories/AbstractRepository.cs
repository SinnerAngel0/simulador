﻿using Microsoft.EntityFrameworkCore;
using Simulador_DAO.EFConfig;
using Simulador_DAO.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Repositories
{
    public class AbstractRepository<TEntity> : IDisposable, AbstractRepositoryBehavior<TEntity> where TEntity : class
    {
        Contexto ctx;

        public AbstractRepository()
        {
            ctx = ContextoProvider.Get().GetContexto();
        }

        public void Add(TEntity obj)
        {
            (obj as AbstractEntity).DataCadastro = DateTime.Now;
            (obj as AbstractEntity).DataAlteracao = DateTime.Now;
            ctx.Set<TEntity>().Add(obj);
        }

        public void Excluir(Func<TEntity, bool> predicate)
        {
            ctx.Set<TEntity>().Where(predicate).ToList().ForEach(x => ctx.Set<TEntity>().Remove(x));
        }

        public TEntity Find(params object[] key)
        {
            return ctx.Set<TEntity>().Find(key);
        }

        public IQueryable<TEntity> Get(Func<TEntity, bool> predicate)
        {
            return ctx.Set<TEntity>().Where(predicate).AsQueryable();
        }

        public IQueryable<TEntity> GetAll()
        {
            return ctx.Set<TEntity>();
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        public void Update(TEntity obj)
        {
            (obj as AbstractEntity).DataAlteracao = DateTime.Now;
            ctx.Entry(obj).State = EntityState.Modified;
        }

        public void Dispose()
        {
            ctx.Dispose();
        }

    }
}
