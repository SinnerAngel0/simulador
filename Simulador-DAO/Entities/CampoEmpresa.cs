﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Entities
{
    [Table("campo_empresa")]
    public class CampoEmpresa :AbstractEntity
    {
        [ForeignKey("EmpresaID")]
        public int EmpresaID { get; set; }
        public Empresa Empresa { get; set; }
        [ForeignKey("CampoID")]
        public int CampoID { get; set; }
        public Campo Campo { get; set; }
        public string Resposta { get; set; }
    }
}
