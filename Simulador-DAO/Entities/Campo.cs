﻿using Simulador_DAO.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Entities
{
    [Table("campo")]
    public class Campo : AbstractEntity
    {
        public Campo()
        {
            SubCampos = new List<Campo>();
        }
        public string Descricao { get; set; }
        public string Texto { get; set; }
        public ENUMIdioma Idioma { get; set; }
        public ENUMTipo Tipo { get; set; }
        [ForeignKey("TabelaID")]
        public int? TabelaID { get; set; }
        public Tabela Tabela { get; set; }
        [ForeignKey("ResultadoID")]
        public int? ResultadoID { get; set; }
        public Resultado Resultado { get; set; }
        [ForeignKey("SuperCampoID")]
        public int? SuperCampoID { get; set; }
        public Campo SuperCampo { get; set; }
        public virtual List<Campo> SubCampos { get; set; }
    }
}
