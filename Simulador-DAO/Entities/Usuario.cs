﻿using Simulador_DAO.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Entities
{
    [Table("usuario")]
    public class Usuario : AbstractEntity
    {
        public Usuario()
        {
            Empresas = new List<Empresa>();
        }
        public string FullName { get; set; }
        public string Cpf { get; set; }        
        public string Email { get; set; }
        public string UserName { get; set; }
        public ENUMTipoTelefone TipoTelefone { get; set; }
        public string Telefone { get; set; }
        public string Senha { get; set; }
        public ENUMAccess Access { get; set; }
        public virtual List<Empresa> Empresas { get; set; }
    }
}
