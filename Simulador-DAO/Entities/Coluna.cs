﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Entities
{
    [Table("coluna")]
    public class Coluna : AbstractEntity
    {
        public string Nome { get; set; }
        [ForeignKey("TabelaID")]
        public int TabelaID { get; set; }
        public Tabela Tabela { get; set; }
    }
}
