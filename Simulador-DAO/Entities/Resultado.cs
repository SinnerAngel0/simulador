﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Entities
{
    [Table("resultado")]
    public class Resultado : AbstractEntity
    {
        public Resultado()
        {
            Campos = new List<Campo>();
        }
        public string Nome { get; set; }
        [ForeignKey("TabelaID")]
        public int TabelaID { get; set; }
        public virtual Tabela Tabela { get; set; }
        public virtual List<Campo> Campos { get; set; }
    }
}
