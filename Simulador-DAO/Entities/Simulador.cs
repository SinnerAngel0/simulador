﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Entities
{
    [Table("simulador")]
    public class Simulador : AbstractEntity
    {
        public Simulador()
        {
            Tabelas = new List<Tabela>();
        }
        public string Nome { get; set; }
        public virtual List<Tabela> Tabelas { get; set; }
    }
}
