﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Entities
{
    [Table("tabela")]
    public class Tabela : AbstractEntity
    {        
        public Tabela()
        {
            Colunas = new List<Coluna>();
            Campos = new List<Campo>();
        }
        public string Nome { get; set; }
        public virtual List<Coluna> Colunas { get; set; }
        [ForeignKey("SimuladorID")]
        public int SimuladorID { get; set; }
        public Simulador Simulador { get; set; }
        public virtual List<Campo> Campos { get; set; }
        public virtual Resultado Resultado { get; set; }

    }
}
