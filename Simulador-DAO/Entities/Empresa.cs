﻿using Simulador_DAO.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Entities
{
    [Table("empresa")]
    public class Empresa :AbstractEntity
    {
        [ForeignKey("UsuarioID")]
        public int UsuarioID { get; set; }
        public virtual Usuario usuario { get; set; }
        public string NomeFantasia { get; set; }
        public ENUMTipoEmpresa TipoEmpresa { get; set; }
        public string RazaoSocial { get; set; }
        public string Cnpj { get; set; }
        public string Produto { get; set; }
        public Double Faturamento { get; set; }
    }
}
