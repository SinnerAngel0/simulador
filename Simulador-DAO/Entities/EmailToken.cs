﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Entities
{
    [Table("email_token")]
    public class EmailToken : AbstractEntity
    {
        public string Email { get; set; }
        public string Token { get; set; }
    }

}
