﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Enums
{
    public enum ENUMTipoEmpresa
    {
        MEI,
        ME,
        EPP,
        PRURAL
    }
}
