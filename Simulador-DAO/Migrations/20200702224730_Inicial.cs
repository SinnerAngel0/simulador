﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Simulador_DAO.Migrations
{
    public partial class Inicial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "email_token",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    DataCadastro = table.Column<DateTime>(nullable: false),
                    DataAlteracao = table.Column<DateTime>(nullable: false),
                    Ativo = table.Column<bool>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    Token = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_email_token", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "simulador",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    DataCadastro = table.Column<DateTime>(nullable: false),
                    DataAlteracao = table.Column<DateTime>(nullable: false),
                    Ativo = table.Column<bool>(nullable: false),
                    Nome = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_simulador", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "usuario",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    DataCadastro = table.Column<DateTime>(nullable: false),
                    DataAlteracao = table.Column<DateTime>(nullable: false),
                    Ativo = table.Column<bool>(nullable: false),
                    FullName = table.Column<string>(nullable: true),
                    Cpf = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    UserName = table.Column<string>(nullable: true),
                    TipoTelefone = table.Column<int>(nullable: false),
                    Telefone = table.Column<string>(nullable: true),
                    Senha = table.Column<string>(nullable: true),
                    Access = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_usuario", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "tabela",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    DataCadastro = table.Column<DateTime>(nullable: false),
                    DataAlteracao = table.Column<DateTime>(nullable: false),
                    Ativo = table.Column<bool>(nullable: false),
                    Nome = table.Column<string>(nullable: true),
                    SimuladorID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tabela", x => x.ID);
                    table.ForeignKey(
                        name: "FK_tabela_simulador_SimuladorID",
                        column: x => x.SimuladorID,
                        principalTable: "simulador",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "empresa",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    DataCadastro = table.Column<DateTime>(nullable: false),
                    DataAlteracao = table.Column<DateTime>(nullable: false),
                    Ativo = table.Column<bool>(nullable: false),
                    UsuarioID = table.Column<int>(nullable: false),
                    NomeFantasia = table.Column<string>(nullable: true),
                    TipoEmpresa = table.Column<int>(nullable: false),
                    RazaoSocial = table.Column<string>(nullable: true),
                    Cnpj = table.Column<string>(nullable: true),
                    Produto = table.Column<string>(nullable: true),
                    Faturamento = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_empresa", x => x.ID);
                    table.ForeignKey(
                        name: "FK_empresa_usuario_UsuarioID",
                        column: x => x.UsuarioID,
                        principalTable: "usuario",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "coluna",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    DataCadastro = table.Column<DateTime>(nullable: false),
                    DataAlteracao = table.Column<DateTime>(nullable: false),
                    Ativo = table.Column<bool>(nullable: false),
                    Nome = table.Column<string>(nullable: true),
                    TabelaID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_coluna", x => x.ID);
                    table.ForeignKey(
                        name: "FK_coluna_tabela_TabelaID",
                        column: x => x.TabelaID,
                        principalTable: "tabela",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "resultado",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    DataCadastro = table.Column<DateTime>(nullable: false),
                    DataAlteracao = table.Column<DateTime>(nullable: false),
                    Ativo = table.Column<bool>(nullable: false),
                    Nome = table.Column<string>(nullable: true),
                    TabelaID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_resultado", x => x.ID);
                    table.ForeignKey(
                        name: "FK_resultado_tabela_TabelaID",
                        column: x => x.TabelaID,
                        principalTable: "tabela",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "campo",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    DataCadastro = table.Column<DateTime>(nullable: false),
                    DataAlteracao = table.Column<DateTime>(nullable: false),
                    Ativo = table.Column<bool>(nullable: false),
                    Descricao = table.Column<string>(nullable: true),
                    Texto = table.Column<string>(nullable: true),
                    Idioma = table.Column<int>(nullable: false),
                    Tipo = table.Column<int>(nullable: false),
                    TabelaID = table.Column<int>(nullable: true),
                    ResultadoID = table.Column<int>(nullable: true),
                    SuperCampoID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_campo", x => x.ID);
                    table.ForeignKey(
                        name: "FK_campo_resultado_ResultadoID",
                        column: x => x.ResultadoID,
                        principalTable: "resultado",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_campo_campo_SuperCampoID",
                        column: x => x.SuperCampoID,
                        principalTable: "campo",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_campo_tabela_TabelaID",
                        column: x => x.TabelaID,
                        principalTable: "tabela",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "campo_empresa",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    DataCadastro = table.Column<DateTime>(nullable: false),
                    DataAlteracao = table.Column<DateTime>(nullable: false),
                    Ativo = table.Column<bool>(nullable: false),
                    EmpresaID = table.Column<int>(nullable: false),
                    CampoID = table.Column<int>(nullable: false),
                    Resposta = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_campo_empresa", x => x.ID);
                    table.ForeignKey(
                        name: "FK_campo_empresa_campo_CampoID",
                        column: x => x.CampoID,
                        principalTable: "campo",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_campo_empresa_empresa_EmpresaID",
                        column: x => x.EmpresaID,
                        principalTable: "empresa",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "simulador",
                columns: new[] { "ID", "Ativo", "DataAlteracao", "DataCadastro", "Nome" },
                values: new object[] { 1, true, new DateTime(2020, 7, 2, 19, 47, 29, 137, DateTimeKind.Local).AddTicks(1488), new DateTime(2020, 7, 2, 19, 47, 29, 137, DateTimeKind.Local).AddTicks(1439), "Diagnóstico de gestão financeira" });

            migrationBuilder.InsertData(
                table: "usuario",
                columns: new[] { "ID", "Access", "Ativo", "Cpf", "DataAlteracao", "DataCadastro", "Email", "FullName", "Senha", "Telefone", "TipoTelefone", "UserName" },
                values: new object[] { 1, 0, true, "000.000.000-00", new DateTime(2020, 7, 2, 19, 47, 29, 132, DateTimeKind.Local).AddTicks(1277), new DateTime(2020, 7, 2, 19, 47, 29, 133, DateTimeKind.Local).AddTicks(8570), "gabrielcdmagalhaes@gmail.com", "Gabriel Cavalcanti D'Albuquerque Magalhães", "MTIzNDU2", "(61) 982046838", 0, "Gabriel Magalhães" });

            migrationBuilder.InsertData(
                table: "empresa",
                columns: new[] { "ID", "Ativo", "Cnpj", "DataAlteracao", "DataCadastro", "Faturamento", "NomeFantasia", "Produto", "RazaoSocial", "TipoEmpresa", "UsuarioID" },
                values: new object[] { 1, true, "00.000.000/0000-00", new DateTime(2020, 7, 2, 19, 47, 29, 136, DateTimeKind.Local).AddTicks(2038), new DateTime(2020, 7, 2, 19, 47, 29, 136, DateTimeKind.Local).AddTicks(1975), 2000.0, "Empresa A", "Lista de dados", "Lista de dados", 0, 1 });

            migrationBuilder.InsertData(
                table: "tabela",
                columns: new[] { "ID", "Ativo", "DataAlteracao", "DataCadastro", "Nome", "SimuladorID" },
                values: new object[,]
                {
                    { 1, true, new DateTime(2020, 7, 2, 19, 47, 29, 137, DateTimeKind.Local).AddTicks(7275), new DateTime(2020, 7, 2, 19, 47, 29, 137, DateTimeKind.Local).AddTicks(7242), "Etapa 1", 1 },
                    { 2, true, new DateTime(2020, 7, 2, 19, 47, 29, 137, DateTimeKind.Local).AddTicks(8347), new DateTime(2020, 7, 2, 19, 47, 29, 137, DateTimeKind.Local).AddTicks(8338), "Etapa 2", 1 },
                    { 3, true, new DateTime(2020, 7, 2, 19, 47, 29, 137, DateTimeKind.Local).AddTicks(8397), new DateTime(2020, 7, 2, 19, 47, 29, 137, DateTimeKind.Local).AddTicks(8393), "Etapa 3", 1 },
                    { 4, true, new DateTime(2020, 7, 2, 19, 47, 29, 137, DateTimeKind.Local).AddTicks(8407), new DateTime(2020, 7, 2, 19, 47, 29, 137, DateTimeKind.Local).AddTicks(8403), "Resulado", 1 }
                });

            migrationBuilder.InsertData(
                table: "campo",
                columns: new[] { "ID", "Ativo", "DataAlteracao", "DataCadastro", "Descricao", "Idioma", "ResultadoID", "SuperCampoID", "TabelaID", "Texto", "Tipo" },
                values: new object[,]
                {
                    { 1, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(3103), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(3070), "", 0, null, null, 1, "Fluxo de Caixa", 0 },
                    { 142, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4319), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4317), "", 0, null, null, 4, "Margem de contribuição", 0 },
                    { 172, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4394), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4392), "", 0, null, null, 4, "Caixa disponivel estimado durante os últimos 12 meses", 0 },
                    { 148, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4421), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4420), "", 0, null, null, 4, "Capacidade mensal de pagamento", 0 },
                    { 150, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4449), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4448), "", 0, null, null, 4, "Maturidade finaceira", 0 },
                    { 152, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4477), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4476), "", 0, null, null, 4, "Seu nível de satisfação", 0 },
                    { 154, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4505), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4504), "", 0, null, null, 4, "Média mensal de vendas", 0 },
                    { 156, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4534), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4532), "", 0, null, null, 4, "Venda mínima para obter um lucro mensal de lucratividade desejada", 0 },
                    { 174, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4562), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4561), "", 0, null, null, 4, "Lucro desejavel", 0 },
                    { 158, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4590), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4589), "", 0, null, null, 4, "Porcentagem do lucro que se deseja obter, com relações as vendas", 0 },
                    { 162, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4618), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4616), "", 0, null, null, 4, "Valor em que o custo de um produto é multiplicado para definição do seu preço de venda", 0 },
                    { 160, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4646), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4645), "", 0, null, null, 4, "Custo do produto para definir o preço de venda", 0 },
                    { 170, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4675), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4673), "", 0, null, null, 4, "Venda", 0 },
                    { 164, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4703), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4701), "", 0, null, null, 4, "Resultado após mortização", 0 },
                    { 166, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4730), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4728), "", 0, null, null, 4, "Lucratividade", 0 },
                    { 168, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4806), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4804), "", 0, null, null, 4, "Ponto de equilibrio", 0 },
                    { 146, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4291), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4289), "", 0, null, null, 4, "Total dos investimentos", 0 },
                    { 144, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4263), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4261), "", 0, null, null, 4, "Total das despesas fixas", 0 },
                    { 140, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4234), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4232), "", 0, null, null, 4, "Total das despesas variáveis", 0 },
                    { 178, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4866), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4864), "", 0, null, null, 4, "Venda", 0 },
                    { 124, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4000), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3998), "", 0, null, null, 3, "Preço de Venda", 0 },
                    { 108, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3914), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3912), "<b>Cálculo de Custos</b>, é a soma dos valores gastos para produzir o produto ou serviço do seu negócio, ou para adquirir o produto em questão.", 0, null, null, 3, "Estoque e Suprimentos", 0 },
                    { 103, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3826), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3796), "<b>Ativo Circulante</b> é uma referência aos bens e direitos que podem ser convertidos em dinheiro em curto prazo.", 0, null, null, 3, "", 0 },
                    { 26, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5033), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5030), "<b>Capital de giro</b> é uma parte do investimento que compõe uma reserva de recursos que serão utilizados para suprir as necessidades financeiras da empresa ao longo do tempo.", 0, null, null, 1, "Capital de Giro", 0 },
                    { 90, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3747), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3745), "", 0, null, null, 2, "Encargos Bancários ", 0 },
                    { 74, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3618), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3615), "<b>Imposto</b> todo o montante de dinheiro que os cidadãos de um país devem pagar ao Estado para garantir a funcionalidade de serviços públicos e coletivos. Na língua portuguesa, imposto também pode ser entendido como um adjetivo, no sentido de ser algo que se impôs.", 0, null, null, 2, "Encargos Administrativos", 0 },
                    { 63, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3250), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3246), "", 0, null, null, 2, "Local Físico", 0 },
                    { 51, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3069), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3040), "<b>Cálculo de Custos</b>, é a soma dos valores gastos para produzir o produto ou serviço do seu negócio, ou para adquirir o produto em questão.", 0, null, null, 2, "Cálculo do Custo", 0 },
                    { 176, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4838), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4836), "", 0, null, null, 4, "Para cobrir custos sem lucrar, o mínimo de vendas", 0 }
                });

            migrationBuilder.InsertData(
                table: "coluna",
                columns: new[] { "ID", "Ativo", "DataAlteracao", "DataCadastro", "Nome", "TabelaID" },
                values: new object[,]
                {
                    { 6, true, new DateTime(2020, 7, 2, 19, 47, 29, 138, DateTimeKind.Local).AddTicks(5271), new DateTime(2020, 7, 2, 19, 47, 29, 138, DateTimeKind.Local).AddTicks(5267), "Satisfação do Empresario", 1 },
                    { 5, true, new DateTime(2020, 7, 2, 19, 47, 29, 138, DateTimeKind.Local).AddTicks(5262), new DateTime(2020, 7, 2, 19, 47, 29, 138, DateTimeKind.Local).AddTicks(5258), "Resposta", 1 },
                    { 4, true, new DateTime(2020, 7, 2, 19, 47, 29, 138, DateTimeKind.Local).AddTicks(5254), new DateTime(2020, 7, 2, 19, 47, 29, 138, DateTimeKind.Local).AddTicks(5248), "Questões", 1 },
                    { 3, true, new DateTime(2020, 7, 2, 19, 47, 29, 138, DateTimeKind.Local).AddTicks(4764), new DateTime(2020, 7, 2, 19, 47, 29, 138, DateTimeKind.Local).AddTicks(4760), "Satisfação do Empresario", 1 },
                    { 2, true, new DateTime(2020, 7, 2, 19, 47, 29, 138, DateTimeKind.Local).AddTicks(4701), new DateTime(2020, 7, 2, 19, 47, 29, 138, DateTimeKind.Local).AddTicks(4693), "Resposta", 1 },
                    { 1, true, new DateTime(2020, 7, 2, 19, 47, 29, 138, DateTimeKind.Local).AddTicks(3732), new DateTime(2020, 7, 2, 19, 47, 29, 138, DateTimeKind.Local).AddTicks(3722), "Questões", 1 }
                });

            migrationBuilder.InsertData(
                table: "resultado",
                columns: new[] { "ID", "Ativo", "DataAlteracao", "DataCadastro", "Nome", "TabelaID" },
                values: new object[,]
                {
                    { 1, true, new DateTime(2020, 7, 2, 19, 47, 29, 138, DateTimeKind.Local).AddTicks(936), new DateTime(2020, 7, 2, 19, 47, 29, 138, DateTimeKind.Local).AddTicks(923), "Resultado Ferramentas de Gestão", 1 },
                    { 2, true, new DateTime(2020, 7, 2, 19, 47, 29, 138, DateTimeKind.Local).AddTicks(2103), new DateTime(2020, 7, 2, 19, 47, 29, 138, DateTimeKind.Local).AddTicks(2093), "Resultado DRE", 2 },
                    { 3, true, new DateTime(2020, 7, 2, 19, 47, 29, 138, DateTimeKind.Local).AddTicks(2160), new DateTime(2020, 7, 2, 19, 47, 29, 138, DateTimeKind.Local).AddTicks(2156), "Resultado Balanço Patrimonial", 3 }
                });

            migrationBuilder.InsertData(
                table: "campo",
                columns: new[] { "ID", "Ativo", "DataAlteracao", "DataCadastro", "Descricao", "Idioma", "ResultadoID", "SuperCampoID", "TabelaID", "Texto", "Tipo" },
                values: new object[,]
                {
                    { 2, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(3805), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(3799), "Diário de Caixa", 0, null, 1, null, "Você controla as entradas e saídas diárias de dinheiro?", 0 },
                    { 114, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3935), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3934), "", 0, null, 108, null, "E como você se sente com relação a isso?", 0 },
                    { 120, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3958), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3957), "Valor médio mensal", 0, null, 108, null, "Suprimentos", 0 },
                    { 122, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3965), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3964), "É o Valor médio mensal correspondente à entrega da venda (mercadoria, serviço prestado etc.).", 0, null, 108, null, "Custo da Mercadoria Vendida (CMV)", 0 },
                    { 125, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4069), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4067), "Cálculo do ganho unitário", 0, null, 124, null, "Você sabe qual é o seu ganho de dinheiro em cada produto ou serviço vendido?", 0 },
                    { 130, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4087), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4086), "", 0, null, 124, null, "E como você se sente com relação a isso?", 0 },
                    { 136, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4107), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4106), "Valor médio mensal", 0, null, 124, null, "Comissão sobre vendas", 0 },
                    { 138, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4114), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4113), "É o Valor médio mensal incidente sobre o montante das vendas.", 0, null, 124, null, "Impostos e taxas (cartão de crédito) sobre vendas", 0 },
                    { 141, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4237), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4236), "", 0, null, 140, null, "Media mensal", 3 },
                    { 145, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4266), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4265), "", 0, null, 144, null, "Media mensal", 3 },
                    { 147, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4294), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4293), "", 0, null, 146, null, "Media mensal", 3 },
                    { 143, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4366), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4364), "", 0, null, 142, null, "Media mensal", 3 },
                    { 173, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4397), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4396), "", 0, null, 172, null, "Media mensal", 3 },
                    { 149, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4425), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4424), "", 0, null, 148, null, "Media mensal", 3 },
                    { 151, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4453), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4452), "", 0, null, 150, null, "Media mensal", 3 },
                    { 153, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4481), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4480), "", 0, null, 152, null, "Media mensal", 3 },
                    { 155, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4509), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4508), "", 0, null, 154, null, "Media mensal", 3 },
                    { 157, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4538), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4537), "", 0, null, 156, null, "Media mensal", 3 },
                    { 175, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4566), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4565), "", 0, null, 174, null, "Media mensal", 3 },
                    { 159, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4594), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4593), "", 0, null, 158, null, "Media mensal", 3 },
                    { 163, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4621), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4620), "", 0, null, 162, null, "Media mensal", 3 },
                    { 161, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4650), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4649), "", 0, null, 160, null, "Media mensal", 3 },
                    { 171, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4678), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4677), "", 0, null, 170, null, "Media mensal", 3 },
                    { 165, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4706), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4705), "", 0, null, 164, null, "Media mensal", 3 },
                    { 167, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4733), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4732), "", 0, null, 166, null, "Media mensal", 3 },
                    { 169, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4810), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4809), "", 0, null, 168, null, "Media mensal", 3 },
                    { 109, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3918), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3917), "Gestão de Estoques", 0, null, 108, null, "Você tem um controle do estoque dos produtos e insumos?", 0 },
                    { 177, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4841), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4840), "", 0, null, 176, null, "Media mensal", 3 },
                    { 106, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3841), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3840), "", 0, null, 103, null, "Por quanto você compra o seu principal produto/serviço?", 0 },
                    { 101, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3802), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3800), "Valor médio mensal", 0, null, 90, null, "Outros", 0 },
                    { 7, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4406), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4405), "", 0, null, 1, null, "E como você se sente com relação a isso?", 0 },
                    { 13, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4427), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4426), "Controle do fluxo de caixa", 0, null, 1, null, "Você possui um controle de entradas, saídas e saldo de dinheiro para os meses futuros?", 0 },
                    { 18, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4526), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4525), "", 0, null, 1, null, "E como você se sente com relação a isso?", 0 },
                    { 24, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4552), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4550), "É o valor total/mensal dos produtos e serviços vendidos no último ano.", 0, null, 1, null, "Informe sua Média Mensal de Vendas.", 0 },
                    { 27, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5052), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5050), "Controle do capital de giro", 0, null, 26, null, "Você sabe quanto dinheiro deve ter em caixa para cobrir suas despesas enquanto não recebe os pagamentos?", 0 },
                    { 32, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5076), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5074), "", 0, null, 26, null, "E como você se sente com relação a isso?", 0 },
                    { 38, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5098), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5097), "Demonstrativo de resultados", 0, null, 26, null, "Você calcula o resultado (lucro ou prejuízo) do seu negócio periodicamente?", 0 },
                    { 43, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5116), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5115), "", 0, null, 26, null, "E como você se sente com relação a isso?", 0 },
                    { 49, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5141), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5140), "", 0, null, 26, null, "Qual é o seu lucro mensal desejado?", 0 },
                    { 52, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3084), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3082), "Demonstrativo de resultados", 0, null, 51, null, "Você calcula os gastos (custos e despesas) mensalmente?", 0 },
                    { 57, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3112), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3111), "", 0, null, 51, null, "E como você se sente com relação a isso?", 0 },
                    { 64, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3255), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3254), "Valor médio mensal", 0, null, 63, null, "Aluguel, condomínio, estrutura física", 0 },
                    { 66, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3407), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3404), "Valor médio mensal (Ex. energia elétrica, água etc.)", 0, null, 63, null, "Despesas gerais", 0 },
                    { 68, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3460), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3458), "Valor médio mensal", 0, null, 63, null, "Gastos administrativos", 0 },
                    { 70, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3509), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3506), "Valor médio mensal", 0, null, 63, null, "IPTU", 0 },
                    { 72, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3563), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3560), "Valor médio mensal", 0, null, 63, null, "Outros custos variáveis", 0 },
                    { 75, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3623), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3621), "Controle do pagamento de tributos", 0, null, 74, null, "Você controla as datas e valores de todos os tributos a serem pagos?", 0 },
                    { 80, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3648), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3646), "", 0, null, 74, null, "E como você se sente com relação a isso?", 0 },
                    { 86, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3677), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3675), "O pró-labore é a remuneração do dono ou sócio pelo trabalho que desempenha na empresa. / Valor médio mensal", 0, null, 74, null, "Salários e pró-labore", 0 },
                    { 88, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3687), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3685), "Valor médio mensal", 0, null, 74, null, "Encargos sociais", 0 },
                    { 91, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3753), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3751), "Valor médio mensal", 0, null, 90, null, "Parcelamentos", 0 },
                    { 93, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3763), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3761), "Valor médio mensal", 0, null, 90, null, "Amortização dos seus Investimentos", 0 },
                    { 95, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3773), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3771), "Valor médio mensal", 0, null, 90, null, "Despesas Fixas e os juros de empréstimos atuais", 0 },
                    { 97, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3783), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3781), "Valor médio mensal", 0, null, 90, null, "Amortização de Empréstimos Atual", 0 },
                    { 99, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3792), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3790), "Valor médio mensal", 0, null, 90, null, "Parcelamentos", 0 },
                    { 104, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3834), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3832), "Valor médio mensal", 0, null, 103, null, "Matéria-prima e produtos para venda", 0 },
                    { 179, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4869), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4868), "", 0, null, 178, null, "Media mensal", 3 }
                });

            migrationBuilder.InsertData(
                table: "campo",
                columns: new[] { "ID", "Ativo", "DataAlteracao", "DataCadastro", "Descricao", "Idioma", "ResultadoID", "SuperCampoID", "TabelaID", "Texto", "Tipo" },
                values: new object[,]
                {
                    { 3, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4370), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4365), "", 0, null, 2, null, "Selecione uma Resposta", 1 },
                    { 92, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3758), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3757), "", 0, null, 91, null, "Media mensal", 2 },
                    { 94, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3768), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3766), "", 0, null, 93, null, "Media mensal", 2 },
                    { 96, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3778), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3776), "", 0, null, 95, null, "Media mensal", 2 },
                    { 98, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3787), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3785), "", 0, null, 97, null, "Media mensal", 2 },
                    { 100, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3796), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3794), "", 0, null, 99, null, "Media mensal", 2 },
                    { 102, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3806), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3805), "", 0, null, 101, null, "Media mensal", 2 },
                    { 105, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3838), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3837), "", 0, null, 104, null, "Média mensal", 2 },
                    { 110, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3921), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3920), "", 0, null, 109, null, "Seleciona sua resposta", 1 },
                    { 115, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3942), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3940), "", 0, null, 114, null, "Selecione sua resposta", 1 },
                    { 121, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3961), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3960), "", 0, null, 120, null, "Média mensal", 2 },
                    { 123, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3968), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3967), "", 0, null, 122, null, "Média mensal", 2 },
                    { 126, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4072), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4071), "", 0, null, 125, null, "Seleciona sua resposta", 1 },
                    { 131, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4090), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4089), "", 0, null, 130, null, "Selecione sua resposta", 1 },
                    { 137, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4111), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4110), "", 0, null, 136, null, "Média mensal", 2 },
                    { 139, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4118), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4116), "", 0, null, 138, null, "Média mensal", 2 },
                    { 89, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3692), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3690), "", 0, null, 88, null, "Media mensal", 2 },
                    { 87, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3682), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3680), "", 0, null, 86, null, "Media mensal", 2 },
                    { 107, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3845), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3843), "", 0, null, 106, null, "Média mensal", 2 },
                    { 76, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3628), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3626), "", 0, null, 75, null, "Selecione sua resposta", 1 },
                    { 81, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3653), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3651), "", 0, null, 80, null, "Selecione sua resposta", 1 },
                    { 8, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4410), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4409), "", 0, null, 7, null, "Selecione uma Resposta", 1 },
                    { 14, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4431), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4430), "", 0, null, 13, null, "Selecione uma Resposta", 1 },
                    { 19, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4531), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4529), "", 0, null, 18, null, "Selecione uma Resposta", 1 },
                    { 25, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4556), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4554), "", 0, null, 24, null, "Média mensal", 2 },
                    { 33, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5079), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5078), "", 0, null, 32, null, "Selecione sua resposta", 1 },
                    { 39, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5102), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5100), "", 0, null, 38, null, "Selecione sua resposta", 1 },
                    { 44, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5119), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5118), "", 0, null, 43, null, "Selecione sua resposta", 1 },
                    { 28, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5056), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5055), "", 0, null, 27, null, "Selecione sua resposta", 1 },
                    { 50, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5145), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5143), "", 0, null, 49, null, "Media mensal", 2 },
                    { 71, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3515), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3513), "", 0, null, 70, null, "Media mensal", 2 },
                    { 69, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3466), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3464), "", 0, null, 68, null, "Media mensal", 2 },
                    { 67, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3412), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3410), "", 0, null, 66, null, "Media mensal", 2 },
                    { 73, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3568), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3566), "", 0, null, 72, null, "Media mensal", 2 },
                    { 58, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3117), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3115), "", 0, null, 57, null, "Selecione sua resposta", 1 },
                    { 53, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3092), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3090), "", 0, null, 52, null, "Seleciona sua resposta", 1 },
                    { 65, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3261), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3259), "", 0, null, 64, null, "Media mensal", 2 }
                });

            migrationBuilder.InsertData(
                table: "campo_empresa",
                columns: new[] { "ID", "Ativo", "CampoID", "DataAlteracao", "DataCadastro", "EmpresaID", "Resposta" },
                values: new object[,]
                {
                    { 55, true, 175, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4940), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4938), 1, null },
                    { 52, true, 169, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4932), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4930), 1, null },
                    { 51, true, 167, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4929), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4928), 1, null },
                    { 50, true, 165, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4926), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4925), 1, null },
                    { 53, true, 171, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4934), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4933), 1, null },
                    { 48, true, 161, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4921), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4919), 1, null },
                    { 49, true, 163, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4923), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4922), 1, null },
                    { 47, true, 159, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4918), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4917), 1, null },
                    { 46, true, 157, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4915), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4914), 1, null },
                    { 38, true, 141, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4893), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4891), 1, null },
                    { 44, true, 153, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4910), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4909), 1, null },
                    { 43, true, 151, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4907), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4906), 1, null },
                    { 42, true, 149, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4904), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4903), 1, null },
                    { 54, true, 173, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4937), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4936), 1, null },
                    { 39, true, 143, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4896), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4895), 1, null },
                    { 41, true, 147, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4902), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4901), 1, null },
                    { 40, true, 145, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4899), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4898), 1, null },
                    { 56, true, 177, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4942), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4941), 1, null },
                    { 45, true, 155, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4913), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4911), 1, null },
                    { 57, true, 179, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4945), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4944), 1, null }
                });

            migrationBuilder.InsertData(
                table: "campo",
                columns: new[] { "ID", "Ativo", "DataAlteracao", "DataCadastro", "Descricao", "Idioma", "ResultadoID", "SuperCampoID", "TabelaID", "Texto", "Tipo" },
                values: new object[,]
                {
                    { 4, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4396), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4395), "", 0, null, 3, null, "Não, apenas vejo quanto tenho de dinheiro no final do dia.", 0 },
                    { 128, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4080), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4079), "", 0, null, 126, null, "Tenho um conhecimento aproximado sobre o ganho que cada produto / serviço traz.", 0 },
                    { 46, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5129), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5127), "", 0, null, 44, null, "Bastante satisfeito.", 0 },
                    { 47, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5133), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5131), "", 0, null, 44, null, "Pouco satisfeito.", 0 },
                    { 48, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5137), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5135), "", 0, null, 44, null, "Nada satisfeito.", 0 },
                    { 127, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4076), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4075), "", 0, null, 126, null, "Não, não sei qual é o meu ganho naquilo que vendo.", 0 },
                    { 54, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3097), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3095), "", 0, null, 53, null, "Não, não sei quais são os custos da empresa.", 0 },
                    { 55, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3102), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3100), "", 0, null, 53, null, "Tenho noção de qual é o valor dos custos e despesas mensais da empresa, mas não tenho um controle preciso.", 0 },
                    { 56, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3107), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3105), "", 0, null, 53, null, "Sim, registro qual o valor dos custos e despesas mensalmente, sabendo quanto foi o desembolso no período.", 0 },
                    { 59, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3121), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3120), "", 0, null, 58, null, "Totalmente satisfeito.", 0 },
                    { 60, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3126), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3124), "", 0, null, 58, null, "Bastante satisfeito.", 0 },
                    { 61, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3131), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3129), "", 0, null, 58, null, "Pouco satisfeito.", 0 },
                    { 62, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3136), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3134), "", 0, null, 58, null, "Nada satisfeito.", 0 },
                    { 42, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5112), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5111), "faturamento", 0, null, 39, null, "Sim, calculo o resultado da minha empresa relacionando os meus custos e despesas com a receita.", 0 },
                    { 119, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3955), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3954), "", 0, null, 115, null, "Nada satisfeito.", 0 },
                    { 117, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3948), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3947), "", 0, null, 115, null, "Bastante satisfeito.", 0 },
                    { 116, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3945), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3944), "", 0, null, 115, null, "Totalmente satisfeito.", 0 },
                    { 77, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3633), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3631), "", 0, null, 76, null, "Não, não tenho controle sobre o pagamento de tributos.", 0 },
                    { 78, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3638), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3636), "", 0, null, 76, null, "Sei quando devem ser pagos alguns tributos, mas não tenho controle sobre quando cada um deve ser pago.", 0 },
                    { 79, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3643), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3641), "", 0, null, 76, null, "Sim, possuo controle formal e registro de quando cada tributo deve ser pago, assim como registro os pagamentos.", 0 },
                    { 113, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3932), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3931), "", 0, null, 110, null, "Sim, registro quanto tenho em estoque e faço seu controle periodicamente.", 0 },
                    { 82, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3658), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3656), "", 0, null, 81, null, "Totalmente satisfeito.", 0 },
                    { 83, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3662), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3660), "", 0, null, 81, null, "Bastante satisfeito.", 0 },
                    { 84, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3667), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3666), "", 0, null, 81, null, "Pouco satisfeito.", 0 },
                    { 85, true, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3672), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3670), "", 0, null, 81, null, "Nada satisfeito.", 0 },
                    { 112, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3928), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3927), "", 0, null, 110, null, "Sei de cabeça, mas não tenho um controle formal.", 0 },
                    { 111, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3925), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3924), "", 0, null, 110, null, "Não, não tenho este controle.", 0 },
                    { 118, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3951), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(3950), "", 0, null, 115, null, "Pouco satisfeito.", 0 },
                    { 41, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5108), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5107), "", 0, null, 39, null, "Tenho uma noção de qual é o resultado da empresa em determinado tempo.", 0 },
                    { 45, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5124), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5123), "", 0, null, 44, null, "Totalmente satisfeito.", 0 },
                    { 129, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4083), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4082), "", 0, null, 126, null, "Sim, calculo periodicamente o ganho de cada produto / serviço.", 0 },
                    { 5, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4399), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4398), "", 0, null, 3, null, "Tenho um controle informal das entradas e saídas de dinheiro.", 0 },
                    { 6, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4403), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4402), "", 0, null, 3, null, "Sim, possuo um controle formalizado e detalhado das entradas e saídas de dinheiro.", 0 },
                    { 9, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4414), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4413), "", 0, null, 8, null, "Totalmente satisfeito.", 0 },
                    { 10, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4417), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4416), "", 0, null, 8, null, "Bastante satisfeito.", 0 },
                    { 11, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4421), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4420), "", 0, null, 8, null, "Pouco satisfeito.", 0 },
                    { 12, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4424), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4423), "", 0, null, 8, null, "Nada satisfeito.", 0 },
                    { 135, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4104), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4103), "", 0, null, 131, null, "Nada satisfeito.", 0 },
                    { 15, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4434), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4433), "", 0, null, 14, null, "Não tenho este controle.", 0 },
                    { 16, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4438), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4437), "", 0, null, 14, null, "Tenho um controle não formalizado e/ou que não prevê como estará o saldo no futuro.", 0 },
                    { 17, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4446), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4445), "", 0, null, 14, null, "Sim, possuo planilhas para controlar o Fluxo de Caixa e visualizo ao longo do tempo quando terei falta ou sobra de dinheiro.", 0 },
                    { 134, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4100), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4099), "", 0, null, 131, null, "Pouco satisfeito.", 0 },
                    { 20, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4535), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4534), "", 0, null, 19, null, "Totalmente satisfeito.", 0 },
                    { 40, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5105), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5104), "", 0, null, 39, null, "Não, não sei qual foi o resultado da empresa no período.", 0 },
                    { 22, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4543), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4542), "", 0, null, 19, null, "Pouco satisfeito.", 0 },
                    { 21, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4538), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4537), "", 0, null, 19, null, "Bastante satisfeito.", 0 },
                    { 133, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4097), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4096), "", 0, null, 131, null, "Bastante satisfeito.", 0 },
                    { 132, true, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4094), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4092), "", 0, null, 131, null, "Totalmente satisfeito.", 0 },
                    { 29, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5060), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5059), "", 0, null, 28, null, "Não faço este cálculo regularmente.", 0 },
                    { 30, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5064), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5063), "", 0, null, 28, null, "Tenho noção de quais são minhas despesas, mas não sei quanto dinheiro preciso ter em caixa.", 0 },
                    { 31, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5072), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5070), "", 0, null, 28, null, "Sim, calculo qual o valor que a empresa precisa ter em caixa para que não precise pedir dinheiro emprestado.", 0 },
                    { 37, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5094), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5093), "", 0, null, 33, null, "Nada satisfeito.", 0 },
                    { 23, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4547), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(4546), "", 0, null, 19, null, "Nada satisfeito.", 0 },
                    { 34, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5083), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5081), "", 0, null, 33, null, "Totalmente satisfeito.", 0 },
                    { 35, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5087), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5085), "", 0, null, 33, null, "Bastante satisfeito.", 0 },
                    { 36, true, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5091), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5089), "", 0, null, 33, null, "Pouco satisfeito.", 0 }
                });

            migrationBuilder.InsertData(
                table: "campo_empresa",
                columns: new[] { "ID", "Ativo", "CampoID", "DataAlteracao", "DataCadastro", "EmpresaID", "Resposta" },
                values: new object[,]
                {
                    { 31, true, 115, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4181), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4180), 1, null },
                    { 28, true, 105, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4173), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4172), 1, null },
                    { 29, true, 107, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4176), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4175), 1, null },
                    { 27, true, 102, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(4036), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(4034), 1, null },
                    { 34, true, 126, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4189), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4188), 1, null },
                    { 33, true, 123, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4187), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4185), 1, null },
                    { 30, true, 110, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4179), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4178), 1, null },
                    { 32, true, 121, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4184), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4183), 1, null },
                    { 35, true, 131, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4192), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4191), 1, null },
                    { 26, true, 100, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(4031), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(4029), 1, null },
                    { 12, true, 58, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3876), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3874), 1, null },
                    { 24, true, 96, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(4024), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(4023), 1, null },
                    { 1, true, 3, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5978), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(5973), 1, null },
                    { 2, true, 8, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(6506), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(6501), 1, null },
                    { 3, true, 14, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(6542), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(6540), 1, null },
                    { 4, true, 19, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(6544), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(6543), 1, null },
                    { 5, true, 25, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(6547), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(6546), 1, null },
                    { 6, true, 28, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(6550), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(6549), 1, null },
                    { 7, true, 33, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(6552), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(6551), 1, null },
                    { 8, true, 39, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(6555), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(6554), 1, null },
                    { 9, true, 44, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(6558), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(6557), 1, null },
                    { 10, true, 50, new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(6560), new DateTime(2020, 7, 2, 19, 47, 29, 147, DateTimeKind.Local).AddTicks(6559), 1, null },
                    { 11, true, 53, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3871), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3868), 1, null },
                    { 36, true, 137, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4195), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4194), 1, null },
                    { 13, true, 65, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3880), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3878), 1, null },
                    { 14, true, 67, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3883), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3881), 1, null },
                    { 15, true, 69, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3887), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3885), 1, null },
                    { 16, true, 71, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3891), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3889), 1, null },
                    { 17, true, 73, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3997), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(3994), 1, null },
                    { 18, true, 76, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(4002), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(4000), 1, null },
                    { 19, true, 81, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(4006), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(4004), 1, null },
                    { 20, true, 87, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(4010), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(4009), 1, null },
                    { 21, true, 89, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(4014), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(4013), 1, null },
                    { 22, true, 92, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(4018), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(4017), 1, null },
                    { 23, true, 94, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(4021), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(4020), 1, null },
                    { 25, true, 98, new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(4027), new DateTime(2020, 7, 2, 19, 47, 29, 158, DateTimeKind.Local).AddTicks(4026), 1, null },
                    { 37, true, 139, new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4197), new DateTime(2020, 7, 2, 19, 47, 29, 164, DateTimeKind.Local).AddTicks(4196), 1, null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_campo_ResultadoID",
                table: "campo",
                column: "ResultadoID");

            migrationBuilder.CreateIndex(
                name: "IX_campo_SuperCampoID",
                table: "campo",
                column: "SuperCampoID");

            migrationBuilder.CreateIndex(
                name: "IX_campo_TabelaID",
                table: "campo",
                column: "TabelaID");

            migrationBuilder.CreateIndex(
                name: "IX_campo_empresa_CampoID",
                table: "campo_empresa",
                column: "CampoID");

            migrationBuilder.CreateIndex(
                name: "IX_campo_empresa_EmpresaID",
                table: "campo_empresa",
                column: "EmpresaID");

            migrationBuilder.CreateIndex(
                name: "IX_coluna_TabelaID",
                table: "coluna",
                column: "TabelaID");

            migrationBuilder.CreateIndex(
                name: "IX_empresa_UsuarioID",
                table: "empresa",
                column: "UsuarioID");

            migrationBuilder.CreateIndex(
                name: "IX_resultado_TabelaID",
                table: "resultado",
                column: "TabelaID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_tabela_SimuladorID",
                table: "tabela",
                column: "SimuladorID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "campo_empresa");

            migrationBuilder.DropTable(
                name: "coluna");

            migrationBuilder.DropTable(
                name: "email_token");

            migrationBuilder.DropTable(
                name: "campo");

            migrationBuilder.DropTable(
                name: "empresa");

            migrationBuilder.DropTable(
                name: "resultado");

            migrationBuilder.DropTable(
                name: "usuario");

            migrationBuilder.DropTable(
                name: "tabela");

            migrationBuilder.DropTable(
                name: "simulador");
        }
    }
}
