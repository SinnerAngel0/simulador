﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.EFConfig
{
    public class ContextoProvider
    {
        private static ContextoProvider instance;
        private Contexto ctx;
        public Contexto GetContexto()
        {
            ctx = ctx == null ? new Contexto() : ctx;
            return ctx;
        }

        public static ContextoProvider Get()
        {
            instance = instance == null ? new ContextoProvider() : instance;
            return instance;
        }

    }
}
