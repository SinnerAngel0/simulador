﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Simulador_DAO.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.EFConfig
{
    public class Contexto : DbContext
    {
        private string connectionString;
        public Contexto() : base()
        {
            var builder = new ConfigurationBuilder();
            builder.AddJsonFile("appsettings.json", optional: false);

            var configuration = builder.Build();

            connectionString = configuration.GetConnectionString("DefaultConnection").ToString();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql(connectionString);
            optionsBuilder.EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Campo>()
                .HasOne(x => x.SuperCampo)
                .WithMany(x => x.SubCampos)
                .HasForeignKey(e => e.SuperCampoID);

            modelBuilder.Entity<Empresa>()
                .HasMany<CampoEmpresa>()
                .WithOne(x => x.Empresa)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();

            modelBuilder.Entity<Campo>()
                .HasMany<CampoEmpresa>()
                .WithOne(x => x.Campo)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();



            //Pre Building Entities

            modelBuilder.Entity<Usuario>()
               .HasData(new Usuario
               {
                   ID = 1,
                   Ativo = true,
                   FullName = "Gabriel Cavalcanti D'Albuquerque Magalhães",
                   UserName = "Gabriel Magalhães",
                   Email = "gabrielcdmagalhaes@gmail.com",
                   Telefone = "(61) 982046838",
                   TipoTelefone = Enums.ENUMTipoTelefone.CELULAR,
                   Cpf = "000.000.000-00",
                   Senha = "MTIzNDU2",
                   Access = Enums.ENUMAccess.USUARIO,
                   DataAlteracao = DateTime.Now,
                   DataCadastro = DateTime.Now
               }) ;

           modelBuilder.Entity<Empresa>()
                .HasData(new Empresa 
                {
                    ID = 1,
                    Ativo = true,
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    NomeFantasia = "Empresa A",
                    Cnpj = "00.000.000/0000-00",
                    RazaoSocial = "Lista de dados",
                    TipoEmpresa = Enums.ENUMTipoEmpresa.MEI,
                    Produto = "Lista de dados",
                    Faturamento = 2000.00,
                    UsuarioID = 1
                });


            modelBuilder.Entity<Simulador>()
                .HasData(new Simulador
                {
                    ID = 1,
                    Ativo = true,
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    Nome = "Diagnóstico de gestão financeira"
                });

            modelBuilder.Entity<Tabela>()
           .HasData(new Tabela
           {
               ID = 1,
               Ativo = true,
               DataCadastro = DateTime.Now,
               DataAlteracao = DateTime.Now,
               Nome = "Etapa 1",
               SimuladorID = 1
           },
           new Tabela
           {
               ID = 2,
               Ativo = true,
               DataCadastro = DateTime.Now,
               DataAlteracao = DateTime.Now,
               Nome = "Etapa 2",
               SimuladorID = 1
           }, 
           new Tabela
           {
               ID = 3,
               Ativo = true,
               DataCadastro = DateTime.Now,
               DataAlteracao = DateTime.Now,
               Nome = "Etapa 3",
               SimuladorID = 1
           },
           new Tabela
           {
               ID = 4,
               Ativo = true,
               DataCadastro = DateTime.Now,
               DataAlteracao = DateTime.Now,
               Nome = "Resulado",
               SimuladorID = 1
           });
            modelBuilder.Entity<Resultado>()
                .HasData(new Resultado
                {
                    ID = 1,
                    Ativo = true,
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    Nome = "Resultado Ferramentas de Gestão",
                    TabelaID = 1,
                },
                new Resultado
                {
                    ID = 2,
                    Ativo = true,
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    Nome = "Resultado DRE",
                    TabelaID = 2,
                },
                new Resultado
                {
                    ID = 3,
                    Ativo = true,
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    Nome = "Resultado Balanço Patrimonial",
                    TabelaID = 3,
                });

            modelBuilder.Entity<Coluna>()
              .HasData(new Coluna
              {
                  ID = 1,
                  Ativo = true,
                  DataCadastro = DateTime.Now,
                  DataAlteracao = DateTime.Now,
                  Nome = "Questões",
                  TabelaID = 1
              },
              new Coluna
              {
                  ID = 2,
                  Ativo = true,
                  DataCadastro = DateTime.Now,
                  DataAlteracao = DateTime.Now,
                  Nome = "Resposta",
                  TabelaID = 1
              },
              new Coluna
              {
                  ID = 3,
                  Ativo = true,
                  DataCadastro = DateTime.Now,
                  DataAlteracao = DateTime.Now,
                  Nome = "Satisfação do Empresario",
                  TabelaID = 1
              });

            modelBuilder.Entity<Coluna>()
          .HasData(new Coluna
          {
              ID = 4,
              Ativo = true,
              DataCadastro = DateTime.Now,
              DataAlteracao = DateTime.Now,
              Nome = "Questões",
              TabelaID = 1
          },
          new Coluna
          {
              ID = 5,
              Ativo = true,
              DataCadastro = DateTime.Now,
              DataAlteracao = DateTime.Now,
              Nome = "Resposta",
              TabelaID = 1
          },
          new Coluna
          {
              ID = 6,
              Ativo = true,
              DataCadastro = DateTime.Now,
              DataAlteracao = DateTime.Now,
              Nome = "Satisfação do Empresario",
              TabelaID = 1
          });

            Etapa1ModelBuilder.Seed(modelBuilder);
            Etapa2ModelBuilder.Seed(modelBuilder);
            Etapa3ModelBuilder.Seed(modelBuilder);


            modelBuilder.Entity<Campo>()
            .HasData(new Campo
            {
                ID = 140,
                Ativo = true,
                Texto = "Total das despesas variáveis",
                Descricao = "",
                DataCadastro = DateTime.Now,
                DataAlteracao = DateTime.Now,
                Tipo = Enums.ENUMTipo.TEXT,
                TabelaID = 4
            },
            new Campo
            {
                ID = 141,
                Ativo = true,
                Texto = "Media mensal",
                Descricao = "",
                DataCadastro = DateTime.Now,
                DataAlteracao = DateTime.Now,
                Tipo = Enums.ENUMTipo.RESPOSTA,
                SuperCampoID = 140,
            });

            

            modelBuilder.Entity<Campo>()
         .HasData(new Campo
         {
             ID = 144,
                Ativo = true,
                Texto = "Total das despesas fixas",
                Descricao = "",
                DataCadastro = DateTime.Now,
                DataAlteracao = DateTime.Now,
                Tipo = Enums.ENUMTipo.TEXT,
             TabelaID = 4
         },
           new Campo
           {
               ID = 145,
               Ativo = true,
               Texto = "Media mensal",
               Descricao = "",
               DataCadastro = DateTime.Now,
               DataAlteracao = DateTime.Now,
               Tipo = Enums.ENUMTipo.RESPOSTA,
               SuperCampoID = 144,
           });

            modelBuilder.Entity<Campo>()
          .HasData(new Campo
          {
              ID = 146,
                Ativo = true,
                Texto = "Total dos investimentos",
                Descricao = "",
                DataCadastro = DateTime.Now,
                DataAlteracao = DateTime.Now,
                Tipo = Enums.ENUMTipo.TEXT,
              TabelaID = 4
          },
                new Campo
                {
                    ID = 147,
                    Ativo = true,
                    Texto = "Media mensal",
                    Descricao = "",
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    Tipo = Enums.ENUMTipo.RESPOSTA,
                    SuperCampoID = 146,
                });

            modelBuilder.Entity<Campo>()
            .HasData(new Campo
            {
                ID = 142,
                Ativo = true,
                Texto = "Margem de contribuição",
                Descricao = "",
                DataCadastro = DateTime.Now,
                DataAlteracao = DateTime.Now,
                Tipo = Enums.ENUMTipo.TEXT,
                TabelaID = 4
            },
            new Campo
            {
                ID = 143,
                Ativo = true,
                Texto = "Media mensal",
                Descricao = "",
                DataCadastro = DateTime.Now,
                DataAlteracao = DateTime.Now,
                Tipo = Enums.ENUMTipo.RESPOSTA,
                SuperCampoID = 142,
            });

            modelBuilder.Entity<Campo>()
           .HasData(new Campo
           {
               ID = 172,
               Ativo = true,
               Texto = "Caixa disponivel estimado durante os últimos 12 meses",
               Descricao = "",
               DataCadastro = DateTime.Now,
               DataAlteracao = DateTime.Now,
               Tipo = Enums.ENUMTipo.TEXT,
               TabelaID = 4
           },
           new Campo
           {
               ID = 173,
               Ativo = true,
               Texto = "Media mensal",
               Descricao = "",
               DataCadastro = DateTime.Now,
               DataAlteracao = DateTime.Now,
               Tipo = Enums.ENUMTipo.RESPOSTA,
               SuperCampoID = 172,
           });

            modelBuilder.Entity<Campo>()
         .HasData(new Campo
         {
             ID = 148,
             Ativo = true,
             Texto = "Capacidade mensal de pagamento",
             Descricao = "",
             DataCadastro = DateTime.Now,
             DataAlteracao = DateTime.Now,
             Tipo = Enums.ENUMTipo.TEXT,
             TabelaID = 4
         },
               new Campo
               {
                   ID = 149,
                   Ativo = true,
                   Texto = "Media mensal",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Tipo = Enums.ENUMTipo.RESPOSTA,
                   SuperCampoID = 148,
               });

            modelBuilder.Entity<Campo>()
         .HasData(new Campo
         {
             ID = 150,
             Ativo = true,
             Texto = "Maturidade finaceira",
             Descricao = "",
             DataCadastro = DateTime.Now,
             DataAlteracao = DateTime.Now,
             Tipo = Enums.ENUMTipo.TEXT,
             TabelaID = 4
         },
               new Campo
               {
                   ID = 151,
                   Ativo = true,
                   Texto = "Media mensal",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Tipo = Enums.ENUMTipo.RESPOSTA,
                   SuperCampoID = 150,
               });

            modelBuilder.Entity<Campo>()
         .HasData(new Campo
         {
             ID = 152,
             Ativo = true,
             Texto = "Seu nível de satisfação",
             Descricao = "",
             DataCadastro = DateTime.Now,
             DataAlteracao = DateTime.Now,
             Tipo = Enums.ENUMTipo.TEXT,
             TabelaID = 4
         },
               new Campo
               {
                   ID = 153,
                   Ativo = true,
                   Texto = "Media mensal",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Tipo = Enums.ENUMTipo.RESPOSTA,
                   SuperCampoID = 152,
               });

            modelBuilder.Entity<Campo>()
         .HasData(new Campo
         {
             ID = 154,
             Ativo = true,
             Texto = "Média mensal de vendas",
             Descricao = "",
             DataCadastro = DateTime.Now,
             DataAlteracao = DateTime.Now,
             Tipo = Enums.ENUMTipo.TEXT,
             TabelaID = 4
         },
               new Campo
               {
                   ID = 155,
                   Ativo = true,
                   Texto = "Media mensal",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Tipo = Enums.ENUMTipo.RESPOSTA,
                   SuperCampoID = 154,
               });            

            modelBuilder.Entity<Campo>()
         .HasData(new Campo
         {
             ID = 156,
             Ativo = true,
             Texto = "Venda mínima para obter um lucro mensal de lucratividade desejada",
             Descricao = "",
             DataCadastro = DateTime.Now,
             DataAlteracao = DateTime.Now,
             Tipo = Enums.ENUMTipo.TEXT,
             TabelaID = 4
         },
               new Campo
               {
                   ID = 157,
                   Ativo = true,
                   Texto = "Media mensal",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Tipo = Enums.ENUMTipo.RESPOSTA,
                   SuperCampoID = 156,
               });

            modelBuilder.Entity<Campo>()
           .HasData(new Campo
           {
               ID = 174,
               Ativo = true,
               Texto = "Lucro desejavel",
               Descricao = "",
               DataCadastro = DateTime.Now,
               DataAlteracao = DateTime.Now,
               Tipo = Enums.ENUMTipo.TEXT,
               TabelaID = 4
           },
           new Campo
           {
               ID = 175,
               Ativo = true,
               Texto = "Media mensal",
               Descricao = "",
               DataCadastro = DateTime.Now,
               DataAlteracao = DateTime.Now,
               Tipo = Enums.ENUMTipo.RESPOSTA,
               SuperCampoID = 174,
           });

            modelBuilder.Entity<Campo>()
         .HasData(new Campo
         {
             ID = 158,
             Ativo = true,
             Texto = "Porcentagem do lucro que se deseja obter, com relações as vendas",
             Descricao = "",
             DataCadastro = DateTime.Now,
             DataAlteracao = DateTime.Now,
             Tipo = Enums.ENUMTipo.TEXT,
             TabelaID = 4
         },
               new Campo
               {
                   ID = 159,
                   Ativo = true,
                   Texto = "Media mensal",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Tipo = Enums.ENUMTipo.RESPOSTA,
                   SuperCampoID = 158,
               });

            modelBuilder.Entity<Campo>()
         .HasData(new Campo
         {
             ID = 162,
             Ativo = true,
             Texto = "Valor em que o custo de um produto é multiplicado para definição do seu preço de venda",
             Descricao = "",
             DataCadastro = DateTime.Now,
             DataAlteracao = DateTime.Now,
             Tipo = Enums.ENUMTipo.TEXT,
             TabelaID = 4
         },
               new Campo
               {
                   ID = 163,
                   Ativo = true,
                   Texto = "Media mensal",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Tipo = Enums.ENUMTipo.RESPOSTA,
                   SuperCampoID = 162,
               });

            modelBuilder.Entity<Campo>()
         .HasData(new Campo
         {
             ID = 160,
             Ativo = true,
             Texto = "Custo do produto para definir o preço de venda",
             Descricao = "",
             DataCadastro = DateTime.Now,
             DataAlteracao = DateTime.Now,
             Tipo = Enums.ENUMTipo.TEXT,
             TabelaID = 4
         },
               new Campo
               {
                   ID = 161,
                   Ativo = true,
                   Texto = "Media mensal",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Tipo = Enums.ENUMTipo.RESPOSTA,
                   SuperCampoID = 160,
               });            

            modelBuilder.Entity<Campo>()
       .HasData(new Campo
       {
           ID = 170,
           Ativo = true,
           Texto = "Venda",
           Descricao = "",
           DataCadastro = DateTime.Now,
           DataAlteracao = DateTime.Now,
           Tipo = Enums.ENUMTipo.TEXT,
           TabelaID = 4
       },
             new Campo
             {
                 ID = 171,
                 Ativo = true,
                 Texto = "Media mensal",
                 Descricao = "",
                 DataCadastro = DateTime.Now,
                 DataAlteracao = DateTime.Now,
                 Tipo = Enums.ENUMTipo.RESPOSTA,
                 SuperCampoID = 170,
             });

            modelBuilder.Entity<Campo>()
        .HasData(new Campo
        {
            ID = 164,
            Ativo = true,
            Texto = "Resultado após mortização",
            Descricao = "",
            DataCadastro = DateTime.Now,
            DataAlteracao = DateTime.Now,
            Tipo = Enums.ENUMTipo.TEXT,
            TabelaID = 4
        },
              new Campo
              {
                  ID = 165,
                  Ativo = true,
                  Texto = "Media mensal",
                  Descricao = "",
                  DataCadastro = DateTime.Now,
                  DataAlteracao = DateTime.Now,
                  Tipo = Enums.ENUMTipo.RESPOSTA,
                  SuperCampoID = 164,
              });

            modelBuilder.Entity<Campo>()
       .HasData(new Campo
       {
           ID = 166,
           Ativo = true,
           Texto = "Lucratividade",
           Descricao = "",
           DataCadastro = DateTime.Now,
           DataAlteracao = DateTime.Now,
           Tipo = Enums.ENUMTipo.TEXT,
           TabelaID = 4
       },
             new Campo
             {
                 ID = 167,
                 Ativo = true,
                 Texto = "Media mensal",
                 Descricao = "",
                 DataCadastro = DateTime.Now,
                 DataAlteracao = DateTime.Now,
                 Tipo = Enums.ENUMTipo.RESPOSTA,
                 SuperCampoID = 166,
             });

            modelBuilder.Entity<Campo>()
       .HasData(new Campo
       {
           ID = 168,
           Ativo = true,
           Texto = "Ponto de equilibrio",
           Descricao = "",
           DataCadastro = DateTime.Now,
           DataAlteracao = DateTime.Now,
           Tipo = Enums.ENUMTipo.TEXT,
           TabelaID = 4
       },
             new Campo
             {
                 ID = 169,
                 Ativo = true,
                 Texto = "Media mensal",
                 Descricao = "",
                 DataCadastro = DateTime.Now,
                 DataAlteracao = DateTime.Now,
                 Tipo = Enums.ENUMTipo.RESPOSTA,
                 SuperCampoID = 168,
             });

            modelBuilder.Entity<Campo>()
         .HasData(new Campo
         {
             ID = 176,
             Ativo = true,
             Texto = "Para cobrir custos sem lucrar, o mínimo de vendas",
             Descricao = "",
             DataCadastro = DateTime.Now,
             DataAlteracao = DateTime.Now,
             Tipo = Enums.ENUMTipo.TEXT,
             TabelaID = 4
         },
               new Campo
               {
                   ID = 177,
                   Ativo = true,
                   Texto = "Media mensal",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Tipo = Enums.ENUMTipo.RESPOSTA,
                   SuperCampoID = 176
               });

            modelBuilder.Entity<Campo>()
       .HasData(new Campo
       {
           ID = 178,
           Ativo = true,
           Texto = "Venda",
           Descricao = "",
           DataCadastro = DateTime.Now,
           DataAlteracao = DateTime.Now,
           Tipo = Enums.ENUMTipo.TEXT,
           TabelaID = 4
       },
             new Campo
             {
                 ID = 179,
                 Ativo = true,
                 Texto = "Media mensal",
                 Descricao = "",
                 DataCadastro = DateTime.Now,
                 DataAlteracao = DateTime.Now,
                 Tipo = Enums.ENUMTipo.RESPOSTA,
                 SuperCampoID = 178,
             });









            modelBuilder.Entity<CampoEmpresa>()
                   .HasData(new CampoEmpresa
                   {
                       ID = 38,
                       Ativo = true,
                       DataCadastro = DateTime.Now,
                       DataAlteracao = DateTime.Now,
                       EmpresaID = 1,
                       CampoID = 141
                   },
                   new CampoEmpresa
                   {
                       ID = 39,
                       Ativo = true,
                       DataCadastro = DateTime.Now,
                       DataAlteracao = DateTime.Now,
                       EmpresaID = 1,
                       CampoID = 143
                   },
                   new CampoEmpresa
                   {
                       ID = 40,
                       Ativo = true,
                       DataCadastro = DateTime.Now,
                       DataAlteracao = DateTime.Now,
                       EmpresaID = 1,
                       CampoID = 145
                   },
                   new CampoEmpresa
                   {
                       ID = 41,
                       Ativo = true,
                       DataCadastro = DateTime.Now,
                       DataAlteracao = DateTime.Now,
                       EmpresaID = 1,
                       CampoID = 147
                   },
                   new CampoEmpresa
                   {
                       ID = 42,
                       Ativo = true,
                       DataCadastro = DateTime.Now,
                       DataAlteracao = DateTime.Now,
                       EmpresaID = 1,
                       CampoID = 149
                   },
                   new CampoEmpresa
                   {
                       ID = 43,
                       Ativo = true,
                       DataCadastro = DateTime.Now,
                       DataAlteracao = DateTime.Now,
                       EmpresaID = 1,
                       CampoID = 151
                   },
                   new CampoEmpresa
                   {
                       ID = 44,
                       Ativo = true,
                       DataCadastro = DateTime.Now,
                       DataAlteracao = DateTime.Now,
                       EmpresaID = 1,
                       CampoID = 153
                   },
                   new CampoEmpresa
                   {
                       ID = 45,
                       Ativo = true,
                       DataCadastro = DateTime.Now,
                       DataAlteracao = DateTime.Now,
                       EmpresaID = 1,
                       CampoID = 155
                   },
                   new CampoEmpresa
                   {
                       ID = 46,
                       Ativo = true,
                       DataCadastro = DateTime.Now,
                       DataAlteracao = DateTime.Now,
                       EmpresaID = 1,
                       CampoID = 157
                   },
                   new CampoEmpresa
                   {
                       ID = 47,
                       Ativo = true,
                       DataCadastro = DateTime.Now,
                       DataAlteracao = DateTime.Now,
                       EmpresaID = 1,
                       CampoID = 159
                   },
                   new CampoEmpresa
                   {
                       ID = 48,
                       Ativo = true,
                       DataCadastro = DateTime.Now,
                       DataAlteracao = DateTime.Now,
                       EmpresaID = 1,
                       CampoID = 161
                   },
                   new CampoEmpresa
                   {
                       ID = 49,
                       Ativo = true,
                       DataCadastro = DateTime.Now,
                       DataAlteracao = DateTime.Now,
                       EmpresaID = 1,
                       CampoID = 163
                   },
            new CampoEmpresa
            {
                ID = 50,
                Ativo = true,
                DataCadastro = DateTime.Now,
                DataAlteracao = DateTime.Now,
                EmpresaID = 1,
                CampoID = 165
            },
            new CampoEmpresa
            {
                ID = 51,
                Ativo = true,
                DataCadastro = DateTime.Now,
                DataAlteracao = DateTime.Now,
                EmpresaID = 1,
                CampoID = 167
            },
            new CampoEmpresa
            {
                ID = 52,
                Ativo = true,
                DataCadastro = DateTime.Now,
                DataAlteracao = DateTime.Now,
                EmpresaID = 1,
                CampoID = 169
            },
             new CampoEmpresa
             {
                 ID = 53,
                 Ativo = true,
                 DataCadastro = DateTime.Now,
                 DataAlteracao = DateTime.Now,
                 EmpresaID = 1,
                 CampoID = 171
             },
             new CampoEmpresa
             {
                 ID = 54,
                 Ativo = true,
                 DataCadastro = DateTime.Now,
                 DataAlteracao = DateTime.Now,
                 EmpresaID = 1,
                 CampoID = 173
             },
             new CampoEmpresa
             {
                 ID = 55,
                 Ativo = true,
                 DataCadastro = DateTime.Now,
                 DataAlteracao = DateTime.Now,
                 EmpresaID = 1,
                 CampoID = 175
             },
             new CampoEmpresa
             {
                 ID = 56,
                 Ativo = true,
                 DataCadastro = DateTime.Now,
                 DataAlteracao = DateTime.Now,
                 EmpresaID = 1,
                 CampoID = 177
             },
            new CampoEmpresa
            {
                ID = 57,
                Ativo = true,
                DataCadastro = DateTime.Now,
                DataAlteracao = DateTime.Now,
                EmpresaID = 1,
                CampoID = 179
            });



        }

        public DbSet<Campo> Campo { get; set; }
        public DbSet<Empresa> Empresa { get; set; }
        public DbSet<CampoEmpresa> CampoEmpresa { get; set; }
        public DbSet<Coluna> Coluna { get; set; }
        public DbSet<Resultado> Resultado { get; set; }
        public DbSet<Simulador> Simulador { get; set; }
        public DbSet<Tabela> Tabela { get; set; }
        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<EmailToken> EmailToken { get; set; }

    }
}
