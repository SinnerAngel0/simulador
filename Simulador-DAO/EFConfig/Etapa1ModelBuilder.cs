﻿using Microsoft.EntityFrameworkCore;
using Simulador_DAO.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.EFConfig
{
    public static class Etapa1ModelBuilder
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Campo>()
                  .HasData(new Campo
                  {
                      ID = 1,
                      Ativo = true,
                      Texto = "Fluxo de Caixa",
                      Descricao = "",
                      DataCadastro = DateTime.Now,
                      DataAlteracao = DateTime.Now,
                      Tipo = Enums.ENUMTipo.TEXT,
                      TabelaID = 1
                  },
                  new Campo
                  {
                      ID = 2,
                      Ativo = true,
                      Texto = "Você controla as entradas e saídas diárias de dinheiro?",
                      Descricao = "Diário de Caixa",
                      DataCadastro = DateTime.Now,
                      DataAlteracao = DateTime.Now,
                      Idioma = Enums.ENUMIdioma.PORTUGUES,
                      Tipo = Enums.ENUMTipo.TEXT,
                      SuperCampoID = 1,
                  },
                  new Campo
                  {
                      ID = 3,
                      Ativo = true,
                      Texto = "Selecione uma Resposta",
                      Descricao = "",
                      DataCadastro = DateTime.Now,
                      DataAlteracao = DateTime.Now,
                      Idioma = Enums.ENUMIdioma.PORTUGUES,
                      Tipo = Enums.ENUMTipo.COMBO,
                      SuperCampoID = 2,
                  },
                  new Campo
                  {
                      ID = 4,
                      Ativo = true,
                      Texto = "Não, apenas vejo quanto tenho de dinheiro no final do dia.",
                      Descricao = "",
                      DataCadastro = DateTime.Now,
                      DataAlteracao = DateTime.Now,
                      Idioma = Enums.ENUMIdioma.PORTUGUES,
                      Tipo = Enums.ENUMTipo.TEXT,
                      SuperCampoID = 3,
                  },
                 new Campo
                 {
                     ID = 5,
                     Ativo = true,
                     Texto = "Tenho um controle informal das entradas e saídas de dinheiro.",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Idioma = Enums.ENUMIdioma.PORTUGUES,
                     Tipo = Enums.ENUMTipo.TEXT,
                     SuperCampoID = 3,
                 },
                 new Campo
                 {
                     ID = 6,
                     Ativo = true,
                     Texto = "Sim, possuo um controle formalizado e detalhado das entradas e saídas de dinheiro.",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Idioma = Enums.ENUMIdioma.PORTUGUES,
                     Tipo = Enums.ENUMTipo.TEXT,
                     SuperCampoID = 3,
                 },                
                 new Campo
                 {
                     ID = 7,
                     Ativo = true,
                     Texto = "E como você se sente com relação a isso?",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Idioma = Enums.ENUMIdioma.PORTUGUES,
                     Tipo = Enums.ENUMTipo.TEXT,
                     SuperCampoID = 1,
                 },
                 new Campo
                 {
                     ID = 8,
                     Ativo = true,
                     Texto = "Selecione uma Resposta",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Idioma = Enums.ENUMIdioma.PORTUGUES,
                     Tipo = Enums.ENUMTipo.COMBO,
                     SuperCampoID = 7,
                 },
                 new Campo
                 {
                     ID = 9,
                     Ativo = true,
                     Texto = "Totalmente satisfeito.",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Idioma = Enums.ENUMIdioma.PORTUGUES,
                     Tipo = Enums.ENUMTipo.TEXT,
                     SuperCampoID = 8,
                 },
                 new Campo
                 {
                     ID = 10,
                     Ativo = true,
                     Texto = "Bastante satisfeito.",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Idioma = Enums.ENUMIdioma.PORTUGUES,
                     Tipo = Enums.ENUMTipo.TEXT,
                     SuperCampoID = 8,
                 },
                  new Campo
                  {
                      ID = 11,
                      Ativo = true,
                      Texto = "Pouco satisfeito.",
                      Descricao = "",
                      DataCadastro = DateTime.Now,
                      DataAlteracao = DateTime.Now,
                      Idioma = Enums.ENUMIdioma.PORTUGUES,
                      Tipo = Enums.ENUMTipo.TEXT,
                      SuperCampoID = 8,
                  },
                  new Campo
                  {
                      ID = 12,
                      Ativo = true,
                      Texto = "Nada satisfeito.",
                      Descricao = "",
                      DataCadastro = DateTime.Now,
                      DataAlteracao = DateTime.Now,
                      Idioma = Enums.ENUMIdioma.PORTUGUES,
                      Tipo = Enums.ENUMTipo.TEXT,
                      SuperCampoID = 8,
                  },
                  new Campo
                  {
                      ID = 13,
                      Ativo = true,
                      Texto = "Você possui um controle de entradas, saídas e saldo de dinheiro para os meses futuros?",
                      Descricao = "Controle do fluxo de caixa",
                      DataCadastro = DateTime.Now,
                      DataAlteracao = DateTime.Now,
                      Idioma = Enums.ENUMIdioma.PORTUGUES,
                      Tipo = Enums.ENUMTipo.TEXT,
                      SuperCampoID = 1,
                  },
                  new Campo
                  {
                      ID = 14,
                      Ativo = true,
                      Texto = "Selecione uma Resposta",
                      Descricao = "",
                      DataCadastro = DateTime.Now,
                      DataAlteracao = DateTime.Now,
                      Idioma = Enums.ENUMIdioma.PORTUGUES,
                      Tipo = Enums.ENUMTipo.COMBO,
                      SuperCampoID = 13,
                  },
                  new Campo
                  {
                      ID = 15,
                      Ativo = true,
                      Texto = "Não tenho este controle.",
                      Descricao = "",
                      DataCadastro = DateTime.Now,
                      DataAlteracao = DateTime.Now,
                      Idioma = Enums.ENUMIdioma.PORTUGUES,
                      Tipo = Enums.ENUMTipo.TEXT,
                      SuperCampoID = 14,
                  },
                 new Campo
                 {
                     ID = 16,
                     Ativo = true,
                     Texto = "Tenho um controle não formalizado e/ou que não prevê como estará o saldo no futuro.",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Idioma = Enums.ENUMIdioma.PORTUGUES,
                     Tipo = Enums.ENUMTipo.TEXT,
                     SuperCampoID = 14,
                 },
                 new Campo
                 {
                     ID = 17,
                     Ativo = true,
                     Texto = "Sim, possuo planilhas para controlar o Fluxo de Caixa e visualizo ao longo do tempo quando terei falta ou sobra de dinheiro.",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Idioma = Enums.ENUMIdioma.PORTUGUES,
                     Tipo = Enums.ENUMTipo.TEXT,
                     SuperCampoID = 14,
                 },
                 new Campo
                 {
                     ID = 18,
                     Ativo = true,
                     Texto = "E como você se sente com relação a isso?",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Idioma = Enums.ENUMIdioma.PORTUGUES,
                     Tipo = Enums.ENUMTipo.TEXT,
                     SuperCampoID = 1,
                 },
                 new Campo
                 {
                     ID = 19,
                     Ativo = true,
                     Texto = "Selecione uma Resposta",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Idioma = Enums.ENUMIdioma.PORTUGUES,
                     Tipo = Enums.ENUMTipo.COMBO,
                     SuperCampoID = 18,
                 },
                 new Campo
                 {
                     ID = 20,
                     Ativo = true,
                     Texto = "Totalmente satisfeito.",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Idioma = Enums.ENUMIdioma.PORTUGUES,
                     Tipo = Enums.ENUMTipo.TEXT,
                     SuperCampoID = 19,
                 },
                 new Campo
                 {
                     ID = 21,
                     Ativo = true,
                     Texto = "Bastante satisfeito.",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Idioma = Enums.ENUMIdioma.PORTUGUES,
                     Tipo = Enums.ENUMTipo.TEXT,
                     SuperCampoID = 19,
                 },
                  new Campo
                  {
                      ID = 22,
                      Ativo = true,
                      Texto = "Pouco satisfeito.",
                      Descricao = "",
                      DataCadastro = DateTime.Now,
                      DataAlteracao = DateTime.Now,
                      Idioma = Enums.ENUMIdioma.PORTUGUES,
                      Tipo = Enums.ENUMTipo.TEXT,
                      SuperCampoID = 19,
                  },
                  new Campo
                  {
                      ID = 23,
                      Ativo = true,
                      Texto = "Nada satisfeito.",
                      Descricao = "",
                      DataCadastro = DateTime.Now,
                      DataAlteracao = DateTime.Now,
                      Idioma = Enums.ENUMIdioma.PORTUGUES,
                      Tipo = Enums.ENUMTipo.TEXT,
                      SuperCampoID = 19,
                  },
                  new Campo
               {
                   ID = 24,
                   Ativo = true,
                   Texto = "Informe sua Média Mensal de Vendas.",
                   Descricao = "É o valor total/mensal dos produtos e serviços vendidos no último ano.",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Tipo = Enums.ENUMTipo.TEXT,
                   SuperCampoID = 1,
               },
               new Campo
               {
                   ID = 25,
                   Ativo = true,
                   Texto = "Média mensal",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Tipo = Enums.ENUMTipo.INPUT,
                   SuperCampoID = 24
               });


            modelBuilder.Entity<Campo>()
                .HasData(new Campo
                {
                    ID = 26,
                    Ativo = true,
                    Texto = "Capital de Giro",
                    Descricao = "<b>Capital de giro</b> é uma parte do investimento que compõe uma reserva de recursos que serão utilizados para suprir as necessidades financeiras da empresa ao longo do tempo.",
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    Idioma = Enums.ENUMIdioma.PORTUGUES,
                    Tipo = Enums.ENUMTipo.TEXT,
                    TabelaID = 1
                },
                new Campo
                {
                    ID = 27,
                    Ativo = true,
                    Texto = "Você sabe quanto dinheiro deve ter em caixa para cobrir suas despesas enquanto não recebe os pagamentos?",
                    Descricao = "Controle do capital de giro",
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    Idioma = Enums.ENUMIdioma.PORTUGUES,
                    Tipo = Enums.ENUMTipo.TEXT,
                    SuperCampoID = 26
                },
                 new Campo
                 {
                     ID = 28,
                     Ativo = true,
                     Texto = "Selecione sua resposta",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Idioma = Enums.ENUMIdioma.PORTUGUES,
                     Tipo = Enums.ENUMTipo.COMBO,
                     SuperCampoID = 27
                 },
                 new Campo
                 {
                     ID = 29,
                     Ativo = true,
                     Texto = "Não faço este cálculo regularmente.",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Idioma = Enums.ENUMIdioma.PORTUGUES,
                     Tipo = Enums.ENUMTipo.TEXT,
                     SuperCampoID = 28
                 },
                 new Campo
                 {
                     ID = 30,
                     Ativo = true,
                     Texto = "Tenho noção de quais são minhas despesas, mas não sei quanto dinheiro preciso ter em caixa.",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Idioma = Enums.ENUMIdioma.PORTUGUES,
                     Tipo = Enums.ENUMTipo.TEXT,
                     SuperCampoID = 28
                 },
                 new Campo
                 {
                     ID = 31,
                     Ativo = true,
                     Texto = "Sim, calculo qual o valor que a empresa precisa ter em caixa para que não precise pedir dinheiro emprestado.",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Idioma = Enums.ENUMIdioma.PORTUGUES,
                     Tipo = Enums.ENUMTipo.TEXT,
                     SuperCampoID = 28
                 },
                 new Campo
                 {
                     ID = 32,
                     Ativo = true,
                     Texto = "E como você se sente com relação a isso?",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Idioma = Enums.ENUMIdioma.PORTUGUES,
                     Tipo = Enums.ENUMTipo.TEXT,
                     SuperCampoID = 26
                 },
                 new Campo
                 {
                     ID = 33,
                     Ativo = true,
                     Texto = "Selecione sua resposta",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Idioma = Enums.ENUMIdioma.PORTUGUES,
                     Tipo = Enums.ENUMTipo.COMBO,
                     SuperCampoID = 32
                 },
               new Campo
               {
                   ID = 34,
                   Ativo = true,
                   Texto = "Totalmente satisfeito.",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Idioma = Enums.ENUMIdioma.PORTUGUES,
                   Tipo = Enums.ENUMTipo.TEXT,
                   SuperCampoID = 33
               },
               new Campo
               {
                   ID = 35,
                   Ativo = true,
                   Texto = "Bastante satisfeito.",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Idioma = Enums.ENUMIdioma.PORTUGUES,
                   Tipo = Enums.ENUMTipo.TEXT,
                   SuperCampoID = 33
               },
                new Campo
                {
                    ID = 36,
                    Ativo = true,
                    Texto = "Pouco satisfeito.",
                    Descricao = "",
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    Idioma = Enums.ENUMIdioma.PORTUGUES,
                    Tipo = Enums.ENUMTipo.TEXT,
                    SuperCampoID = 33
                },
                new Campo
                {
                    ID = 37,
                    Ativo = true,
                    Texto = "Nada satisfeito.",
                    Descricao = "",
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    Idioma = Enums.ENUMIdioma.PORTUGUES,
                    Tipo = Enums.ENUMTipo.TEXT,
                    SuperCampoID = 33
                },
                new Campo
                {
                    ID = 38,
                    Ativo = true,
                    Texto = "Você calcula o resultado (lucro ou prejuízo) do seu negócio periodicamente?",
                    Descricao = "Demonstrativo de resultados",
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    Idioma = Enums.ENUMIdioma.PORTUGUES,
                    Tipo = Enums.ENUMTipo.TEXT,
                    SuperCampoID = 26
                },
                 new Campo
                 {
                     ID = 39,
                     Ativo = true,
                     Texto = "Selecione sua resposta",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Idioma = Enums.ENUMIdioma.PORTUGUES,
                     Tipo = Enums.ENUMTipo.COMBO,
                     SuperCampoID = 38
                 },
                 new Campo
                 {
                     ID = 40,
                     Ativo = true,
                     Texto = "Não, não sei qual foi o resultado da empresa no período.",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Idioma = Enums.ENUMIdioma.PORTUGUES,
                     Tipo = Enums.ENUMTipo.TEXT,
                     SuperCampoID = 39
                 },
                 new Campo
                 {
                     ID = 41,
                     Ativo = true,
                     Texto = "Tenho uma noção de qual é o resultado da empresa em determinado tempo.",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Idioma = Enums.ENUMIdioma.PORTUGUES,
                     Tipo = Enums.ENUMTipo.TEXT,
                     SuperCampoID = 39
                 },
                 new Campo
                 {
                     ID = 42,
                     Ativo = true,
                     Texto = "Sim, calculo o resultado da minha empresa relacionando os meus custos e despesas com a receita.",
                     Descricao = "faturamento",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Idioma = Enums.ENUMIdioma.PORTUGUES,
                     Tipo = Enums.ENUMTipo.TEXT,
                     SuperCampoID = 39
                 },
                 new Campo
                 {
                     ID = 43,
                     Ativo = true,
                     Texto = "E como você se sente com relação a isso?",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Idioma = Enums.ENUMIdioma.PORTUGUES,
                     Tipo = Enums.ENUMTipo.TEXT,
                     SuperCampoID = 26
                 },
                 new Campo
                 {
                     ID = 44,
                     Ativo = true,
                     Texto = "Selecione sua resposta",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Idioma = Enums.ENUMIdioma.PORTUGUES,
                     Tipo = Enums.ENUMTipo.COMBO,
                     SuperCampoID = 43
                 },
               new Campo
               {
                   ID = 45,
                   Ativo = true,
                   Texto = "Totalmente satisfeito.",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Idioma = Enums.ENUMIdioma.PORTUGUES,
                   Tipo = Enums.ENUMTipo.TEXT,
                   SuperCampoID = 44
               },
               new Campo
               {
                   ID = 46,
                   Ativo = true,
                   Texto = "Bastante satisfeito.",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Idioma = Enums.ENUMIdioma.PORTUGUES,
                   Tipo = Enums.ENUMTipo.TEXT,
                   SuperCampoID = 44
               },
                new Campo
                {
                    ID = 47,
                    Ativo = true,
                    Texto = "Pouco satisfeito.",
                    Descricao = "",
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    Idioma = Enums.ENUMIdioma.PORTUGUES,
                    Tipo = Enums.ENUMTipo.TEXT,
                    SuperCampoID = 44
                },
                new Campo
                {
                    ID = 48,
                    Ativo = true,
                    Texto = "Nada satisfeito.",
                    Descricao = "",
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    Idioma = Enums.ENUMIdioma.PORTUGUES,
                    Tipo = Enums.ENUMTipo.TEXT,
                    SuperCampoID = 44
                },
                new Campo
                {
                    ID = 49,
                    Ativo = true,
                    Texto = "Qual é o seu lucro mensal desejado?",
                    Descricao = "",
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    Tipo = Enums.ENUMTipo.TEXT,
                    SuperCampoID = 26
                },
                 new Campo
                 {
                     ID = 50,
                     Ativo = true,
                     Texto = "Media mensal",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Tipo = Enums.ENUMTipo.INPUT,
                     SuperCampoID = 49
                 }); 

            modelBuilder.Entity<CampoEmpresa>()
                .HasData(new CampoEmpresa
                {
                    ID = 1,
                    Ativo = true,
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    EmpresaID = 1,
                    CampoID = 3
                },
                new CampoEmpresa
                {
                    ID = 2,
                    Ativo = true,
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    EmpresaID = 1,
                    CampoID = 8
                },
                new CampoEmpresa
                {
                    ID = 3,
                    Ativo = true,
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    EmpresaID = 1,
                    CampoID = 14
                },
                new CampoEmpresa
                {
                    ID = 4,
                    Ativo = true,
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    EmpresaID = 1,
                    CampoID = 19
                },
                new CampoEmpresa
                {
                    ID = 5,
                    Ativo = true,
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    EmpresaID = 1,
                    CampoID = 25
                },
                new CampoEmpresa
                {
                    ID = 6,
                    Ativo = true,
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    EmpresaID = 1,
                    CampoID = 28
                },
                new CampoEmpresa
                {
                    ID = 7,
                    Ativo = true,
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    EmpresaID = 1,
                    CampoID = 33
                },
                new CampoEmpresa
                {
                    ID = 8,
                    Ativo = true,
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    EmpresaID = 1,
                    CampoID = 39
                },
                new CampoEmpresa
                {
                    ID = 9,
                    Ativo = true,
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    EmpresaID = 1,
                    CampoID = 44
                },
                new CampoEmpresa
                {
                    ID = 10,
                    Ativo = true,
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    EmpresaID = 1,
                    CampoID = 50
                });


        }
    }
}
