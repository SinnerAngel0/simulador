﻿using Microsoft.EntityFrameworkCore;
using Simulador_DAO.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.EFConfig
{
    public static class Etapa3ModelBuilder
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Campo>()
           .HasData(new Campo
           {
               ID = 103,
               Ativo = true,
               Texto = "",
               Descricao = "<b>Ativo Circulante</b> é uma referência aos bens e direitos que podem ser convertidos em dinheiro em curto prazo.",
               DataCadastro = DateTime.Now,
               DataAlteracao = DateTime.Now,
               Tipo = Enums.ENUMTipo.TEXT,
               TabelaID = 3
           },
           new Campo
           {
               ID = 104,
               Ativo = true,
               Texto = "Matéria-prima e produtos para venda",
               Descricao = "Valor médio mensal",
               DataCadastro = DateTime.Now,
               DataAlteracao = DateTime.Now,
               Tipo = Enums.ENUMTipo.TEXT,
               SuperCampoID = 103,
           },
           new Campo
           {
               ID = 105,
               Ativo = true,
               Texto = "Média mensal",
               Descricao = "",
               DataCadastro = DateTime.Now,
               DataAlteracao = DateTime.Now,
               Tipo = Enums.ENUMTipo.INPUT,
               SuperCampoID = 104,
           },
            new Campo
            {
                ID = 106,
                Ativo = true,
                Texto = "Por quanto você compra o seu principal produto/serviço?",
                Descricao = "",
                DataCadastro = DateTime.Now,
                DataAlteracao = DateTime.Now,
                Tipo = Enums.ENUMTipo.TEXT,
                SuperCampoID = 103,
            },
           new Campo
           {
               ID = 107,
               Ativo = true,
               Texto = "Média mensal",
               Descricao = "",
               DataCadastro = DateTime.Now,
               DataAlteracao = DateTime.Now,
               Tipo = Enums.ENUMTipo.INPUT,
               SuperCampoID = 106,
           });

            modelBuilder.Entity<Campo>()
               .HasData(new Campo
               {
                   ID = 108,
                   Ativo = true,
                   Texto = "Estoque e Suprimentos",
                   Descricao = "<b>Cálculo de Custos</b>, é a soma dos valores gastos para produzir o produto ou serviço do seu negócio, ou para adquirir o produto em questão.",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Idioma = Enums.ENUMIdioma.PORTUGUES,
                   Tipo = Enums.ENUMTipo.TEXT,
                   TabelaID = 3
               },
               new Campo
               {
                   ID = 109,
                   Ativo = true,
                   Texto = "Você tem um controle do estoque dos produtos e insumos?",
                   Descricao = "Gestão de Estoques",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Idioma = Enums.ENUMIdioma.PORTUGUES,
                   Tipo = Enums.ENUMTipo.TEXT,
                   SuperCampoID = 108
               },
               new Campo
               {
                   ID = 110,
                   Ativo = true,
                   Texto = "Seleciona sua resposta",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Idioma = Enums.ENUMIdioma.PORTUGUES,
                   Tipo = Enums.ENUMTipo.COMBO,
                   SuperCampoID = 109
               },
               new Campo
               {
                   ID = 111,
                   Ativo = true,
                   Texto = "Não, não tenho este controle.",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Idioma = Enums.ENUMIdioma.PORTUGUES,
                   Tipo = Enums.ENUMTipo.TEXT,
                   SuperCampoID = 110
               },
               new Campo
               {
                   ID = 112,
                   Ativo = true,
                   Texto = "Sei de cabeça, mas não tenho um controle formal.",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Idioma = Enums.ENUMIdioma.PORTUGUES,
                   Tipo = Enums.ENUMTipo.TEXT,
                   SuperCampoID = 110
               },
               new Campo
               {
                   ID = 113,
                   Ativo = true,
                   Texto = "Sim, registro quanto tenho em estoque e faço seu controle periodicamente.",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Idioma = Enums.ENUMIdioma.PORTUGUES,
                   Tipo = Enums.ENUMTipo.TEXT,
                   SuperCampoID = 110
               },
               new Campo
               {
                   ID = 114,
                   Ativo = true,
                   Texto = "E como você se sente com relação a isso?",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Idioma = Enums.ENUMIdioma.PORTUGUES,
                   Tipo = Enums.ENUMTipo.TEXT,
                   SuperCampoID = 108,
               },
               new Campo
               {
                   ID = 115,
                   Ativo = true,
                   Texto = "Selecione sua resposta",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Idioma = Enums.ENUMIdioma.PORTUGUES,
                   Tipo = Enums.ENUMTipo.COMBO,
                   SuperCampoID = 114
               },
              new Campo
              {
                  ID = 116,
                  Ativo = true,
                  Texto = "Totalmente satisfeito.",
                  Descricao = "",
                  DataCadastro = DateTime.Now,
                  DataAlteracao = DateTime.Now,
                  Idioma = Enums.ENUMIdioma.PORTUGUES,
                  Tipo = Enums.ENUMTipo.TEXT,
                  SuperCampoID = 115,
              },
              new Campo
              {
                  ID = 117,
                  Ativo = true,
                  Texto = "Bastante satisfeito.",
                  Descricao = "",
                  DataCadastro = DateTime.Now,
                  DataAlteracao = DateTime.Now,
                  Idioma = Enums.ENUMIdioma.PORTUGUES,
                  Tipo = Enums.ENUMTipo.TEXT,
                  SuperCampoID = 115,
              },
               new Campo
               {
                   ID = 118,
                   Ativo = true,
                   Texto = "Pouco satisfeito.",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Idioma = Enums.ENUMIdioma.PORTUGUES,
                   Tipo = Enums.ENUMTipo.TEXT,
                   SuperCampoID = 115,
               },
               new Campo
               {
                   ID = 119,
                   Ativo = true,
                   Texto = "Nada satisfeito.",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Idioma = Enums.ENUMIdioma.PORTUGUES,
                   Tipo = Enums.ENUMTipo.TEXT,
                   SuperCampoID = 115,
               },
               new Campo
           {
               ID = 120,
               Ativo = true,
               Texto = "Suprimentos",
               Descricao = "Valor médio mensal",
               DataCadastro = DateTime.Now,
               DataAlteracao = DateTime.Now,
               Tipo = Enums.ENUMTipo.TEXT,
               SuperCampoID = 108,
           },
           new Campo
           {
               ID = 121,
               Ativo = true,
               Texto = "Média mensal",
               Descricao = "",
               DataCadastro = DateTime.Now,
               DataAlteracao = DateTime.Now,
               Tipo = Enums.ENUMTipo.INPUT,
               SuperCampoID = 120,
           },
            new Campo
            {
                ID = 122,
                Ativo = true,
                Texto = "Custo da Mercadoria Vendida (CMV)",
                Descricao = "É o Valor médio mensal correspondente à entrega da venda (mercadoria, serviço prestado etc.).",
                DataCadastro = DateTime.Now,
                DataAlteracao = DateTime.Now,
                Tipo = Enums.ENUMTipo.TEXT,
                SuperCampoID = 108,
            },
           new Campo
           {
               ID = 123,
               Ativo = true,
               Texto = "Média mensal",
               Descricao = "",
               DataCadastro = DateTime.Now,
               DataAlteracao = DateTime.Now,
               Tipo = Enums.ENUMTipo.INPUT,
               SuperCampoID = 122,
           });

            modelBuilder.Entity<Campo>()
               .HasData(new Campo
               {
                   ID = 124,
                   Ativo = true,
                   Texto = "Preço de Venda",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Idioma = Enums.ENUMIdioma.PORTUGUES,
                   Tipo = Enums.ENUMTipo.TEXT,
                   TabelaID = 3
               },
               new Campo
               {
                   ID = 125,
                   Ativo = true,
                   Texto = "Você sabe qual é o seu ganho de dinheiro em cada produto ou serviço vendido?",
                   Descricao = "Cálculo do ganho unitário",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Idioma = Enums.ENUMIdioma.PORTUGUES,
                   Tipo = Enums.ENUMTipo.TEXT,
                   SuperCampoID = 124
               },
               new Campo
               {
                   ID = 126,
                   Ativo = true,
                   Texto = "Seleciona sua resposta",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Idioma = Enums.ENUMIdioma.PORTUGUES,
                   Tipo = Enums.ENUMTipo.COMBO,
                   SuperCampoID = 125
               },
               new Campo
               {
                   ID = 127,
                   Ativo = true,
                   Texto = "Não, não sei qual é o meu ganho naquilo que vendo.",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Idioma = Enums.ENUMIdioma.PORTUGUES,
                   Tipo = Enums.ENUMTipo.TEXT,
                   SuperCampoID = 126
               },
               new Campo
               {
                   ID = 128,
                   Ativo = true,
                   Texto = "Tenho um conhecimento aproximado sobre o ganho que cada produto / serviço traz.",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Idioma = Enums.ENUMIdioma.PORTUGUES,
                   Tipo = Enums.ENUMTipo.TEXT,
                   SuperCampoID = 126
               },
               new Campo
               {
                   ID = 129,
                   Ativo = true,
                   Texto = "Sim, calculo periodicamente o ganho de cada produto / serviço.",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Idioma = Enums.ENUMIdioma.PORTUGUES,
                   Tipo = Enums.ENUMTipo.TEXT,
                   SuperCampoID = 126
               },
               new Campo
               {
                   ID = 130,
                   Ativo = true,
                   Texto = "E como você se sente com relação a isso?",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Idioma = Enums.ENUMIdioma.PORTUGUES,
                   Tipo = Enums.ENUMTipo.TEXT,
                   SuperCampoID = 124,
               },
               new Campo
               {
                   ID = 131,
                   Ativo = true,
                   Texto = "Selecione sua resposta",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Idioma = Enums.ENUMIdioma.PORTUGUES,
                   Tipo = Enums.ENUMTipo.COMBO,
                   SuperCampoID = 130
               },
              new Campo
              {
                  ID = 132,
                  Ativo = true,
                  Texto = "Totalmente satisfeito.",
                  Descricao = "",
                  DataCadastro = DateTime.Now,
                  DataAlteracao = DateTime.Now,
                  Idioma = Enums.ENUMIdioma.PORTUGUES,
                  Tipo = Enums.ENUMTipo.TEXT,
                  SuperCampoID = 131,
              },
              new Campo
              {
                  ID = 133,
                  Ativo = true,
                  Texto = "Bastante satisfeito.",
                  Descricao = "",
                  DataCadastro = DateTime.Now,
                  DataAlteracao = DateTime.Now,
                  Idioma = Enums.ENUMIdioma.PORTUGUES,
                  Tipo = Enums.ENUMTipo.TEXT,
                  SuperCampoID = 131,
              },
               new Campo
               {
                   ID = 134,
                   Ativo = true,
                   Texto = "Pouco satisfeito.",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Idioma = Enums.ENUMIdioma.PORTUGUES,
                   Tipo = Enums.ENUMTipo.TEXT,
                   SuperCampoID = 131,
               },
               new Campo
               {
                   ID = 135,
                   Ativo = true,
                   Texto = "Nada satisfeito.",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Idioma = Enums.ENUMIdioma.PORTUGUES,
                   Tipo = Enums.ENUMTipo.TEXT,
                   SuperCampoID = 131,
               },
               new Campo
               {
                   ID = 136,
                   Ativo = true,
                   Texto = "Comissão sobre vendas",
                   Descricao = "Valor médio mensal",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Tipo = Enums.ENUMTipo.TEXT,
                   SuperCampoID = 124,
               },
           new Campo
           {
               ID = 137,
               Ativo = true,
               Texto = "Média mensal",
               Descricao = "",
               DataCadastro = DateTime.Now,
               DataAlteracao = DateTime.Now,
               Tipo = Enums.ENUMTipo.INPUT,
               SuperCampoID = 136,
           },
            new Campo
            {
                ID = 138,
                Ativo = true,
                Texto = "Impostos e taxas (cartão de crédito) sobre vendas",
                Descricao = "É o Valor médio mensal incidente sobre o montante das vendas.",
                DataCadastro = DateTime.Now,
                DataAlteracao = DateTime.Now,
                Tipo = Enums.ENUMTipo.TEXT,
                SuperCampoID = 124,
            },
           new Campo
           {
               ID = 139,
               Ativo = true,
               Texto = "Média mensal",
               Descricao = "",
               DataCadastro = DateTime.Now,
               DataAlteracao = DateTime.Now,
               Tipo = Enums.ENUMTipo.INPUT,
               SuperCampoID = 138,
           });




            modelBuilder.Entity<CampoEmpresa>()
                    .HasData(new CampoEmpresa
                    {
                        ID = 28,
                        Ativo = true,
                        DataCadastro = DateTime.Now,
                        DataAlteracao = DateTime.Now,
                        EmpresaID = 1,
                        CampoID = 105
                    },
                    new CampoEmpresa
                    {
                        ID = 29,
                        Ativo = true,
                        DataCadastro = DateTime.Now,
                        DataAlteracao = DateTime.Now,
                        EmpresaID = 1,
                        CampoID = 107
                    },
                    new CampoEmpresa
                    {
                        ID = 30,
                        Ativo = true,
                        DataCadastro = DateTime.Now,
                        DataAlteracao = DateTime.Now,
                        EmpresaID = 1,
                        CampoID = 110
                    },
                    new CampoEmpresa
                    {
                        ID = 31,
                        Ativo = true,
                        DataCadastro = DateTime.Now,
                        DataAlteracao = DateTime.Now,
                        EmpresaID = 1,
                        CampoID = 115
                    },
                    new CampoEmpresa
                    {
                        ID = 32,
                        Ativo = true,
                        DataCadastro = DateTime.Now,
                        DataAlteracao = DateTime.Now,
                        EmpresaID = 1,
                        CampoID = 121
                    },
                    new CampoEmpresa
                    {
                        ID = 33,
                        Ativo = true,
                        DataCadastro = DateTime.Now,
                        DataAlteracao = DateTime.Now,
                        EmpresaID = 1,
                        CampoID = 123
                    },
                    new CampoEmpresa
                    {
                        ID = 34,
                        Ativo = true,
                        DataCadastro = DateTime.Now,
                        DataAlteracao = DateTime.Now,
                        EmpresaID = 1,
                        CampoID = 126
                    },
                    new CampoEmpresa
                    {
                        ID = 35,
                        Ativo = true,
                        DataCadastro = DateTime.Now,
                        DataAlteracao = DateTime.Now,
                        EmpresaID = 1,
                        CampoID = 131
                    },
                    new CampoEmpresa
                    {
                        ID = 36,
                        Ativo = true,
                        DataCadastro = DateTime.Now,
                        DataAlteracao = DateTime.Now,
                        EmpresaID = 1,
                        CampoID = 137
                    },
                    new CampoEmpresa
                    {
                        ID = 37,
                        Ativo = true,
                        DataCadastro = DateTime.Now,
                        DataAlteracao = DateTime.Now,
                        EmpresaID = 1,
                        CampoID = 139
                    });

            

        }
    }
}
