﻿using Microsoft.EntityFrameworkCore;
using Simulador_DAO.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.EFConfig
{
    public static class Etapa2ModelBuilder
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Campo>()
                .HasData(new Campo
                {
                    ID = 51,
                    Ativo = true,
                    Texto = "Cálculo do Custo",
                    Descricao = "<b>Cálculo de Custos</b>, é a soma dos valores gastos para produzir o produto ou serviço do seu negócio, ou para adquirir o produto em questão.",
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    Idioma = Enums.ENUMIdioma.PORTUGUES,
                    Tipo = Enums.ENUMTipo.TEXT,
                    TabelaID = 2
                },
                new Campo
                {
                    ID = 52,
                    Ativo = true,
                    Texto = "Você calcula os gastos (custos e despesas) mensalmente?",
                    Descricao = "Demonstrativo de resultados",
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    Idioma = Enums.ENUMIdioma.PORTUGUES,
                    Tipo = Enums.ENUMTipo.TEXT,
                    SuperCampoID = 51
                },
                new Campo
                {
                    ID = 53,
                    Ativo = true,
                    Texto = "Seleciona sua resposta",
                    Descricao = "",
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    Idioma = Enums.ENUMIdioma.PORTUGUES,
                    Tipo = Enums.ENUMTipo.COMBO,
                    SuperCampoID = 52
                },
                new Campo
                {
                    ID = 54,
                    Ativo = true,
                    Texto = "Não, não sei quais são os custos da empresa.",
                    Descricao = "",
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    Idioma = Enums.ENUMIdioma.PORTUGUES,
                    Tipo = Enums.ENUMTipo.TEXT,
                    SuperCampoID = 53
                },
                new Campo
                {
                    ID = 55,
                    Ativo = true,
                    Texto = "Tenho noção de qual é o valor dos custos e despesas mensais da empresa, mas não tenho um controle preciso.",
                    Descricao = "",
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    Idioma = Enums.ENUMIdioma.PORTUGUES,
                    Tipo = Enums.ENUMTipo.TEXT,
                    SuperCampoID = 53
                },
                new Campo
                {
                    ID = 56,
                    Ativo = true,
                    Texto = "Sim, registro qual o valor dos custos e despesas mensalmente, sabendo quanto foi o desembolso no período.",
                    Descricao = "",
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    Idioma = Enums.ENUMIdioma.PORTUGUES,
                    Tipo = Enums.ENUMTipo.TEXT,
                    SuperCampoID = 53
                },
                new Campo
                {
                    ID = 57,
                    Ativo = true,
                    Texto = "E como você se sente com relação a isso?",
                    Descricao = "",
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    Idioma = Enums.ENUMIdioma.PORTUGUES,
                    Tipo = Enums.ENUMTipo.TEXT,
                    SuperCampoID = 51
                },
                new Campo
                {
                    ID = 58,
                    Ativo = true,
                    Texto = "Selecione sua resposta",
                    Descricao = "",
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    Idioma = Enums.ENUMIdioma.PORTUGUES,
                    Tipo = Enums.ENUMTipo.COMBO,
                    SuperCampoID = 57
                },
               new Campo
               {
                   ID = 59,
                   Ativo = true,
                   Texto = "Totalmente satisfeito.",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Idioma = Enums.ENUMIdioma.PORTUGUES,
                   Tipo = Enums.ENUMTipo.TEXT,
                   SuperCampoID = 58
               },
               new Campo
               {
                   ID = 60,
                   Ativo = true,
                   Texto = "Bastante satisfeito.",
                   Descricao = "",
                   DataCadastro = DateTime.Now,
                   DataAlteracao = DateTime.Now,
                   Idioma = Enums.ENUMIdioma.PORTUGUES,
                   Tipo = Enums.ENUMTipo.TEXT,
                   SuperCampoID = 58
               },
                new Campo
                {
                    ID = 61,
                    Ativo = true,
                    Texto = "Pouco satisfeito.",
                    Descricao = "",
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    Idioma = Enums.ENUMIdioma.PORTUGUES,
                    Tipo = Enums.ENUMTipo.TEXT,
                    SuperCampoID = 58
                },
                new Campo
                {
                    ID = 62,
                    Ativo = true,
                    Texto = "Nada satisfeito.",
                    Descricao = "",
                    DataCadastro = DateTime.Now,
                    DataAlteracao = DateTime.Now,
                    Idioma = Enums.ENUMIdioma.PORTUGUES,
                    Tipo = Enums.ENUMTipo.TEXT,
                    SuperCampoID = 58
                });
            
            modelBuilder.Entity<Campo>()
            .HasData(new Campo
            {
                ID = 63,
                Ativo = true,
                Texto = "Local Físico",
                Descricao = "",
                DataCadastro = DateTime.Now,
                DataAlteracao = DateTime.Now,
                Tipo = Enums.ENUMTipo.TEXT,
                TabelaID = 2
            },
            new Campo
            {
                ID = 64,
                Ativo = true,
                Texto = "Aluguel, condomínio, estrutura física",
                Descricao = "Valor médio mensal",
                DataCadastro = DateTime.Now,
                DataAlteracao = DateTime.Now,
                Tipo = Enums.ENUMTipo.TEXT,
                SuperCampoID = 63
            },
            new Campo
            {
                ID = 65,
                Ativo = true,
                Texto = "Media mensal",
                Descricao = "",
                DataCadastro = DateTime.Now,
                DataAlteracao = DateTime.Now,
                Tipo = Enums.ENUMTipo.INPUT,
                SuperCampoID = 64
            });

            modelBuilder.Entity<Campo>()
          .HasData(new Campo
          {
              ID = 66,
              Ativo = true,
              Texto = "Despesas gerais",
              Descricao = "Valor médio mensal (Ex. energia elétrica, água etc.)",
              DataCadastro = DateTime.Now,
              DataAlteracao = DateTime.Now,
              Tipo = Enums.ENUMTipo.TEXT,
              SuperCampoID = 63
          },
          new Campo
          {
              ID = 67,
              Ativo = true,
              Texto = "Media mensal",
              Descricao = "",
              DataCadastro = DateTime.Now,
              DataAlteracao = DateTime.Now,
              Tipo = Enums.ENUMTipo.INPUT,
              SuperCampoID = 66
          });

            modelBuilder.Entity<Campo>()
          .HasData(new Campo
          {
              ID = 68,
              Ativo = true,
              Texto = "Gastos administrativos",
              Descricao = "Valor médio mensal",
              DataCadastro = DateTime.Now,
              DataAlteracao = DateTime.Now,
              Tipo = Enums.ENUMTipo.TEXT,
              SuperCampoID = 63
          },
          new Campo
          {
              ID = 69,
              Ativo = true,
              Texto = "Media mensal",
              Descricao = "",
              DataCadastro = DateTime.Now,
              DataAlteracao = DateTime.Now,
              Tipo = Enums.ENUMTipo.INPUT,
              SuperCampoID = 68
          });

            modelBuilder.Entity<Campo>()
          .HasData(new Campo
          {
              ID = 70,
              Ativo = true,
              Texto = "IPTU",
              Descricao = "Valor médio mensal",
              DataCadastro = DateTime.Now,
              DataAlteracao = DateTime.Now,
              Tipo = Enums.ENUMTipo.TEXT,
              SuperCampoID = 63
          },
          new Campo
          {
              ID = 71,
              Ativo = true,
              Texto = "Media mensal",
              Descricao = "",
              DataCadastro = DateTime.Now,
              DataAlteracao = DateTime.Now,
              Tipo = Enums.ENUMTipo.INPUT,
              SuperCampoID = 70
          });

            modelBuilder.Entity<Campo>()
           .HasData(new Campo
           {
               ID = 72,
               Ativo = true,
               Texto = "Outros custos variáveis",
               Descricao = "Valor médio mensal",
               DataCadastro = DateTime.Now,
               DataAlteracao = DateTime.Now,
               Tipo = Enums.ENUMTipo.TEXT,
               SuperCampoID = 63
           },
           new Campo
           {
               ID = 73,
               Ativo = true,
               Texto = "Media mensal",
               Descricao = "",
               DataCadastro = DateTime.Now,
               DataAlteracao = DateTime.Now,
               Tipo = Enums.ENUMTipo.INPUT,
               SuperCampoID = 72
           });


            modelBuilder.Entity<Campo>()
                  .HasData(new Campo
                  {
                      ID = 74,
                      Ativo = true,
                      Texto = "Encargos Administrativos",
                      Descricao = "<b>Imposto</b> todo o montante de dinheiro que os cidadãos de um país devem pagar ao Estado para garantir a funcionalidade de serviços públicos e coletivos. Na língua portuguesa, imposto também pode ser entendido como um adjetivo, no sentido de ser algo que se impôs.",
                      DataCadastro = DateTime.Now,
                      DataAlteracao = DateTime.Now,
                      Idioma = Enums.ENUMIdioma.PORTUGUES,
                      Tipo = Enums.ENUMTipo.TEXT,
                      TabelaID = 2
                  },
                  new Campo
                  {
                      ID = 75,
                      Ativo = true,
                      Texto = "Você controla as datas e valores de todos os tributos a serem pagos?",
                      Descricao = "Controle do pagamento de tributos",
                      DataCadastro = DateTime.Now,
                      DataAlteracao = DateTime.Now,
                      Idioma = Enums.ENUMIdioma.PORTUGUES,
                      Tipo = Enums.ENUMTipo.TEXT,
                      SuperCampoID = 74
                  },
                   new Campo
                   {
                       ID = 76,
                       Ativo = true,
                       Texto = "Selecione sua resposta",
                       Descricao = "",
                       DataCadastro = DateTime.Now,
                       DataAlteracao = DateTime.Now,
                       Idioma = Enums.ENUMIdioma.PORTUGUES,
                       Tipo = Enums.ENUMTipo.COMBO,
                       SuperCampoID = 75
                   },
                   new Campo
                   {
                       ID = 77,
                       Ativo = true,
                       Texto = "Não, não tenho controle sobre o pagamento de tributos.",
                       Descricao = "",
                       DataCadastro = DateTime.Now,
                       DataAlteracao = DateTime.Now,
                       Idioma = Enums.ENUMIdioma.PORTUGUES,
                       Tipo = Enums.ENUMTipo.TEXT,
                       SuperCampoID = 76
                   },
                   new Campo
                   {
                       ID = 78,
                       Ativo = true,
                       Texto = "Sei quando devem ser pagos alguns tributos, mas não tenho controle sobre quando cada um deve ser pago.",
                       Descricao = "",
                       DataCadastro = DateTime.Now,
                       DataAlteracao = DateTime.Now,
                       Idioma = Enums.ENUMIdioma.PORTUGUES,
                       Tipo = Enums.ENUMTipo.TEXT,
                       SuperCampoID = 76
                   },
                   new Campo
                   {
                       ID = 79,
                       Ativo = true,
                       Texto = "Sim, possuo controle formal e registro de quando cada tributo deve ser pago, assim como registro os pagamentos.",
                       Descricao = "",
                       DataCadastro = DateTime.Now,
                       DataAlteracao = DateTime.Now,
                       Idioma = Enums.ENUMIdioma.PORTUGUES,
                       Tipo = Enums.ENUMTipo.TEXT,
                       SuperCampoID = 76
                   },
                   new Campo
                   {
                       ID = 80,
                       Ativo = true,
                       Texto = "E como você se sente com relação a isso?",
                       Descricao = "",
                       DataCadastro = DateTime.Now,
                       DataAlteracao = DateTime.Now,
                       Idioma = Enums.ENUMIdioma.PORTUGUES,
                       Tipo = Enums.ENUMTipo.TEXT,
                       SuperCampoID = 74
                   },
                   new Campo
                   {
                       ID = 81,
                       Ativo = true,
                       Texto = "Selecione sua resposta",
                       Descricao = "",
                       DataCadastro = DateTime.Now,
                       DataAlteracao = DateTime.Now,
                       Idioma = Enums.ENUMIdioma.PORTUGUES,
                       Tipo = Enums.ENUMTipo.COMBO,
                       SuperCampoID = 80
                   },
                 new Campo
                 {
                     ID = 82,
                     Ativo = true,
                     Texto = "Totalmente satisfeito.",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Idioma = Enums.ENUMIdioma.PORTUGUES,
                     Tipo = Enums.ENUMTipo.TEXT,
                     SuperCampoID = 81
                 },
                 new Campo
                 {
                     ID = 83,
                     Ativo = true,
                     Texto = "Bastante satisfeito.",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Idioma = Enums.ENUMIdioma.PORTUGUES,
                     Tipo = Enums.ENUMTipo.TEXT,
                     SuperCampoID = 81
                 },
                  new Campo
                  {
                      ID = 84,
                      Ativo = true,
                      Texto = "Pouco satisfeito.",
                      Descricao = "",
                      DataCadastro = DateTime.Now,
                      DataAlteracao = DateTime.Now,
                      Idioma = Enums.ENUMIdioma.PORTUGUES,
                      Tipo = Enums.ENUMTipo.TEXT,
                      SuperCampoID = 81
                  },
                  new Campo
                  {
                      ID = 85,
                      Ativo = true,
                      Texto = "Nada satisfeito.",
                      Descricao = "",
                      DataCadastro = DateTime.Now,
                      DataAlteracao = DateTime.Now,
                      Idioma = Enums.ENUMIdioma.PORTUGUES,
                      Tipo = Enums.ENUMTipo.TEXT,
                      SuperCampoID = 81
                  },
                  new Campo
                  {
                      ID = 86,
                      Ativo = true,
                      Texto = "Salários e pró-labore",
                      Descricao = "O pró-labore é a remuneração do dono ou sócio pelo trabalho que desempenha na empresa. / Valor médio mensal",
                      DataCadastro = DateTime.Now,
                      DataAlteracao = DateTime.Now,
                      Tipo = Enums.ENUMTipo.TEXT,
                      SuperCampoID = 74
                  },
                  new Campo
                  {
                      ID = 87,
                      Ativo = true,
                      Texto = "Media mensal",
                      Descricao = "",
                      DataCadastro = DateTime.Now,
                      DataAlteracao = DateTime.Now,
                      Tipo = Enums.ENUMTipo.INPUT,
                      SuperCampoID = 86
                  },
                  new Campo
                  {
                      ID = 88,
                      Ativo = true,
                      Texto = "Encargos sociais",
                      Descricao = "Valor médio mensal",
                      DataCadastro = DateTime.Now,
                      DataAlteracao = DateTime.Now,
                      Tipo = Enums.ENUMTipo.TEXT,
                      SuperCampoID = 74
                  },
                  new Campo
                  {
                      ID = 89,
                      Ativo = true,
                      Texto = "Media mensal",
                      Descricao = "",
                      DataCadastro = DateTime.Now,
                      DataAlteracao = DateTime.Now,
                      Tipo = Enums.ENUMTipo.INPUT,
                      SuperCampoID = 88
                  });


            modelBuilder.Entity<Campo>()
                  .HasData(new Campo
                  {
                     ID = 90,
                     Ativo = true,
                     Texto = "Encargos Bancários ",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Tipo = Enums.ENUMTipo.TEXT,
                     TabelaID = 2,
                 },
                 new Campo
                 {
                     ID = 91,
                     Ativo = true,
                     Texto = "Parcelamentos",
                     Descricao = "Valor médio mensal",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Tipo = Enums.ENUMTipo.TEXT,
                     SuperCampoID = 90
                 },
                 new Campo
                 {
                     ID = 92,
                     Ativo = true,
                     Texto = "Media mensal",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Tipo = Enums.ENUMTipo.INPUT,
                     SuperCampoID = 91
                 },

                 new Campo
                 {
                     ID = 93,
                     Ativo = true,
                     Texto = "Amortização dos seus Investimentos",
                     Descricao = "Valor médio mensal",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Tipo = Enums.ENUMTipo.TEXT,
                     SuperCampoID = 90
                 },
                 new Campo
                 {
                     ID = 94,
                     Ativo = true,
                     Texto = "Media mensal",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Tipo = Enums.ENUMTipo.INPUT,
                     SuperCampoID = 93
                 },

                 new Campo
                 {
                     ID = 95,
                     Ativo = true,
                     Texto = "Despesas Fixas e os juros de empréstimos atuais",
                     Descricao = "Valor médio mensal",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Tipo = Enums.ENUMTipo.TEXT,
                     SuperCampoID = 90
                 },
                 new Campo
                 {
                     ID = 96,
                     Ativo = true,
                     Texto = "Media mensal",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Tipo = Enums.ENUMTipo.INPUT,
                     SuperCampoID = 95
                 },

                 new Campo
                 {
                     ID = 97,
                     Ativo = true,
                     Texto = "Amortização de Empréstimos Atual",
                     Descricao = "Valor médio mensal",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Tipo = Enums.ENUMTipo.TEXT,
                     SuperCampoID = 90
                 },
                 new Campo
                 {
                     ID = 98,
                     Ativo = true,
                     Texto = "Media mensal",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Tipo = Enums.ENUMTipo.INPUT,
                     SuperCampoID = 97
                 },
                 new Campo
                 {
                     ID = 99,
                     Ativo = true,
                     Texto = "Parcelamentos",
                     Descricao = "Valor médio mensal",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Tipo = Enums.ENUMTipo.TEXT,
                     SuperCampoID = 90
                 },
                 new Campo
                 {
                     ID = 100,
                     Ativo = true,
                     Texto = "Media mensal",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Tipo = Enums.ENUMTipo.INPUT,
                     SuperCampoID = 99
                 },
                 new Campo
                 {
                     ID = 101,
                     Ativo = true,
                     Texto = "Outros",
                     Descricao = "Valor médio mensal",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Tipo = Enums.ENUMTipo.TEXT,
                     SuperCampoID = 90
                 },
                 new Campo
                 {
                     ID = 102,
                     Ativo = true,
                     Texto = "Media mensal",
                     Descricao = "",
                     DataCadastro = DateTime.Now,
                     DataAlteracao = DateTime.Now,
                     Tipo = Enums.ENUMTipo.INPUT,
                     SuperCampoID = 101
                 });




            modelBuilder.Entity<CampoEmpresa>()
                    .HasData(new CampoEmpresa
                    {
                        ID = 11,
                        Ativo = true,
                        DataCadastro = DateTime.Now,
                        DataAlteracao = DateTime.Now,
                        EmpresaID = 1,
                        CampoID = 53
                    },
                    new CampoEmpresa
                    {
                        ID = 12,
                        Ativo = true,
                        DataCadastro = DateTime.Now,
                        DataAlteracao = DateTime.Now,
                        EmpresaID = 1,
                        CampoID = 58
                    },
                    new CampoEmpresa
                    {
                        ID = 13,
                        Ativo = true,
                        DataCadastro = DateTime.Now,
                        DataAlteracao = DateTime.Now,
                        EmpresaID = 1,
                        CampoID = 65
                    },
                    new CampoEmpresa
                    {
                        ID = 14,
                        Ativo = true,
                        DataCadastro = DateTime.Now,
                        DataAlteracao = DateTime.Now,
                        EmpresaID = 1,
                        CampoID = 67
                    },
                    new CampoEmpresa
                    {
                        ID = 15,
                        Ativo = true,
                        DataCadastro = DateTime.Now,
                        DataAlteracao = DateTime.Now,
                        EmpresaID = 1,
                        CampoID = 69
                    },
                    new CampoEmpresa
                    {
                        ID = 16,
                        Ativo = true,
                        DataCadastro = DateTime.Now,
                        DataAlteracao = DateTime.Now,
                        EmpresaID = 1,
                        CampoID = 71
                    },
                    new CampoEmpresa
                    {
                        ID = 17,
                        Ativo = true,
                        DataCadastro = DateTime.Now,
                        DataAlteracao = DateTime.Now,
                        EmpresaID = 1,
                        CampoID = 73
                    },
                    new CampoEmpresa
                    {
                        ID = 18,
                        Ativo = true,
                        DataCadastro = DateTime.Now,
                        DataAlteracao = DateTime.Now,
                        EmpresaID = 1,
                        CampoID = 76
                    },
                    new CampoEmpresa
                    {
                        ID = 19,
                        Ativo = true,
                        DataCadastro = DateTime.Now,
                        DataAlteracao = DateTime.Now,
                        EmpresaID = 1,
                        CampoID = 81
                    },
                    new CampoEmpresa
                    {
                        ID = 20,
                        Ativo = true,
                        DataCadastro = DateTime.Now,
                        DataAlteracao = DateTime.Now,
                        EmpresaID = 1,
                        CampoID = 87
                    },
                    new CampoEmpresa
                    {
                        ID = 21,
                        Ativo = true,
                        DataCadastro = DateTime.Now,
                        DataAlteracao = DateTime.Now,
                        EmpresaID = 1,
                        CampoID = 89
                    },
                    new CampoEmpresa
                    {
                        ID = 22,
                        Ativo = true,
                        DataCadastro = DateTime.Now,
                        DataAlteracao = DateTime.Now,
                        EmpresaID = 1,
                        CampoID = 92
                    },
                     new CampoEmpresa
                     {
                         ID = 23,
                         Ativo = true,
                         DataCadastro = DateTime.Now,
                         DataAlteracao = DateTime.Now,
                         EmpresaID = 1,
                         CampoID = 94
                     },
                      new CampoEmpresa
                      {
                          ID = 24,
                          Ativo = true,
                          DataCadastro = DateTime.Now,
                          DataAlteracao = DateTime.Now,
                          EmpresaID = 1,
                          CampoID = 96
                      },
                       new CampoEmpresa
                       {
                           ID = 25,
                           Ativo = true,
                           DataCadastro = DateTime.Now,
                           DataAlteracao = DateTime.Now,
                           EmpresaID = 1,
                           CampoID = 98
                       },
                        new CampoEmpresa
                        {
                            ID = 26,
                            Ativo = true,
                            DataCadastro = DateTime.Now,
                            DataAlteracao = DateTime.Now,
                            EmpresaID = 1,
                            CampoID = 100
                        },
                         new CampoEmpresa
                         {
                             ID = 27,
                             Ativo = true,
                             DataCadastro = DateTime.Now,
                             DataAlteracao = DateTime.Now,
                             EmpresaID = 1,
                             CampoID = 102
                         });
        }
    }
}
