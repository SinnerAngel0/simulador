﻿using Simulador_DAO.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Facades.Interfaces
{
    public interface CampoProviderBehavior
    {
        Campo Cadastrar(Campo obj);
        Campo Alterar(Campo obj);
        void Excluir(int ID);
        Campo Buscar(int ID);
        Campo Buscar(Func<Campo, bool> predicate);
        List<Campo> Listar();
        List<Campo> Listar(Func<Campo, bool> predicate);

    }
}
