﻿using Simulador_DAO.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Facades.Interfaces
{
    public interface SimuladorProviderBehavior
    {
        Simulador Cadastrar(Simulador obj);
        Simulador Alterar(Simulador obj);
        void Excluir(int ID);
        Simulador Buscar(int ID);
        Simulador Buscar(Func<Simulador, bool> predicate);
        List<Simulador> Listar();
        List<Simulador> Listar(Func<Simulador, bool> predicate);
    }
}
