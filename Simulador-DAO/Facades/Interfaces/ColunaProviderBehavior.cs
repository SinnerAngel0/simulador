﻿using Simulador_DAO.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Facades.Interfaces
{
    public interface ColunaProviderBehavior
    {
        Coluna Cadastrar(Coluna obj);
        Coluna Alterar(Coluna obj);
        void Excluir(int ID);
        Coluna Buscar(int ID);
        Coluna Buscar(Func<Coluna, bool> predicate);
        List<Coluna> Listar();
        List<Coluna> Listar(Func<Coluna, bool> predicate);
    }
}
