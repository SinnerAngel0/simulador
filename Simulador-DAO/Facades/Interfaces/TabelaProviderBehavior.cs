﻿using Simulador_DAO.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Facades.Interfaces
{
    public interface TabelaProviderBehavior
    {
        Tabela Cadastrar(Tabela obj);
        Tabela Alterar(Tabela obj);
        void Excluir(int ID);
        Tabela Buscar(int ID);
        Tabela Buscar(Func<Tabela, bool> predicate);
        List<Tabela> Listar();
        List<Tabela> Listar(Func<Tabela, bool> predicate);
    }
}
