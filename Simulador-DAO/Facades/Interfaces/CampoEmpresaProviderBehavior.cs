﻿using Simulador_DAO.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Facades.Interfaces
{
    public interface CampoEmpresaProviderBehavior
    {

        CampoEmpresa Cadastrar(CampoEmpresa obj);
        CampoEmpresa Alterar(CampoEmpresa obj);
        void Excluir(int ID);
        CampoEmpresa Buscar(int ID);
        CampoEmpresa Buscar(Func<CampoEmpresa, bool> predicate);
        List<CampoEmpresa> Listar();
        List<CampoEmpresa> Listar(Func<CampoEmpresa, bool> predicate);
    }
}
