﻿using Simulador_DAO.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Facades.Interfaces
{
    public interface UsuarioProviderBehavior
    {
        Usuario Cadastrar(Usuario obj);
        Usuario Alterar(Usuario obj);
        void Excluir(int ID);
        Usuario Buscar(int ID);
        Usuario Buscar(Func<Usuario, bool> predicate);
        List<Usuario> Listar();
        List<Usuario> Listar(Func<Usuario, bool> predicate);
    }
}
