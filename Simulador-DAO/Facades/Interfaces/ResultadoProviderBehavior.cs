﻿using Simulador_DAO.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Facades.Interfaces
{
    public interface ResultadoProviderBehavior
    {
        Resultado Cadastrar(Resultado obj);
        Resultado Alterar(Resultado obj);
        void Excluir(int ID);
        Resultado Buscar(int ID);
        Resultado Buscar(Func<Resultado, bool> predicate);
        List<Resultado> Listar();
        List<Resultado> Listar(Func<Resultado, bool> predicate);
    }
}
