﻿using Simulador_DAO.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Facades.Interfaces
{
    public interface EmailTokenProviderBehavior
    {
        EmailToken Cadastrar(EmailToken obj);
        EmailToken Alterar(EmailToken obj);
        void Excluir(int ID);
        EmailToken Buscar(int ID);
        EmailToken Buscar(Func<EmailToken, bool> predicate);
        List<EmailToken> Listar();
        List<EmailToken> Listar(Func<EmailToken, bool> predicate);
    }

}
