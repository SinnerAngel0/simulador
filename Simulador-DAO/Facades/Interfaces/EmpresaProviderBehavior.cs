﻿using Simulador_DAO.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Facades.Interfaces
{
    public interface EmpresaProviderBehavior
    {
        Empresa Cadastrar(Empresa obj);
        Empresa Alterar(Empresa obj);
        void Excluir(int ID);
        Empresa Buscar(int ID);
        Empresa Buscar(Func<Empresa, bool> predicate);
        List<Empresa> Listar();
        List<Empresa> Listar(Func<Empresa, bool> predicate);
    }
}
