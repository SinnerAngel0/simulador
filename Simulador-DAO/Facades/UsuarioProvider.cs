﻿using Simulador_DAO.Entities;
using Simulador_DAO.Facades.Interfaces;
using Simulador_DAO.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Facades
{
    public class UsuarioProvider : UsuarioProviderBehavior
    {
        private UsuarioRepository repo;
        public UsuarioProvider()
        {
            repo = new UsuarioRepository();
        }
        public Usuario Alterar(Usuario obj)
        {
            repo.Update(obj);
            repo.Save();

            return obj;

        }

        public Usuario Buscar(int ID)
        {
            return repo.Find(ID);
        }

        public Usuario Buscar(Func<Usuario, bool> predicate)
        {
            return repo.Get(predicate).FirstOrDefault();
        }

        public Usuario Cadastrar(Usuario obj)
        {
            repo.Add(obj);
            repo.Save();

            return obj;

        }

        public void Excluir(int ID)
        {
            repo.Excluir(x => x.ID == ID);
            repo.Save();

        }

        public List<Usuario> Listar()
        {
            return repo.GetAll().ToList();
        }

        public List<Usuario> Listar(Func<Usuario, bool> predicate)
        {
            return repo.Get(predicate).ToList();
        }
    }
}
