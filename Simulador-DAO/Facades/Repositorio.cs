﻿using Simulador_DAO.Facades.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Facades
{
    public class Repositorio
    {
        private static Repositorio instance;

        public CampoProviderBehavior Campo { get; set; }
        public CampoEmpresaProviderBehavior CampoEmpresa { get; set; }
        public EmpresaProviderBehavior Empresa { get; set; }
        public ColunaProviderBehavior Coluna { get; set; }
        public ResultadoProviderBehavior Resultado { get; set; }
        public SimuladorProviderBehavior Simulador { get; set; }
        public TabelaProviderBehavior Tabela { get; set; }
        public UsuarioProviderBehavior Usuario { get; set; }
        public EmailTokenProviderBehavior EmailToken { get; set; }

        public Repositorio()
        {
            Campo = new CampoProvider();
            CampoEmpresa = new CampoEmpresaProvider();
            Empresa = new EmpresaProvider();
            Coluna = new ColunaProvider();
            Resultado = new ResultadoProvider();
            Simulador = new SimuladorProvider();
            Tabela = new TabelaProvider();
            Usuario = new UsuarioProvider();
            EmailToken = new EmailTokenProvider();
        }

        public static Repositorio Get()
        {
            instance = instance == null ? new Repositorio() : instance;
            return instance;
        }
    }
}
