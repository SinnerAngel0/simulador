﻿using Simulador_DAO.Entities;
using Simulador_DAO.Facades.Interfaces;
using Simulador_DAO.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Facades
{
    public class EmailTokenProvider : EmailTokenProviderBehavior
    {
        private EmailTokenRepository repo;
        public EmailTokenProvider()
        {
            repo = new EmailTokenRepository();
        }

        public EmailToken Alterar(EmailToken obj)
        {
            repo.Update(obj);
            repo.Save();

            return obj;
        }

        public EmailToken Buscar(int ID)
        {
            return repo.Find(ID);
        }

        public EmailToken Buscar(Func<EmailToken, bool> predicate)
        {
            return repo.Get(predicate).FirstOrDefault();
        }

        public EmailToken Cadastrar(EmailToken obj)
        {
            repo.Add(obj);
            repo.Save();

            return obj;
        }

        public void Excluir(int ID)
        {
            repo.Excluir(x => x.ID == ID);
            repo.Save();
        }

        public List<EmailToken> Listar()
        {
            return repo.GetAll().ToList();
        }

        public List<EmailToken> Listar(Func<EmailToken, bool> predicate)
        {
            return repo.Get(predicate).ToList();
        }
    }

}
