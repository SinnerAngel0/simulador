﻿using Simulador_DAO.Entities;
using Simulador_DAO.Facades.Interfaces;
using Simulador_DAO.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Facades
{
    public class SimuladorProvider : SimuladorProviderBehavior
    {
        private SimuladorRepository repo;
        public SimuladorProvider()
        {
            repo = new SimuladorRepository();
        }
        public Simulador Alterar(Simulador obj)
        {
            repo.Update(obj);
            repo.Save();

            return obj;

        }

        public Simulador Buscar(int ID)
        {
            return repo.Find(ID);
        }

        public Simulador Buscar(Func<Simulador, bool> predicate)
        {
            return repo.Get(predicate).FirstOrDefault();
        }

        public Simulador Cadastrar(Simulador obj)
        {
            repo.Add(obj);
            repo.Save();

            return obj;

        }

        public void Excluir(int ID)
        {
            repo.Excluir(x => x.ID == ID);
            repo.Save();

        }

        public List<Simulador> Listar()
        {
            return repo.GetAll().ToList();
        }

        public List<Simulador> Listar(Func<Simulador, bool> predicate)
        {
            return repo.Get(predicate).ToList();
        }
    }
}
