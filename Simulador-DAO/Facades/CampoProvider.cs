﻿using Simulador_DAO.Entities;
using Simulador_DAO.Facades.Interfaces;
using Simulador_DAO.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Facades
{
    public class CampoProvider : CampoProviderBehavior
    {
        private CampoRepository repo;
        public CampoProvider()
        {
            repo = new CampoRepository();
        }

        public Campo Alterar(Campo obj)
        {
            repo.Update(obj);
            repo.Save();

            return obj;

        }

        public Campo Buscar(int ID)
        {
            return repo.Find(ID);
        }

        public Campo Buscar(Func<Campo, bool> predicate)
        {
            return repo.Get(predicate).FirstOrDefault();
        }

        public Campo Cadastrar(Campo obj)
        {
            repo.Add(obj);
            repo.Save();

            return obj;

        }

        public void Excluir(int ID)
        {
            repo.Excluir(x => x.ID == ID);
            repo.Save();

        }

        public List<Campo> Listar()
        {
            return repo.GetAll().ToList();
        }

        public List<Campo> Listar(Func<Campo, bool> predicate)
        {
            return repo.Get(predicate).ToList();
        }
    }
}
