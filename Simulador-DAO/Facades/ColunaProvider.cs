﻿using Simulador_DAO.Entities;
using Simulador_DAO.Facades.Interfaces;
using Simulador_DAO.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Facades
{
    public class ColunaProvider : ColunaProviderBehavior
    {
        private ColunaRepository repo;
        public ColunaProvider()
        {
            repo = new ColunaRepository();
        }
        public Coluna Alterar(Coluna obj)
        {
            repo.Update(obj);
            repo.Save();

            return obj;

        }

        public Coluna Buscar(int ID)
        {
            return repo.Find(ID);
        }

        public Coluna Buscar(Func<Coluna, bool> predicate)
        {
            return repo.Get(predicate).FirstOrDefault();
        }

        public Coluna Cadastrar(Coluna obj)
        {
            repo.Add(obj);
            repo.Save();

            return obj;

        }

        public void Excluir(int ID)
        {
            repo.Excluir(x => x.ID == ID);
            repo.Save();

        }

        public List<Coluna> Listar()
        {
            return repo.GetAll().ToList();
        }

        public List<Coluna> Listar(Func<Coluna, bool> predicate)
        {
            return repo.Get(predicate).ToList();
        }
    }
}
