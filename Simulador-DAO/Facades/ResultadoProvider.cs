﻿using Simulador_DAO.Entities;
using Simulador_DAO.Facades.Interfaces;
using Simulador_DAO.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Facades
{
    public class ResultadoProvider : ResultadoProviderBehavior
    {
        ResultadoRepository repo;
        public ResultadoProvider()
        {
            repo = new ResultadoRepository();
        }
        public Resultado Alterar(Resultado obj)
        {
            repo.Update(obj);
            repo.Save();

            return obj;
        }

        public Resultado Buscar(int ID)
        {
            return repo.Find(ID);
        }

        public Resultado Buscar(Func<Resultado, bool> predicate)
        {
            return repo.Get(predicate).FirstOrDefault();
        }

        public Resultado Cadastrar(Resultado obj)
        {
            repo.Add(obj);
            repo.Save();

            return obj;
        }

        public void Excluir(int ID)
        {
            repo.Excluir(x => x.ID == ID);
            repo.Save();
        }

        public List<Resultado> Listar()
        {
            return repo.GetAll().ToList();
        }

        public List<Resultado> Listar(Func<Resultado, bool> predicate)
        {
            return repo.Get(predicate).ToList();
        }
    }
}
