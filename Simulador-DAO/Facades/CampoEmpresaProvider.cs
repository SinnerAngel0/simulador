﻿using Simulador_DAO.Entities;
using Simulador_DAO.Facades.Interfaces;
using Simulador_DAO.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Facades
{
    public class CampoEmpresaProvider : CampoEmpresaProviderBehavior
    {
        private CampoEmpresaRepository repo;
        public CampoEmpresaProvider()
        {
            repo = new CampoEmpresaRepository();
        }

        public CampoEmpresa Alterar(CampoEmpresa obj)
        {
            repo.Update(obj);
            repo.Save();

            return obj;
        }

        public CampoEmpresa Buscar(int ID)
        {
            return repo.Find(ID);
        }

        public CampoEmpresa Buscar(Func<CampoEmpresa, bool> predicate)
        {
            return repo.Get(predicate).FirstOrDefault();
        }

        public CampoEmpresa Cadastrar(CampoEmpresa obj)
        {
            repo.Add(obj);
            repo.Save();

            return obj;
        }

        public void Excluir(int ID)
        {
            repo.Excluir(x => x.ID == ID);
            repo.Save();
        }

        public List<CampoEmpresa> Listar()
        {
            return repo.GetAll().ToList();
        }

        public List<CampoEmpresa> Listar(Func<CampoEmpresa, bool> predicate)
        {
            return repo.Get(predicate).ToList();
        }
    }
}
