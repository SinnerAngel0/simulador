﻿using Simulador_DAO.Entities;
using Simulador_DAO.Facades.Interfaces;
using Simulador_DAO.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Facades
{
    public class TabelaProvider : TabelaProviderBehavior
    {
        private TabelaRepository repo;

        public TabelaProvider()
        {
            repo = new TabelaRepository();
        }
        public Tabela Alterar(Tabela obj)
        {
            repo.Update(obj);
            repo.Save();

            return obj;

        }

        public Tabela Buscar(int ID)
        {
            return repo.Find(ID);
        }

        public Tabela Buscar(Func<Tabela, bool> predicate)
        {
            return repo.Get(predicate).FirstOrDefault();
        }

        public Tabela Cadastrar(Tabela obj)
        {
            repo.Add(obj);
            repo.Save();

            return obj;

        }

        public void Excluir(int ID)
        {
            repo.Excluir(x => x.ID == ID);
            repo.Save();

        }

        public List<Tabela> Listar()
        {
            return repo.GetAll().ToList();
        }

        public List<Tabela> Listar(Func<Tabela, bool> predicate)
        {
            return repo.Get(predicate).ToList();
        }
    }
}
