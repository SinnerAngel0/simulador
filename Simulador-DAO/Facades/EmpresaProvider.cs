﻿using Simulador_DAO.Entities;
using Simulador_DAO.Facades.Interfaces;
using Simulador_DAO.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Simulador_DAO.Facades
{
    public class EmpresaProvider : EmpresaProviderBehavior
    {
        private EmpresaRepository repo;

        public EmpresaProvider()
        {
            repo = new EmpresaRepository();
        }

        public Empresa Alterar(Empresa obj)
        {
            repo.Update(obj);
            repo.Save();

            return obj;
        }

        public Empresa Buscar(int ID)
        {
            return repo.Find(ID);
        }

        public Empresa Buscar(Func<Empresa, bool> predicate)
        {
            return repo.Get(predicate).FirstOrDefault();
        }

        public Empresa Cadastrar(Empresa obj)
        {
            repo.Add(obj);
            repo.Save();

            return obj;
        }

        public void Excluir(int ID)
        {
            repo.Excluir(x => x.ID == ID);
            repo.Save();
        }

        public List<Empresa> Listar()
        {
            return repo.GetAll().ToList();
        }

        public List<Empresa> Listar(Func<Empresa, bool> predicate)
        {
            return repo.Get(predicate).ToList();
        }
    }
}
